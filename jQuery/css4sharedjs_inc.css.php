<?php
if (!isset($headerSent)) $headerSent = false; // pour éviter les warnings qui mettent la merde
if (!$headerSent) header("Content-type: text/css; charset=utf-8");
// fichier css4sharedjs_inc.css.php
// fichier partagé placé dans php_inc

include (__DIR__."/jquery-ui.css"); 
// styles calendrier jQuery appelé sur input date 
//include ("ui.datepicker.css"); // tout ça est maintenant inclus dans l'ui
// styles rte jQuery appelé sur input de class rte-zone
//include ("rte.css"); 
include (__DIR__."/jquery.rte.css"); 
?>
/* On peut la modifier comme ça...
input.dateinput {
	color : blue;
	size : 7;
*/
.notNullField {color: red; font-weight:bold;}
.warning {background-color: #ffbab7;}
/* Styles LimKnife */
#LimKnifeFilter { display: none; position: fixed; top: 0%; left: 0; width: 100%; height: 100%; background-color: black; z-index: 10; opacity: 0.7; filter: alpha(opacity = 70);}
#LimKnifepopupDiv{
	display:none; 
	position: fixed !important; /* carfixed ne marche pas avec IE */
	position: absolute; 
	top: 30%; 
	left: 30%;
	width : auto;
	height : auto; 
	z-index: 1100; 
	overflow: auto;
	background-color:white; 
	border-style: solid;
	border-width: 2px;
	border-color: #006DCC;
	text-align: center;
    background-repeat: no-repeat;
    padding: 15px;
    padding-top: 1px;
}
.LimKnifepopupBut {background-color:#006DCC; color:white;font-weight:bold; border-radius:10px}
