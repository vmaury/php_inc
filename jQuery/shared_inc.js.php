<?php
error_reporting(E_ERROR); // si warning ça met la gouaille
if (!isset($headerSent)) $headerSent = false; // pour éviter les warnings qui mettent la merde

if (!$headerSent) header("Content-type: text/javascript; charset=utf-8");
// fichier shared_inc.js.php

// fctions principales de jQuery
include (__DIR__."/jquery.min.js");
// jQuery ui
//include (__DIR__."/jquery-ui-1.8.18.custom.min.js"); // viré car fout la merde avec GDP2 ancienne version (erreur IE)
include (__DIR__."/jquery-ui.min.js"); 
// calendrier sur input date
//include (__DIR__."/ui.datepicker.js"); inclus dans jQueryui
// francisation du calendrier
include (__DIR__."/ui.datepicker-fr.js");
// timepicker sur input datetime
include (__DIR__."/jquery-ui-timepicker-addon.js");
// francisation du timepicker
include (__DIR__."/jquery-ui-timepicker-fr.js");
// popin
include (__DIR__."/jquery.popin.js");
// RTE
include (__DIR__."/jquery.rte.js");
// Toolbars du RTE
include (__DIR__."/jquery.rte.tb.js");
// script de customisation
if (isset($_REQUEST['custJS'])) {
	echo urldecode($_REQUEST['custJS']);}
// init diverses JS dlcube
include (__DIR__."/initjqdl3.js");
//include (__DIR__."/runonload.js");
?>
/* test activation/désactivation entrée de type LDandTxt*/
function CheckLDandTxt(theId) {
	var selvalue = document.getElementById('assLD4Txt' + theId).options[document.getElementById('assLD4Txt' + theId).selectedIndex].value;
	if (selvalue == 'OTH') {
		/* IE ne supporte pas qu'on change le type à la volée... 
		document.getElementById(theId).type = 'text';*/
		document.getElementById(theId).value = '';
	} else {
		/*IE ne supporte pas qu'on change le type à la volée... 
		document.getElementById(theId).type = 'hidden';*/
		document.getElementById(theId).value = selvalue;
	}
}

/* contrôle de formulaire; appelé par onsubmit de form */
var nbInput2test = 0;
var tbId2Verif= new Array();
var tbTypeVerif = new Array();
var tbLibChp2Verif = new Array();

function testJSValChp() {
	var formOk = true;
	var theValue ='';

	if (typeof oPopupWin != "undefined") closepop();	
	for (i=1; i<=nbInput2test; i++) {
		theValue = '';
		if (document.getElementById(tbId2Verif[i]).type =="checkbox" || document.getElementById(tbId2Verif[i]).type =="radio") {
			for( j=0; j < document.getElementsByName(tbId2Verif[i]).length; j++) { /* tt ça pr les boutons radio ou les checkbox */
				theValue = theValue + document.getElementsByName(tbId2Verif[i]).item(j).value;
			}
		} else  theValue =  document.getElementById(tbId2Verif[i]).value;
		//alert (tbLibChp2Verif[i] + tbId2Verif[i] + theValue);			
		formOk = formOk && valueCtrl(tbTypeVerif[i], theValue, tbLibChp2Verif[i], tbId2Verif[i]);
	}
	return(formOk);
}
// fonction qui contrôle une valeur en fonction d'un code
/* Chaque fois qu'on rajoute un code il faut le mettre dans tbCodesToTest ci dessus ,'number','numberNN'*/
/*
<?php
// inclusions de fichiers partagés placés dans php_inc
//include_once ('../fonctions_js_html.php'); // pour récupérer $GLOBALS['tbEvenmtVFAutoJS'] ; ca c'est le bordel complet, car le chemein est pas le bon suivant d'où est inclus ce fichier
$GLOBALS['tbEvenmtVFAutoJS'] = array ( 
			"notNull" => "Non vide",
			"email" => "Adresse email",
			"emailNN" => "Adresse email non vide",
			"tel" =>"N° téléphone",
			"telNN" => "N° téléphone non vide",
			"number" => "Nombre",
			"numberNN" => "Nombre >0",
    	 	);

foreach ($GLOBALS['tbEvenmtVFAutoJS'] as $k=>$v) $jsArr[] = "'$k'";
?>
*/
var tbCodesToTest = new Array (<?php echo implode(",",$jsArr)?>);

function valueCtrl(codeTypVerif, theValue, libChp, theId) {
	var condNN = false;
	// messages: a traduire
	var errOnField = "Erreur sur le champ ";
	var mustNoBeNull = " : il ne doit pas être vide";
	var mustBeEmail = " : ce devrait être une adresse email valide";
	var mustBeTel = " : ce devrait être un n° de tel valide (+)00 00 00 00 00";
	var mustBeNumberNN = " : ce devrait être un nombre non nul";
	var mustBeNumber = " : ce devrait être un nombre";
	var	regMail = new RegExp( "^\\w[\\w+\.\-]*@[\\w\-]+\.\\w[\\w+\.\-]*\\w$", "gi" );
	// 	 Explication du modèle: \\w: un caractère au début de cet email. [\\w+\.\-]*: autant de caractères que l’on veut après, plus point et tiret. @:un arobase [\\w\-]: au moins un caractère, plus tiret. \.: un point \\w: un caractère après le point [\\w+\.\-]*: autant de caractères que l’on veut après, plus point et tiret. \\w: un caractère après. $: fin de l’email.
	var	regTel = new RegExp( "^[\\d+\+][\\d+ ]{9,16}$", "gi" );
	// 	 Explication du modèle: [\\d+\+]: un digit ou + au début; [\\d+ ]{9,16}: de 9 à 16 digit ou espace; $: fin du tel
	var	regNum = new RegExp( "^[\\d+\+\-][\\d+\.]*$", "gi" );
	

	/* Chaque fois qu'on rajoute un code il faut le mettre dans tbCodesToTest / $GLOBALS['tbEvenmtVFAutoJS'] qui est dans 'fonctions_js_html.php' ci dessus*/
	if (libChp == '' || libChp === null  || libChp === undefined) libChp = theId;
	var errOnField = "Erreur sur le champ \"" + libChp + "\"";
	var formOk = true;
	switch (codeTypVerif) {
		case 'notNull':
		case 'puretextNN':
			if (theValue == ''|| theValue == null) {
				alert (	errOnField + mustNoBeNull);
				formOk = false;
			}
		break;
		
		case 'emailNN':
			condNN = (theValue == '' || theValue == null);
			if(theValue.search(regMail) == -1 || condNN) {
				alert (	errOnField + ' (' + theValue + ') ' + mustBeEmail);
				formOk = false;
			}
		break;
		
		case 'email':
			condNN = (theValue == '' || theValue == null);
			if(theValue.search(regMail) == -1 && !condNN) {
				alert (	errOnField + ' (' + theValue + ') ' + mustBeEmail);
				formOk = false;
			}
		break;
		
		
		case 'telNN':
			condNN = (theValue == '' || theValue == null || theValue == '0' );
			if(theValue.search(regTel) == -1 || condNN) {
				alert (	errOnField + ' (' + theValue + ') ' + mustBeTel);
				formOk = false;
			}
		break;
		
		case 'tel':
			condNN = (theValue == '' || theValue == null || theValue == '0' );
			if(theValue.search(regTel) == -1 && !condNN) {
				alert (	errOnField + ' (' + theValue + ') ' + mustBeTel);
				formOk = false;
			}
		break;
		
		case 'numberNN':
			condNN = (parseInt(theValue) == 0 || theValue == '' || theValue == null);
			theValue = theValue.replace(",","."); 
			document.getElementById(theId).value = theValue;
			if(theValue.search(regNum) == -1 || condNN) {
				alert (	errOnField + ' (' + theValue + ') ' + mustBeNumberNN);
				formOk = false;
			}
		break;

		case 'number':
			theValue = theValue.replace(",","."); 
			document.getElementById(theId).value = theValue;
			if(theValue.search(regNum) == -1) {
				alert (	errOnField + ' (' + theValue + ') ' + mustBeNumber);
				formOk = false;
			}
		break;
	}
	return(formOk);
}

function getRadioValue(radioObj) {
	if(!radioObj)
		return "";
	var radioLength = radioObj.length;
	if(radioLength == undefined)
		if(radioObj.checked)
			return radioObj.value;
		else
			return "";
	for(var i = 0; i < radioLength; i++) {
		if(radioObj[i].checked) {
			return radioObj[i].value;
		}
	}
	return "";
}

function showLimKnifePop(ajaxUrl) {
	if (navigator.appName == 'Microsoft Internet Explorer') window.location.href="#LimKnifepopupDivAnchor";  // car la position fixed ne ne marche pas sous IE
	if (navigator.appName != 'Microsoft Internet Explorer') $("#LimKnifeFilter").fadeIn();
	$('#LimKnifepopupDiv').show('normal');
	$('#LimKnifepopupDiv').load(ajaxUrl);
}

function hideLimKnifePop(ajaxUrl) {
	if (navigator.appName != 'Microsoft Internet Explorer') $("#LimKnifeFilter").hide();
	$("#LimKnifepopupDiv").hide('normal');
}

