/* initialisations diverses de jQuery
*/
var tabRteZone = new Array();

if (typeof rteWidth == 'undefined') var rteWidth = 400;
if (typeof rteHeight == 'undefined') var rteHeight = 300;

$(document).ready(function() {
/* commence par les gadgets */
/* affichage du calendrier à toutes les classe dateinput */
	$('.dateinput').datepicker();
/* affichage du datetime picker à toutes les classe datetimeinput */
	$('.datetimeinput').datetimepicker();
/*	$('.rte-zone').rte("", "/php_inc/jQuery/rteImgs/");*/
/* idem Rich Text Editor */
	$('.jqrte2').rte({
		css: ['default.css'],
		/* base_url: 'http://mysite.com',*/
		frame_class: 'frameBody',
		width: rteWidth,
		height: rteHeight,
		controls_rte: rte_toolbar,
		controls_html: html_toolbar
	});

/* on passe aux contrôles de saisie 
tous ceux qui sont dans la table tbCodesToTest 
les codes sont identiques aux noms de classe  */
// test des champs quand on les quitte
	formOk = true;
	$(':input').blur(function() { // syntaxe :input toutes les input !
		$(this).removeClass('warning');
		for (var icoTest in tbCodesToTest) { // balaye les class/codes a tester
			var codTest = tbCodesToTest[icoTest]; // le code
			//alert(codTest);
			if ($(this).hasClass(codTest)) { // si l'input a la classe qui va bien
				//alert(this.name + "=>" + codTest);
				var chpOk = valueCtrl(codTest, this.value, this.title, this.id);
				formOk = formOk && chpOk;
				if (!chpOk)	{
					$(this).addClass('warning');
				}
			}
		}
		//alert($(this).attr('id') + codTest + formOk);
		//return (formOk);
	});
	
	$('form').submit(function() {
		formOk = true;
		$(':input').trigger('blur');
		return(formOk);
	});

/*
on ne peut pas passer une var pour désigner une classe ???
	for (var icoTest in tbCodesToTest) {
		var codTest = tbCodesToTest[icoTest];
		$('input.' + codTest).blur(function() {
			valueCtrl(codTest, this.value, this.name);
		});
	} */
// 
});

