<?php 
/**
   * Objet PYA, permettant l'affichage de filtres, de champ dans les listes et de controle d'edition
   * 
   * @author: Vincent MAURY <artec.vm@arnac.net>
   /// TODO TODO voir si on ne peut pas gérer les contrôles de formulaire en jquery, en passant juste un style
   */

class PYAobj {

	var $NbCarMxCust=0;
	var $NmBase = "pyaDB"; /// a priori, ne pas définir la bdd n'est pas important TODO A VERIFIER;
	var $NmTable;
	var $NmChamp;
	var $Libelle;
	var $title = "";
	var $Typaff_l = "NOR"; // type affichage en mode liste (tableau de plusieurs enregistrements)
	var $FieldType;
	var $TTC; 
	var $FieldValDef = "";
	var $ValChp;
	var $ValChpAvMaj; // valeur champ avant MAJ
	var $FieldNullOk; // YES ou rien
	var $FieldKey; // clePRI, index=MUL, unique=UNI
	var $FieldExtra; // auto_increment
	var $TypeAff = "AUT";
	var $Fccr = ""; // force cases a cocher/radio en place des LD. rien ou
				// si qqchose, force. Si contient 'BR' force avec retour de lignes
				// si contient RAD force boutons radios
	var $Valeurs; // THE propriété, qui contient les liaisons de champ
	var $Val2 = ""; // valeurs en plus, Statuts en fonction de l'enregistrement
	var $nmfpopl = "popl.php"; // nom du fichier popup
	var $chempopl = ""; // chemin par défaut pour la popup, incluant les /
	var $TypFilt;
	var $TypCSA;
	var $Tt_AvMaj;
	var $Tt_PdtMaj;
	var $Tt_AprMaj;
	var $TypEdit = "N"; // type edition : creation ("" ou N), copie(2), edition(1 ou M) ou consultation (C), effacement (-1)
	var $DirEcho = true; // Direct Echo: pour savoir si on echoise direct ou renvoie la valeur
	var $Comment;
	var $MaxFSize; // taille max des fichiers joints !!
	var $MaxImgWidth = 0; // largeur max des images
	var $MaxImgSize2display = 100000; // taille max des images
	var $dateInpAttribs =' class="dateinput" size="8" maxlength="10" ';
	var $datetimeInpAttribs =' class="datetimeinput" size="12" maxlength="19" ';
	var $error;
	var $errInit;
	/// Propriétés liées à la Méthode EchoCompField
// nom des classes par défaut utilisées pour EchoCompField
	var $classDivECF = "DivECF";
	var $classFieldECF = "FieldECF";
	var $classLabECF = "LabECF";
	var $classCSAECF = "CSAECF";
	var $spanOrDiv4FieldECF = "span";
	var	$modTable = false ;// mode table / div
	var	$br = false;//  CR après libellé
	var	$dispComment = false;// affichage commentaire
	var	$altLibel = "";//  autre libellé que celui stocké en base
/// Prop pour filtre uniquement: 
	var	$bDN = true ;// display négation dans filtre
	var	$bDL = true ; // display baratin libellés dates
	var	$choixMult = "yes"; // 
	var	$dispCSA = "yes"; // display champ sélectionnable ou n'importe quoi <> no
	// Pour Edit uniquement : 
	var	$dhidden = true; //display Hidden en édition
	var $dialFormId = "dialogform"; // id par défaut du formulaire de saisie (utilisé dans les types d'édition JQCTRL)
	var $tblocfkeys = array();
	
	/** methode qui initialise toutes les propriete de l'objet pour un champ
	 simplement a partir de la definition du nom du champ, de la table et de la base (maintenant optionnelle)
	 @param $Val2assign la valeur a assigner à $this->ValChp
	 @param $CcChp array tableau des caractéristiques du champ(enregistrement de la table DESC_TABLES
	 */
	function InitPO($Val2assign="", $CcChp = false)  {
		// test l'existence de la cste qui dit si on met les noms de table dans les var html
		if(!defined("DefOutNmTblInHtmlVarName")) {
			define("DefOutNmTblInHtmlVarName",false);
		}
		if(!defined("sepNmTableNmChp")) { // carac de séparation NOM_TABLE#NOM_CHAMP; on utilise celui_la car il passe avec Oracle dans les alias de champ
			define("sepNmTableNmChp","#"); // sécurité
		}
		
		// Nom de la table de description des autres
		$TBDname = $GLOBALS["DESC_TABLES"] ? $GLOBALS["DESC_TABLES"] : "DESC_TABLES";
		// nom du champ contenant les caracteristiques globales de la table
		$NmChDT = $GLOBALS["NmChDT"] ? $GLOBALS["NmChDT"] : "TABLE0COMM";
		// morceau de chaine identifiant une table virtuelle, ie celles qui n'existent pas en base (alias)
		$id_vtb = $GLOBALS["id_vtb"] ? $GLOBALS["id_vtb"] : "_vtb_";
		if ($this->TypEdit == "") $this->TypEdit = "N";
		if ($this->TypEdit == 1) $this->TypEdit = "M";
		$ult = rtb_ultchp(); // tableau des noms de champs sensibles a la casse (a cause de pgsql...)
		// verrue pour les req custom: dans le cas d'un UNION, ce con de mysql est buggué, la fonction db_field_table($resreq,$i) ne marche pas !!
		// on cherche donc à retrouver le nom de la table via la table de description
		if ($this->NmTable == "") {
			$rpDF = db_query("select * from $TBDname where NM_CHAMP='$this->NmChamp'");
			if (db_num_rows($rpDF) == 1) {
				$CcChp = db_fetch_assoc($rpDF);
				$this->NmTable = $CcChp[$ult['NM_TABLE']];
			} else {
				$retval = "Impossible de déterminer automatiquement la table du champ ".$this->NmChamp;
			}
		}
		if ($this->NmChamp != "" && $this->NmTable != "" && $this->NmBase!= "") {
			if (!$CcChp) { // les args de champ ne sont pas passés
				/// TODO TODO C'est ici qu'il faudrait mettre et récupérer les objets en cache mémoire... bof, un objet s'utilise rarement seul, mieux vaut stocker des tableaux
				$rqsh = "select * from $TBDname where NM_TABLE LIKE '$this->NmTable' AND NM_CHAMP LIKE '$this->NmChamp'";
				$CcChp = db_qr_rass2($rqsh);
				if (!$CcChp) $retVal .= "Champ $this->NmChamp (table :$this->NmTable, base: $this->NmBase) non trouve dans la table de description";// <br/>$rqsh<br/>
			}
			
			if ($CcChp) {
				$this->Libelle = tradLib(stripslashes($CcChp[$ult['LIBELLE']]));
				$this->Typaff_l = $CcChp[$ult['TYPAFF_L']];
				$this->TypeAff = $CcChp[$ult['TYPEAFF']];
				$this->Valeurs = $CcChp[$ult['VALEURS']];
				$this->ParseValeurs(); // parse la valeur avec la nouvelle syntaxe avancée..
				if (strstr($this->Valeurs,"@@")) $this->Fccr="LDF"; // si hiérarchique, force les listes déroulantes
				$this->Tt_AvMaj = $CcChp[$ult['TT_AVMAJ']];
				$this->Tt_PdtMaj = $CcChp[$ult['TT_PDTMAJ']];
				$this->Tt_AprMaj = $CcChp[$ult['TT_APRMAJ']];
				$this->TypFilt = $CcChp[$ult['VAL_DEFAUT']];
				$this->TypCSA = $CcChp[$ult['TYP_CHP']];
				$this->Comment = stripslashes($CcChp[$ult[$GLOBALS["NmChpComment"]]]);
				if (!stristr($this->NmTable, $id_vtb)) { // on cherche a caracteriser le champ que s'il n'est pas dans une table virtuelle
					$TbDescChp = db_table_defs($this->NmTable, $this->NmChamp);
					$this->FieldType = $TbDescChp[$this->NmChamp]['FieldType'];
					$TTC = preg_replace('`\(.*`', '',  $this->FieldType); // vire tt ce qui est après la parenthese
					if (stristr($TTC, "blob") || stristr($TTC, "text")) $TTC = "txtblob";
					if (stristr($TTC, "datetime")) {
							$TTC = "datetime";  // ds oracle DATE est en maj et ça fout la merde
					} elseif (stristr($TTC, "date")) $TTC = "date";  // ds oracle DATE est en maj et ça fout la merde
					if (stristr($TTC, "varchar") || stristr($TTC, "char")) $TTC = "char";
					if (stristr($TTC, "int") || stristr($TTC, "decimal")|| stristr($TTC, "float") || stristr($TTC, "double") || stristr($TTC, "number"))  $TTC = "numeric";
					if (preg_match('`.*(enum|set).*`i', $TTC)) { // construit le tableau des valeurs de set ou enum
						$m[0] = '`.*(enum|set) *\(`i'; // n'importe quoi suivi d'enum ou de set, 0 ou + d'espaces et une parenthèse ouvrante
						$m[1] = '`\).*`'; // tout ce qui suit la parenthèse fermante
						$m[2] = "`'`"; // les simples guillemets
						$this->tbvalsetenum = explode(',', preg_replace($m, '', $this->FieldType)); // tableau des valeurs de set ou enum
					} else unset($this->tbvalsetenum);
					$this->TTC = $TTC;
					$this->FieldValDef = $TbDescChp[$this->NmChamp]['FieldValDef'];
					// si nouvel enregistrement, affecte la valeur par defaut
					$this->FieldNullOk = strtoupper($TbDescChp[$this->NmChamp]['FieldNullOk']); // YES ou rien
					$this->FieldKey = $TbDescChp[$this->NmChamp]['FieldKey']; // cle : PRI, index=MUL, unique=UNI
					$this->FieldExtra = $TbDescChp[$this->NmChamp]['FieldExtra']; // auto_increment
					// si champ auto increment et pas caché on le met en statique
					if (stristr($this->FieldExtra,"auto_increment") && $this->TypeAff!="HID") $this->TypeAff="STA";
				} // fin si pas champ dans une table virtuelle
				if ($this->TypEdit == "N" || !$Val2assign) {
						$this->ValChp = $this->FieldValDef; 
				} else $this->ValChp = $Val2assign;
				$this->checkNmChp4EditHtml(); /* regenere les noms de champs html et 4 edit.*/
			} else {
				$retVal .= "; champ non trouve ou parametres non passes en arguments";
			} // fin si champ trouvé ou renseigné
		} else {
			$retVal .= "Appel de la methode InitPO de l'objet PYAObj incorrecte: au moins une des propriete de base n'a pas definie; NmChamp=".$this->NmChamp."; NmTable=".$this->NmTable."; NmBase=".$this->NmBase."<br/>\n";
		}
		$this->ErrInit = $retVal;
		if ($this->DirEcho && $retVal) {
			echo "<p><small>$retVal</small></p>";
		}
		return($retVal); // retourne quoi qu'il arrive, normalement c'est vide, si ya qqchose c kya une erreur et ça la décrit
	} // fin methode d'initialisation
	
	/** fonction qui regenere les noms de champs html et 4 edit. Rappelé a chaque appel de Edit pour compatibilité */
	function checkNmChp4EditHtml() {
		if(!defined("DefOutNmTblInHtmlVarName")) {
			define("DefOutNmTblInHtmlVarName",false);
		}
		if(!defined("sepNmTableNmChp")) { // carac de séparation NOM_TABLE#NOM_CHAMP; on utilise celui_la car il passe avec Oracle dans les alias de champ
			define("sepNmTableNmChp","#"); // sécurité
		}		
		$this->NmChpHtml = (defined("DefOutNmTblInHtmlVarName") && DefOutNmTblInHtmlVarName ? $this->NmTable.sepNmTableNmChp : "").$this->NmChamp; // si $this->outNmTblInHtmlVarName sort les var html ss la forme nomtable.nomchamp
		$this->NmChpHtml4Edit = $this->sepNmTableNmChpInEdit ? $this->NmChpHtml : $this->NmChamp;
		$this->title = $this->Libelle.($this->Comment != '' ? ' : ' : '').$this->Comment;
//		// verrue ici pour jquery-ui et les celdndriers
//		 if ($GLOBALS['jquery-ui'])  $this->dateInpAttribs = ' class=" datepicker" size="10" maxlength="10" ';

	}
	
	 /** methode affichage de type <div class="classDiv"><label class="classLab"  for="theField">The Label </label>(<br/>)<span class="classField">The Field</span></div>
	 	ou "<tr class="classDivECF"><td><label class="classLabECF"  for="theField">The Label </span></td><td><span class="classFieldECF">The Field</span></td></tr>
	 	s'applique aux filtres, ou edition
		$type = Edit | Filt
		$tbArgs : 
			$modTable=false mode table
			$br=false CR après libellé
			$dispComment=false
			$altLibel="": autre libellé que celui stocké en base
		Pour filtre uniquement: 
			$bDN (display négation)=true 
			$bDL=true; baratin libellé dates
			$choixMult="yes"
			$dispCSA="yes" ou n'importe quoi <> no
		Pour Edit uniquement : 
			$dhidden (display Hidden) = true

		On peut passer les args en tableau, ou comme propriétés au choix */
	function EchoCompField($type,$tbArgs=array()) {
		if ($this->TypeAff == "HID" && $type == "Edit") return($this->EchoEditAll($this->dhidden));
		$this->checkNmChp4EditHtml(); /* regenere les noms de champs html et 4 edit. Rappelé a chaque appel de Edit pour compatibilité */
		foreach ($tbArgs as $c=>$v) $this->$c = $v;
		if ($this->modTable) {
			$retVal = '<tr class="'.$this->classDivECF.'"><td>';
		} else $retVal = '<div class="'.$this->classDivECF.'">';
		$retVal .= '<label class="'.$this->classLabECF.'" for="'.($type == "Filt" ? "rq_" : "").$this->NmChpHtml4Edit.'">'.($this->altLibel ? $this->altLibel : $this->Libelle);
		if ($this->Comment && $this->dispComment) $retVal .='<br/><small>'.tradLib($this->Comment).'</small>';
		$retVal .= '</label>';
		if ($this->br) $retVal .= '<br/>';
		if ($this->modTable) {
			$retVal .= '</td><td>';
		}
		$retVal .= '<'.$this->spanOrDiv4FieldECF.' class="'.$this->classFieldECF.'">';
		// sinon les meth  EchoEditAll & EchoFilt echoisent et ça fout la merde
		$DirEchoSave = $this->DirEcho;
		$this->DirEcho = false;
		if ($type == "Edit") {
			$retVal .= $this->EchoEditAll($this->dhidden);
		} elseif ($type == "Filt") {
			$retVal .= $this->EchoFilt($this->bDN,$this->bDL,$this->choixMult);
		} else {
			$err = "<h3>type $type inconnu pour methode PYA->EchoCompField</h3>";
			echo $err;
			return($err);
		}
		$retVal .= '</'.$this->spanOrDiv4FieldECF.'>';
		if ($type == "Filt" && $this->dispCSA != "no" && $this->TypCSA != "") {
			if ($this->modTable) {
				$retVal .= "<td>".$this->EchoCSA()."</td>";
			} else 	
				$retVal .= '<div class="'.$this->classCSAECF.'" >'.$this->EchoCSA(true)."</div>\n";
		}
		if ($this->modTable) {
			$retVal .= "</td></tr>\n";
		} else 	$retVal .= "</div>\n";
		$this->DirEcho = $DirEchoSave;
		if ($this->DirEcho) {
				echo $retVal;
		} else {
			return($retVal);
		}
	} // fin meth EchoCompField

	/**  methode d'affichage de filtre de requete
	@param $bDN bool affiche négation
	@param $bDL bool affiche baratins sur les dates et autres ...
	@param $choixMult 'yes' (def), 'no' listes deroulantes à choix multiples
	*/
	function EchoFilt($bDN=true, $bDL=true, $choixMult="yes") { // booleens affiche negation et affiche libelles ie baratins sur les dates par ex
		$boolMult = ($choixMult != "no");
		$this->checkNmChp4EditHtml(); /* regenere les noms de champs html et 4 edit. Rappelé a chaque appel de Edit pour compatibilité */
		if (stristr($this->NmTable,$GLOBALS['id_vtb'])) { // si table virtuelle alias, la vraie table doit être indiquée dans le champs VALEURS avec la nouvelle syntaxe cad $physTable=matable
			$nmtable = $this->physTable;
			if ($nmtable == "" && ($this->TypFilt == "LDC" || $this->TypFilt == "LDLLV")) throw new Exception ("La table ".$this->NmTable." est une table virtuelle, le champ ".$this->NmChamp.' necessite que le nom de la VRAIE table soit mise dans le champ VALEURS sous la forme $physTable=NomDeLaTable');
		} else $nmtable = $this->NmTable; 
	// regarde si valeur déjà filtrée pour les listes 
		$valDjFilt = is_array($_SESSION['memFilt']['rq_'.$this->NmChpHtml]) && !(in_array("%", $_SESSION['memFilt']['rq_'.$this->NmChpHtml]));
		switch ($this->TypFilt) {
			case "INPLIKE":
				$suptitle = " - entrez le début de la valeur à rechercher";
			case "EGAL":
				if (!$suptitle) $suptitle = " - entrez la valeur exacte à rechercher";
				$retVal = dispInpHid('tf_'.$this->NmChpHtml,  $this->TypFilt);
				$retVal .= dispInpTxt('rq_'.$this->NmChpHtml, $_SESSION['memFilt']['rq_'.$this->NmChpHtml],array("title" => $this->title.$suptitle));
			break;
			
			case "EGALHID": // special pour OSS
				$retVal = dispInpHid('tf_'.$this->NmChpHtml,  $this->TypFilt);
				$retVal .= dispInpHid('rq_'.$this->NmChpHtml, $_SESSION['memFilt']['rq_'.$this->NmChpHtml]).$_SESSION['memFilt']['rq_'.$this->NmChpHtml];
			break;
			
		
		//! ttes les listes sont à choix multiple (condition OR sur les valeurs) sauf si spécifié
			case "LDC": // Liste deroulante affichant les différentes valeurs du champ
				$retVal = dispInpHid('tf_'.$this->NmChpHtml,  "LDM");
				if ($this->Val2 != "") $whsup = " AND ".$this->Val2;
			// affiche que les valeurs significatives
				$rqldcu = db_query("SELECT distinct $this->NmChamp from $nmtable WHERE $this->NmChamp".$GLOBALS['sqllenstr0']." $whsup order by $this->NmChamp");
				$tbval = array();
				while ($rwsq=db_fetch_row($rqldcu)) {
					$tbval[$rwsq[0]] = (is_arr_in_array($rwsq[0],$_SESSION['memFilt']['rq_'.$this->NmChpHtml]) ? VSLD : "").tradLib($rwsq[0]);
				}
				asort($tbval); // tri de la liste 
				$tbval = array("%"=>(!$valDjFilt ? VSLD : "").$this->trad("all")) + $tbval;
				$retVal .= DispLD($tbval,"rq_".$this->NmChpHtml,$choixMult,$this->Fccr,false);
			break;
		
			case "LDF": // Liste deroulante affichant les differentes valeurs fixes listees dans le champ valeur, ou les valeurs du set ou du enum
				if ($this->TypeAff == "CKBX" || $this->TTC == "enum" || $this->TypeAff == "LD" || $this->TTC == "set") { // choix type de traitement
					$tf = "LDMEG";
					//$choixMult = "no";
				} elseif ( $this->TypeAff == "LDM") {
					$tf = "LDM_SPL";
				} else throw new Exception ("Meth PYA EchoFilt : le type d'affichage $this->TypeAff est incompatible avec le filtre $this->TypFilt (champ $this->NmChamp, table $this->NmTable)");
				// le type de filtre LDM ne devrait plus etre utilisé
				$retVal = dispInpHid('tf_'.$this->NmChpHtml, $tf);
				//print_r($this->tbvalsetenum);
				if ($this->TypeAff == "CKBX") {
					$tabval = array($this->Valeurs);
					$choixMult = "no";
				} else {
					// priorité aux valeurs manuelles. L'hashexplode du $this->Valeurs est faite dans valValc2tb
					$tabval = strstr($this->Valeurs, ",") ?  $this->Valeurs :  (is_array($this->tbvalsetenum) ?  $this->tbvalsetenum :array());
				}
				$tbval = valValc2tb($tabval, $_SESSION['memFilt']['rq_'.$this->NmChpHtml]); 
//				asort($tbval); // tri de la liste
				$tbval = array("%" => (!$valDjFilt ? VSLD : "").$this->trad("all")) + $tbval;
				$retVal .= DispLD($tbval, "rq_".$this->NmChpHtml, $choixMult, $this->Fccr, false);
			break;
		
			case "LDLLV": // Liste deroulante affichant les differentes valeurs liees par le champ valeur limitées aux valeurs existantes
				// ON construit le AND qui va bien
				if (is_array($this->tblocfkeys) && count($this->tblocfkeys) > 0) throw new Exception ("Champ : $this->NmChamp : Utilisation d'un filtre LDLLV avec des clés étrangères multiple est pour l'insant impossible  (champ $this->NmChamp, table $this->NmTable)");
				$valbrut = explode(';', $this->Valeurs);
				if (count($valbrut)>1) { // connection a une base differente
					/// TODO A gérer
					$lntable = $valbrut[1];
				} else { // normal
					$lntable = $valbrut[0];
				}
				// gestion condition AND depuis PYA
				$valb2 = explode("[[", $lntable);
				if (count($valb2) > 1) { // il ya une condition dans la chaine valeurs
					$lntable = $valb2[0];
					$this->Val2 = $this->Val2 != "" ? "(".$this->Val2." AND ".$valb2[1].")" : $valb2[1];
				}
				// 0: table, 1: champ(s) lié(s)(clé, éventuellement multiple); 2: ET SUIVANTS champs à afficher donc dans notre cas on s'en tappe
				$defl = explode(',', $lntable);	
				$val2spcllv = "(".$defl[1]." IN (SELECT distinct $this->NmChamp from $nmtable $whsup)) ";
			
			case "LDL": // Liste deroulante affichant les differentes valeurs liees par le champ valeurs
				$retVal = dispInpHid('tf_'.$this->NmChpHtml,  $this->TypeAff=="LDLM" ? "LDM_SPL" : "LDMEG" );
				$tabval = ttChpLink($this->Valeurs, $val2spcllv ? $val2spcllv : $this->Val2);
				if (!is_array($tabval)) $tabval = array(); // ptit bug si la liste est vide
				$tbjok['%'] = (!$valDjFilt ? VSLD : "").$this->trad("all"); 
				$tabval = $tbjok + $tabval;
				// asort($tabval); // pas de tri de la liste (sppecifie dans la liaison)
/*				if ($valDjFilt) { // filtre mémorisé, et pas bidon /// Maintenant on le fait dans la constitution de la liste déroulante
					foreach ($tabval as $key=>$val) { // re-parcourt le tableau pour mémoriser les sélectionné
						$tabval[$key]=(is_arr_in_array($key,$_SESSION['memFilt']['rq_'.$this->NmChpHtml]) && $key!='0' ? VSLD : "").$val;
					}
				}
				$retVal.=DispLD($tabval,"rq_".$this->NmChpHtml,$choixMult,$this->Fccr,false); */
				$retVal .= dispLD2($tabval, "rq_".$this->NmChpHtml, array("multiple" => $boolMult, //s'il est multiple ou non (non par défaut)
													 "force_type" => $this->Fccr, "title" => $this->title,
													 "tbclesel" => $_SESSION['memFilt']['rq_'.$this->NmChpHtml])); // tableau des clés sélectionnées
				break;
				
			case "FPOPL":
				$retVal = dispInpHid('tf_'.$this->NmChpHtml,  $this->TypeAff=="LDLM" ? "LDM_SPL" : "LDMEG" );
				if (is_array($_SESSION['memFilt']['rq_'.$this->NmChpHtml]) && count($_SESSION['memFilt']['rq_'.$this->NmChpHtml]) > 0) {
					foreach ($_SESSION['memFilt']['rq_'.$this->NmChpHtml] as $vs) $tabVS[$vs] = $vs;
				} else $tabVS =  array("%"=>"%");
				$retVal .= dispLD2($tabVS, "rq_".$this->NmChpHtml, array("multiple" => $boolMult, 
														"force_type" => "LDF",
														"title" => $this->title,
														"allselected" => true, // force toutes les options a etre sélectionnées (ben oui ça sert)
														"size" => count($tabVS)));
				// pour la compat avec gdp2 multi-onglets dans FF et bientot PYA
				if (isset($_REQUEST['SESSION_NAME'])) $sessnreq = "&SESSION_NAME=".$_REQUEST['SESSION_NAME'];
				// Ca c'est un peu sioux : on autorise maintnant aussi les popup de recherche à chercher dans le champ lui même dans sa table 
				$Valeurs = empty($this->Valeurs) ? $this->NmTable.','.$this->NmChamp.','.$this->NmChamp : $this->Valeurs;
				$url2load = $this->chempopl.$this->nmfpopl.'?Valeurs='.urlencode($Valeurs).'&NmChp='.urlencode("rq_".$this->NmChpHtml).'&Mult='.($choixMult ? 10 : 1).$sessnreq;
				$retVal .= '<input type="button" class="fxbutton" onclick="showLimKnifePop(\''.$url2load.'\')" value=" S " />';
				//$retVal .= '<input type="button" class="fxbutton" onclick="popup(\''.$this->chempopl.$this->nmfpopl.'?Valeurs='.urlencode($this->Valeurs).'&NmChp='.urlencode("rq_".$this->NmChpHtml).'&Mult='.($choixMult ? 10 : 1).$sessnreq.'\',500,500)" value=" M " />';				
				
				break;
				
			case "DEG":
			case "DANT": // criteres sur date anterieure ou posterieure,
			case "DPOST":
				$classinp = $this->dateInpAttribs;
			case "VINF": //valeurs inf et sup
			case "VSUP":				
				switch ($this->TypFilt) {
					case "DANT": 
						$clause = $this->trad("ante");
					break;
					case "DPOST":
						$clause = $this->trad("poste");
					break;
					case "VINF":
						$clause = $this->trad("inf");
					break;
					case "VSUP":
						$clause = $this->trad("sup");
					break;
				}
				$retVal = dispInpHid('tf_'.$this->NmChpHtml,  $this->TypFilt);
				if ($bDL) $retVal.= $this->trad("Enrpq").$this->Libelle.$this->trad("is").$clause.$this->trad("agrave");
				$retVal .= dispInpTxt('rq_'.$this->NmChpHtml, $_SESSION['memFilt']['rq_'.$this->NmChpHtml], array("oth_att" => $classinp, "title" => $this->title));
				break;
				
			case "DATAP": // criteres sur date antérieure ET postérieure
			case "VIS" : // bornes sup et inf
				if ($this->TypFilt == "DATAP") {
					$classinp = $this->dateInpAttribs;
					$lbi = $this->trad("poste");
					$lbs = $this->trad("ante");
				} else {
					$lbi = $this->trad("sup");
					$lbs = $this->trad("inf");
				}
				$retVal = dispInpHid('tf_'.$this->NmChpHtml,  $this->TypFilt);
				// on construit les input seules
				$f0 = dispInpTxt('rq_'.$this->NmChpHtml.'[0]', $_SESSION['memFilt']['rq_'.$this->NmChpHtml][0], array("oth_att" => $classinp, "id" => 'rq_'.$this->NmChpHtml.'0'));
				$f1 = dispInpTxt('rq_'.$this->NmChpHtml.'[1]', $_SESSION['memFilt']['rq_'.$this->NmChpHtml][1], array("oth_att" => $classinp, "id" => 'rq_'.$this->NmChpHtml.'1'));
				if ($bDL) { // $bDL boolenn Display Baratins
					$retVal.=  $this->trad("Enrpq"). $this->Libelle . $this->trad("is")." <br/>$lbi".$this->trad("agrave").$f0.
						"<br/> ".$this->trad("and")." $lbs ".$this->trad("agrave").$f1;
				} else 
					$retVal.= $this->Libelle." &gt;  ".$f0.$this->trad("and")." &lt; ".$f1;
			break;
			
			case "NOTNUL":
				$retVal = dispInpHid('tf_'.$this->NmChpHtml,  $this->TypFilt);
				$retVal .= echCheckBox('rq_'.$this->NmChpHtml, 'y', false, $_SESSION['memFilt']['rq_'.$this->NmChpHtml],array("title" => $this->title));
				$retVal .= $this->trad("notnul");
				$bDN = false;
			break;
		
			default:
				$retVal = "&nbsp;";
				$bDN =false;
			break;
		} // fin switch sur type de filtre
		// negation
		if ($bDN) $retVal .= $this->DispNeg(false);

		return ($this->retOrEch($retVal));
	} // fin methode EchoFilt
	
	/**  methode d'affichage de la case de négation
	@param $bDE2 bool force non echo
	*/
	function DispNeg($DE2 = true) {	
		return ('<br/><span class="FdR">'.echCheckBox('neg_'.$this->NmChpHtml, 'neg', false, $_SESSION['memFilt']['neg_'.$this->NmChpHtml]).$this->trad("neg")."</span>");
	}
	
	/**  methode d'affichage de la case pour affichage optionnel du champ
	@param $dl bool affiche le baratin
	*/
	function EchoCSA($dl = false) {
		switch ($this->TypCSA) { // type d'affichage selectionnable
			case "OCD":
			case "ONCD":
				$nmvarAfC = "AfC_".$this->NmChpHtml;
				$retVal .= dispInpHid($nmvarAfC,  'no'); // obligatoire, sinon on sait pas qd la case a été décochée
				if (isset($_SESSION[$nmvarAfC]) || isset($_SESSION['memFilt'][$nmvarAfC])) {
					$cs= $_SESSION[$nmvarAfC]=="yes" || $_SESSION['memFilt'][$nmvarAfC]=="yes";
				} else $cs = $this->TypCSA == "OCD";
				$retVal.= echCheckBox($nmvarAfC, 'yes', false, $cs).($dl ? $this->trad("2disp") : "");
			break;
			
			default:
				$retVal.= "&nbsp;";
			break;
		} // fin switch sur affichage oui ou non
		return ($this->retOrEch($retVal));
	} // fin methode EchoCSA

	/**  methode d'affichage d'un champ en liste
	*/
	function EchoVCL($caraft = "\n") {
		if ($this->DirEcho) { echo $this->RetVCL().$caraft;} else return $this->RetVCL().$caraft;
	}
	
	/**  methode de calcul d'un champ en liste
	TODO c'est là qu'il faudra bricoler pour pouvoir y mettre des cases à cocher par ex.
	@param $dhtml bool display html (car sert aussi dans les téléchargements de fichiers)
	*/
	function RetVCL($dhtml = true) {
		$this->checkNmChp4EditHtml(); /* regenere les noms de champs html et 4 edit. Rappelé a chaque appel de Edit pour compatibilité */
		$EiS = $dhtml ? "&nbsp;" : ""; // espace insecable  HTML ou rien
		if ((int)$_SESSION['NoLang'] == 0 && ($this->TTC == "date" || stristr($this->NmChamp,"_DT"))) {
			$this->ValChp = DateF($this->ValChp); // convertit dates
			if (empty($this->ValChp)) $this->ValChp = ""; // n'affiche pas 0/0/00
		}
		if ($this->TTC == "datetime") $this->ValChp = DateTimeF($this->ValChp); // convertit dates times
		if ($this->TypeAff == "HR_MN") $this->ValChp = DispHRMN($this->ValChp);
 	
		switch ($this->Typaff_l) {
			case "LNK": // si champ lie a une autre table, ou liste compose
				return $this->RVLS($EiS);  //  retourne valeur statique (non editable) lie,  externe ou pas
			break;
			
			case "AHREF": // si lien HTML
				if ($this->ValChp=="") {
					return $EiS;
				} else {
					
					if (strpos($this->Valeurs, "#") !== 0 && $this->Valeurs != "") {
						$ChemFF = $this->Valeurs;
					} else {
						$ChemFF = "./".$this->NmBase."_".$this->NmTable."_".$this->NmChamp."/";
					}
					if ($dhtml) {
						return "<A HREF=\"".$ChemFF.$this->ValChp."\" ".(TestNFImg($this->ValChp) ? "target=\"_blank\"" : "").">".htmlspecialchars($this->ValChp)."</A>";
					} else 
						return $this->trad("fat").$ChemFF.$this->ValChp;
				}
			break;
			
			case "DT_TS":
				return date("d/m/Y", $this->ValChp);
			break;
		
			case "DTHMS_TS":
				return date("d/m/Y H:i:s", $this->ValChp);
			break;
		
			default: 
				if ($dhtml && $this->TypeAff == "CKBX") {
					$defl = explode(',', $this->Valeurs);
					$val0 = explode(':', $defl[0]); 
					return echCheckBox($this->NmChpHtml, htmlspecialchars($val0[0]), false, $this->ValChp == $val0[0],"",true); // .tradLib($val0[1]) met plus le libellé qui en entete
				}
				// nbre de car max par defaut, ou passe en propriete
				$NbCarMax = $this->NbCarMxCust>0 ? $this->NbCarMxCust : $GLOBALS['nbcarmxlist'];
				$Val2Af =  $this->ValChp;
				$Val2Af = stripslashes($Val2Af);
				if ($NbCarMax<=50) $Val2Af =  str_replace("\n","",strip_tags($Val2Af)) ; // si peu de caractères autorisés, on affiche pas le HTML ni les sauts de ligne
				$Val2Af = tronqstrww ($Val2Af,$NbCarMax); // + malin, tronquature qui coupe pas les mots
				if ($dhtml) { 
					$Val2Af = DispCustHT($Val2Af);
					$Val2Af = ($Val2Af=="" ? $EiS : $Val2Af);
				} else 
					$Val2Af = strip_tags(htmlspecialchars_decode($Val2Af)) ;
				return ($Val2Af);
			break;
		} // fin du switch
	} // fin methode EchoVCL

	/** methode commune aux 2 methodes liste et edition, qui affiche les valeurs liées en statique
	@param $EiS str code de l'espace insécable
	@param $rtb retourne une chaine ou un tableau
	*/
	function RVLS($EiS = "&nbsp;",$rtb = false) {
		// LnkTb=true si liste liée à une table, false si liste de valeurs fixes
		$LnkTb = ! ($this->TypeAff=="LD" || $this->TypeAff=="LDM" || $this->TypeAff=="CKBX"); // lien a une autre table
		if (!$LnkTb) { // si liste de valeurs, construit directement un tableau de hachage cle=>valeurs
			$defl = explode(',', $this->Valeurs);
			foreach ($defl as $val) {
				if (strstr($val, ":")) {
					$valtb = explode(":", $val);
					$key = $valtb[0];
					$val = $valtb[1];
				}
				else $key=$val;
				$tbval[$key] = tradLib($val);
			}
		} // fin si valeurs statiques
		$cle = $this->ValChp;
		$valaff = array();
		if ($cle != "") {
			$tabcle = explode(",",$cle);
			foreach ($tabcle as $cle) {
				if ($cle != "") { // ignore valeurs vides
					if ($LnkTb) {
						$valsuiv = ttChpLink2($this->Valeurs,$cle);
					} else $valsuiv = $tbval[$cle];
					$valaff[$cle] = $valsuiv ? trim($valsuiv): "NC[$cle]"; // affiche si trouve, ou erreur NC[]
				} // fin si valeur non vide
			}
		} // fin si $cle!=""
		if ($rtb) { // si retour format tableau
			return($valaff);
		} else {
			return (count($valaff) > 0 ? strip_tags(implode(", ",$valaff), '<span>') : $EiS );
		}
	} // fin fonction RVLS

	/** methode qui affiche le   controle d'edition d'un champ en html
	// on a separe auto (fonction du type de champ) et manuel
	@param $dhid bool affiche les champs cachés
	*/
	function EchoEditAll($dhid = true) {
		if ($this->TypeAff!="AUT") {
			return($this->EchoEdit($dhid));
		} else return($this->EchoEditAuto());
	}	
	/** methode qui renvoie un bout de JS dans le cadre des contrôles de validité des champs
	AINSI que des classes pour le jQuery*/
	function genEditJS() {
		if ($this->Tt_PdtMaj == "") return;
		/// ceci est géré maintenant en jquery, en passant juste un style; voir initjqdl3.js et shared_in.js.php
		if (array_key_exists($this->Tt_PdtMaj,$GLOBALS['tbEvenmtVFAutoJS'])) { // $GLOBALS['tbEvenmtVFAutoJS'] tableau des evenements auto gérés; stocké ds fonctions_pya.php
			/* ancienne méthode incremente nbre de champs a tester
			return(outJS("nbInput2test ++;
			tbId2Verif[nbInput2test] = '".$this->NmChpHtml4Edit."';
			tbTypeVerif[nbInput2test] = '".$this->Tt_PdtMaj."';
			tbLibChp2Verif[nbInput2test] = '".$this->Libelle."';
			"));*/
			/* plugin validate de jquery-ui n'est pas mûr...
			if ($GLOBALS['jquery-ui']) { // dans le cas du jquery-ui (DeProj), on utilise le plugin validate
				//tableau de correspondance des codes déjà existants => les codes utilisés par le plugin jquery validate
				$tbCorr['notNull'] = 'required';
				if ($tbCorr[$this->Tt_PdtMaj]) {
					$this->inpclass = $tbCorr[$this->Tt_PdtMaj];
				} else $this->inpclass = $this->Tt_PdtMaj;
			} else */
				$this->inpclass = $this->Tt_PdtMaj;
		} elseif ($this->Tt_PdtMaj != "" && !array_key_exists($this->Tt_PdtMaj,$GLOBALS['tbEvenmtVFAutoJS']) && stristr($this->Tt_PdtMaj, ":")) { // custom
			$tbeven = explode(":", $this->Tt_PdtMaj);
			$this->evJS = " ".$tbeven[0].'="'.$tbeven[1].'" '; // rajout d'un onClick="toto()";
		} elseif (!strstr($this->Tt_PdtMaj, ":")) { // si pas de :, ie que ce n'est pas un js spécifié
		/// pour jquery a terme on garde que ça; on pourra avoir plusieurs traitements pendant la maj sélectionnables
			$this->inpclass = $this->Tt_PdtMaj;
		}
	}
	// fonction qui regarde si un champs est obligatoire dans les valeurs de $this->Tt_PdtMaj
	function testOblig ($vret = "") {
		if (($this->Tt_PdtMaj =="notNull" || stristr($this->Tt_PdtMaj, "NN")) && $this->TypEdit != "C")  return ($vret ? $ $vret : '<span title="Champ a renseigner obligatoirement" class="notNullField">*</span>&nbsp;');
	}
	
	/** mehode qui affiche le controle d'edition
	// pour les les type d'aff <> de AUT  
	@param $dhidden bool affiche les champs cachés
	*/
	function EchoEdit($dhidden=true) {
		$this->checkNmChp4EditHtml(); /* regenere les noms de champs html et 4 edit. Rappelé a chaque appel de Edit pour compatibilité */
		$dhtmSpcChr = true; // épuration du html par défaut en TypEdit= C
		$retVal .= $this->genEditJS(); // crache du JS pour notamment les champs obigatoires
		$retVal .= $this->testOblig(); // met un style qui va bien
		if ($this->TTC == 'date') { // champs date
			$this->ValChp = DateF($this->ValChp);
			if ($this->ValChpAvMaj!="") $this->ValChpAvMaj = DateF($this->ValChpAvMaj);
			if ((int)$this->ValChp == 0) $this->ValChp = ""; // n'affiche pas 0/0/00
		} elseif ($this->TTC == 'datetime') { // champs datetime
			$this->ValChp = DateTimeF($this->ValChp);
			if ($this->ValChpAvMaj!="") $this->ValChpAvMaj = DateTimeF($this->ValChpAvMaj);
		} else { 
			if ($this->TypeAff != "HID") $this->ValChp = stripslashes($this->ValChp);
		}
		if ($this->title == "") $this->title = $this->Libelle;
		if ($this->TypEdit == "C") { // si on est en consultation, on passe tout en STA ou STAL ou rien
			if ($this->TTC == "date" && ($this->ValChp == "00/00/2000" || (strstr($this->ValChp,"/") && (int)$this->ValChp == 0))) $this->ValChp = "-";
			switch ($this->TypeAff) {
			//chgt en statique normale
				case "TXARTE":
					$dhtmSpcChr = false; // (surtout) pas d'épuration du html
				case "AUT": // AUto
				case "TXT": // boite texte normale
				case "LDTXT": // boite texte + liste déroulante
				case "TXA": // texte area
				case "TXARTE":
					$this->TypeAff = "STA";
				break;
			
				case "PASSWD": // 
					$this->TypeAff = "HID";
				break;
			
				case "DT_TSTAMP_ED" :
					$this->TypeAff = "DT_TSTAMP_VS";
				break;
	
				case "HR_MN":
					$Val2Af = DispHRMN($this->ValChp);
					$retVal .= $Val2Af.($dhidden ? dispInpHid($this->NmChpHtml4Edit, $this->ValChp) : "");
				/// on sort direct, pour ne pas modifier la valeur de ->TypeAff; si on le fait, comme on rappelle deux fois la methode (heure/minute) pour le meme champ ds une page, ca plante
					return ($this->retOrEch($retVal));
				break;
	
				
		//chgt en statique normale force (on recherche les paires cle: valeurs dans this->Valeurs
				case "LD": // liste deoulante non liée
				case "LDM": // liste deroulante non lie MULTIPLE
				case "LDL":
				case "LDLM":
				case "POPL":
				case "POPLM":
				case "JQCTRL":
				case "JQCTRLM":
					$Val2Af = DispCustHT($this->RVLS()); // renvoie valeurs en lecture seule pr les listes
					$retVal .= $Val2Af.($dhidden ? dispInpHid($this->NmChpHtml4Edit, $this->ValChp) : "");
					/// on sort direct, pour ne pas modifier la valeur de ->TypeAff; si on le fait, comme on rappelle deux fois la methode (heure/minute) pour le meme champ ds une page, ca plante
					return ($this->retOrEch($retVal));
				break;
			// le reste on change pas, les photos sont traites dans la proc elle-meme
			} // fin switch du typeAff
		}   // fin si on est en consultation
	
		switch ($this->TypeAff) {
			case "": // on affiche rien
			case "NULL":
			break;
			
			case "HID": // on affiche un champ caché que si $dhidden=true, sinon rien
				if ($dhidden) $retVal.= dispInpHid($this->NmChpHtml4Edit, htmlspecialchars($this->ValChp));
			break;
	
			case "CKBX": //check box
				$defl = explode(',',$this->Valeurs);
				$val0 = explode(':',$defl[0]);
				$retVal = echCheckBox($this->NmChpHtml4Edit, $val0[0], false, $this->ValChp == $val0[0], "", $this->TypEdit == "C").tradLib($val0[1]);
			break;
	
			case "LD": // liste deroulante non liee
			case "LDTXT": // liste déroulante + texte
//	POURRI	TODO	if ($this->FieldNullOk == "YES") {  // si null autorise
//					$val =  $this->ValChp == "NULL" ? VSLD."NULL" : "NULL";
//					$tbval["NULL"] = $this->trad("null");
//				}
				$tbval = valValc2tb ($this->Valeurs, $this->ValChp);
				if ($this->TypeAff == "LDTXT") {
						$retVal .= DispLDandTxt ($tbval,$this->NmChpHtml4Edit, $this->ValChp, false);
				} else { // liste "normale"
					if (!array_key_exists($this->ValChp, $tbval) && $this->ValChp != "") {
						$retVal .= str_replace("###",  $this->ValChp, $this->trad("err6"));
					}
					$retVal .= dispLD2($tbval, $this->NmChpHtml4Edit, array("multiple" => false, "force_type" => $this->Fccr, "class" => $this->inpclass, "title" => $this->title)); // liste der. non multiple
				}
			break;
	
			case "LDM": // liste deroulante non liée MULTIPLE
				$defl = explode(',',$this->Valeurs); // definition des valeurs
				$tbvalc = explode(',',$this->ValChp); // valeur courante du champ
				$tbval =  valValc2tb ($defl, $tbvalc);
				//echo ($this->ValChp) ;print_r($tbvalc);
				//if ($this->Fccr=="" || $this->Fccr=="LDF") $tbval["NULL"] = $this->trad("NONE"); // valeur vide les mecs ont qu' a rien selectionner/cocher
				// controle erreur, que toutes les valeurs du champ existent dans la liste définie		
				foreach ($tbvalc as $valm) {
					if (!array_key_exists($valm,$tbval) && $valm != "") $err .= $valm.", ";
				}
				if ($err!="") {
					$err = vdc($err,2); // enleve dernier", ";
					$retVal .= str_replace("###", $err, $this->trad("err7"));
				}
				$retVal .= dispLD2($tbval, $this->NmChpHtml4Edit, 
						array("multiple" => true, 
						"force_type" => $this->Fccr, 
						"class" => $this->inpclass, 
						"title" => $this->title)); // liste der. multiple
			break;
			
			case "LDL": // liste deroulante lie
			case "LDLM": // liste deroulante liée MULTIPLE
				$tabCorlb = ttChpLink($this->Valeurs, $this->Val2);
				if (strstr($this->Valeurs,"@@")) {  // si hierachique
					$valnul = $this->TTC == "char" ? "" : 0;
					$valns = $this->ValChp == $valnul ? VSLD : '';
					$tbval[$valnul] = $valns.$this->trad("none").$this->trad("oris");
				} elseif ($this->FieldNullOk == "YES") {  // si null autorise
					$valns = ($this->ValChp == "NULL" || $this->ValChp == "" || $this->ValChp == 0) ? VSLD : ""; // bordel avec Mysql
					$tbval["NULL"] = $valns.$this->trad("none");
				} else $tbval = array();
				$tbvalc = explode_nn(",",$this->ValChp);
				// test que les valeurs duc champ sont bien dans la liste
				if (!array_keys_exists($tbvalc, $tabCorlb) && $this->ValChp!="") $retVal.= str_replace("###",  $this->ValChp, $this->trad("err7"));
				$retVal .= dispLD2 ($tbval + $tabCorlb, $this->NmChpHtml4Edit, 
						array("multiple" => ($this->TypeAff == "LDLM" ? true : false), 
						"force_type" => $this->Fccr, 
						"tbclesel" => $tbvalc, 
						"class" => $this->inpclass,
						"title" => $this->title));
			break;
	
			
			$Mult=false;
			case "POPLM": // popup de selection
				$Mult=true;
			case "POPL": // popup de selection
				$tabVS = $this->RVLS("",true); // renvoie les valeurs liées sous forme de tableau  
				if (count($tabVS) == 0) $tabVS = array("-");
				$retVal .= dispLD2($tabVS, $this->NmChpHtml4Edit, array("multiple" => $Mult, 
														"force_type" => "LDF",
														"class" => $this->inpclass,
														"title" => $this->title,
														"allselected" => true, // force toutes les options a etre sélectionnées (ben oui ça sert)
														"size" => $Mult ? max(1,count($tabVS)) : 1));
				// pour la compat avec gdp2 multi-onglets dans FF et bientot PYA
				if (isset($_REQUEST['SESSION_NAME'])) $sessnreq = "&SESSION_NAME=".$_REQUEST['SESSION_NAME'];
				$url2load = $this->chempopl.$this->nmfpopl.'?Valeurs='.urlencode($this->Valeurs).'&NmChp='.urlencode($this->NmChpHtml4Edit).'&Mult='.($Mult ? 10 : 1).$sessnreq;
				//$retVal .= '<input type="button" class="fxbutton" onclick="popup(\''.$this->chempopl.$this->nmfpopl.'?Valeurs='.urlencode($this->Valeurs).'&NmChp='.urlencode($this->NmChpHtml4Edit).'&Mult='.($Mult ? 10 : 1).$sessnreq.'\',500,500)" value=" M " />';
				$retVal .= '<input type="button" class="fxbutton" onclick="showLimKnifePop(\''.$url2load.'\')" value=" S " />';
			break;
			
			/*
			 * JQCTR : n'est pas paramétrable direcetement dans les écrans pya. Conçu pour être utilisé pour l'instant directement dans une appli
			 * Voir php_commun/fich_proj_dialog.php dans DeProj
			 */
			$Mult=false;
			case "JQCTRLM": // jquery contrôle multiple
				$Mult=true;
			case "JQCTRL": // jquery contrôle
				$nc = $this->NmChamp;
				$retVal = '
				<div class="jqctrl">
					<input type="text" id="txt2s'.$nc.'" style="color:grey" value="Rechercher ...">
					<div id="ress'.$nc.'"></div>
					<div>';
				if (!$Mult) {
					$retVal .= '<input type="hidden" name="'.$this->NmChpHtml.'" id="'.$nc.'"  value="'.$this->ValChp.'"/><span id="valtxt'.$nc.'"> '.$this->EchoVCL().'</span>';
				} else {
					$tabVS = $this->RVLS("",true); // renvoie les valeurs liées sous forme de tableau  
					if (count($tabVS) == 0) $tabVS = array("-");
					$retVal .= dispLD2($tabVS, $this->NmChpHtml,  array("multiple" => true, //s'il est multiple ou non (non par défaut)
																	"force_type" => "LDF", // force  les cases à cocher ou boutons radio (=RAD) ou liste deroulante (=LDF), ou checkBoxes (=CKBX) qqsoit le nbre de valeur; on peut mettre RADBR ou CKBXBR pour forcer les retour à la ligne
																	"tbclesel" => array(), // tableau des clés sélectionnées
																	"allselected" => true, // force toutes les options a etre sélectionnées (ben oui ça sert)
																	"id" => "", // id html
																	"size" =>  3, // SIZE= html
																	"title" => $this->title, // titre html 
																	"class" => $this->inpclass,
																	"style" => $this->title,
																	"oth_att" => "",
																	"dir_echo" => false, // retourne ou echoise direct
																	"novalues_msg" => "<i>Aucune valeur </i>£<i>No value </i>"
																	));
					$retVal .= ' <button id="delitem" type="button">-</button>';
					$retVal .= outJS('$(function() {
							$("#delitem").click( function () {
								$("#'.$this->NmChpHtml.' option:selected").each( function() {
									$(this).remove();
								});
							});
							$("#'.$this->dialFormId.'").submit(function() {
								$("#'.$nc.' option").prop("selected", true);
//								$("#'.$nc.' option").each(function () {
//									$(this).attr("selected", "selected");
//								});
							});
						}); // fin doc ready
					');
				}	
				$retVal .= '	</div>
				</div>';
				$retVal .= outJS('$(function() {
					// efface la val courante des qu on clique, mais que le premier coup
					first'.$nc.' = true;
					$("#txt2s'.$nc.'").click( function() {
						if (first'.$nc.') {
							$("#txt2s'.$nc.'").val("");
							first'.$nc.' = false;
						}
					});

					// met à jour la LD en ajax dès qu\'on tappe pkus de 3 caract ds la boite de rech
					// ld_ajax_dyn.php doit être défini dans l\'appli, et faire une connexion BDD puis une inclusion de php_inc/gen_ldajax4jqctrl.php
					var ajaxurl="ld_ajax4jqctrl.php?chp_lnk='.urlencode($this->Valeurs).($_REQUEST['SESSION_NAME'] ? "&SESSION_NAME=".$_REQUEST['SESSION_NAME'] : "").'&nmchp='.$nc.($Mult ? '&Mult=1' : '').'&txt2srch=";	
					$("#txt2s'.$nc.'").keyup( function() {
						var valcs = $("#txt2s'.$nc.'").val();
						if (valcs.length >= 2) {
							$("#ress'.$nc.'").load(ajaxurl + valcs );
						}
					});
				}); // fin doc ready
				');
			break;
			
					
			case "FICFOT":
				//if ($this->TypEdit==2) $this->ValChp=""; // annule si copie
				$retVal .= '<div class="pyafichctrl">';
				$retVal .= dispInpHid("Old".$this->NmChpHtml4Edit, $this->ValChp);
				if ($this->ValChp!="") {  
					$ChemFF = $this->Valeurs != "" ? $this->Valeurs : "./".$this->NmBase."_".$this->NmTable."_".$this->NmChpHtml4Edit."/"; // regarde si chemin parametre correcetent
					$ChemFF .= $this->ValChp;
					if (TestNFImg($this->ValChp)) { // si c'est un fichier de type image et qu'il est pas trop gros
						if (filesize($ChemFF)<= $this->MaxImgSize2display) {
							$retVal.= '<div class="img4pyafich"><IMG SRC="'.$ChemFF.'" '.($this->MaxImgWidth > 0 ? ' width="'.$this->MaxImgWidth.'" ' : "").'/></div>';
						} else $retVal .= '<div class="pyafichcmt"><i>'.$this->ValChp.'</i> : '.$this->trad("img2big").' ('.round(filesize($ChemFF) / 1024).'k)</div>';
					} 
					$retVal.= ($this->bDL ? $this->trad("linktof")." : " : "" ).dispLink($ChemFF, $this->ValChp, array("title" => "Charger le fichier", "target" => "_blank"))."<br/>";
		
					if ($this->TypEdit != "C") $retVal.= echCheckBox("Fok".$this->NmChpHtml4Edit, $this->ValChp, false, true,$id="", $disabled=false, $class="", $this->trad("uncktodel")).$this->ValChp."&nbsp;";
				}// Fin si il y a un fichier associe
				if ($this->TypEdit != "C") { // saisie que si pas en edition
					$retVal.= "<INPUT TYPE=\"file\" NAME=\"".$this->NmChpHtml4Edit."\" SIZE=\"30\" MAXLENGTH=\"200\">";
				if ($this->ValChp!="") $retVal.= '<div class="pyafichcmt" style="font-size:85%">'.$this->trad("mes1")."</div>";
				$retVal .= '</div>';
			}
			break; // fin champ de type fichier-photo
	
			case "STAF": // statique force
				// on ne fait rien, on l'a deja fait au début
			break;
	
			case "STA": // statique  simple
				// remplace caracteres merdiques et convertit adresses mail en cliquables pour le HTML
				$retVal = DispCustHT($this->ValChp,$dhtmSpcChr);
				// si nouvelle valeur (MAJ automatique, style date du jour par ex.) on affiche plus la valeur courante, on s'en branle
				if ($this->ValChpAvMaj!="" && $this->ValChpAvMaj!=$this->ValChp) $retVal = DispCustHT($this->ValChpAvMaj); 
				$retVal.= dispInpHid($this->NmChpHtml4Edit, htmlspecialchars($this->ValChp));
			break;
	
			case "STAL": // statique liée
				$retVal.= ttChpLink($this->Valeurs,"",$this->ValChp);
				// on affiche plus la valeur courante, on s'en branle
				if ($this->ValChpAvMaj!="" && $this->ValChpAvMaj!=$this->ValChp) $retVal = ttChpLink($this->Valeurs,"",$this->ValChpAvMaj); 
				$retVal.= dispInpHid($this->NmChpHtml4Edit, strip_tags($this->ValChp));
			break;
	
			case "TXARTE":
				$class = "jqrte2";
			case "TXA":
				$defl = explode(',',$this->Valeurs);
				// pour les dimensions, on prend les chiffres dans le champ valeurs (nblignes, nbcolonnes)
				$ligs = ((float)$defl[0] > 0 && !preg_match("`(em|px|%)`i", $defl[0])) ? $defl[0] : $GLOBALS['nbrtxa'];
				$cols = ((float)$defl[1] > 0 && !preg_match("`(em|px|%)`i", $defl[1])) ? $defl[1] : $GLOBALS['nbctxa'];
				// dans le cas des TXARTE il faut des dim css sinon ça déconne
				if ($class == "jqrte2") {
					if ($defl[0] > 0 && !preg_match("`(em|px|%)`i", $defl[0])) $defl[0] =  ($defl[0] * 15)."px"; // calcul pifométrique : hauteur d'une ligne ~ 15px
					if ($defl[1] > 0 && !preg_match("`(em|px|%)`i", $defl[1])) $defl[1] =  ($defl[1] * 7)."px"; // calcul pifométrique : largeur d'une colonne ~ 7px
				}
				$height = ($defl[0]>0  && preg_match("`(em|px|%)`i", $defl[0])) ? 'height:'.$defl[0] : "";
				$width = ($defl[1]>0 && preg_match("`(em|px|%)`i", $defl[1])) ? 'width:'.$defl[1]  : "";
				$retVal .= dispTxtArea($this->NmChpHtml4Edit, $this->ValChp, $othparams = array( 
													 "cols" => $cols, // SIZE= html
													 "rows" => $ligs, // SIZE= html
													 "title" => $this->title, // titre html 
													 "class" => $this->inpclass." ".$class,
													 "style" => $height.';'.$width,
													 ));
			break;
	
			case "DT_TSTAMP_ED":
				$retVal .= dispInpTxt($this->NmChpHtml4Edit, date("d/m/Y",$this->ValChp), array("oth_att" => $this->dateInpAttribs." ".$this->evJS, "class" => $this->inpclass,"title" => $this->title));
				if ($this->ValChpAvMaj!="" && $this->ValChpAvMaj!=$this->ValChp) $retVal.= " (".$this->trad("avtmaj").": ".date("d/m/Y",$this->ValChpAvMaj).") ";
			break;
			
			case "DT_TSTAMP_VS":
				$retVal.= date("d/m/Y",$this->ValChp);
				$retVal.= dispInpHid($this->NmChpHtml4Edit, date("d/m/Y",$this->ValChp));
				if ($this->ValChpAvMaj!="" && $this->ValChpAvMaj!=$this->ValChp) $retVal.= " (".$this->trad("avtmaj").": ".date("d/m/Y",$this->ValChpAvMaj).") ";
			break;
			
			case "DTHMS_TSTAMP_VS":
				$retVal.= date("d/m/Y H:i:s",$this->ValChp);
				$retVal.= dispInpHid($this->NmChpHtml4Edit, $this->ValChp);
				if ($this->ValChpAvMaj!="" && $this->ValChpAvMaj!=$this->ValChp) $retVal.= " (".$this->trad("avtmaj").": ".date("d/m/Y H:i:s",$this->ValChpAvMaj).") ";
			break;
			
			case "HR_MN":
				$retVal.= dispInpTxt($this->NmChpHtml4Edit."_hr", floor($this->ValChp/100), array("size" => 2, "maxlength" => 2, "oth_att" => $this->evJS  ));
				$retVal.= " : ".dispInpTxt($this->NmChpHtml4Edit."_mn", c2c($this->ValChp % 100), array("size" => 2, "maxlength" => 2, "oth_att" => $this->evJS ));
			break;
				
			case "DATEF":
				$classi = "date";
			case "TXT":
			case "PASSWD":
				$defl = explode(',',$this->Valeurs); // size,maxlength
				if ((float)$defl[0] == 0) $defl[0] = ""; // size doit etre 1 val numériques
				if ((float)$defl[1] == 0) $defl[1] = ""; // maxlength doit etre 1 val numériques
			default :
				if (stristr($this->NmChpHtml4Edit, "_DT") || $classi == "date") {
					$classi = $this->dateInpAttribs;
					unset($defl);
				}
				$retVal .= dispInpTxt($this->NmChpHtml4Edit, htmlspecialchars($this->ValChp), 
						array("oth_att" => $classi." ".$this->evJS , "size" => $defl[0],"maxlength" => $defl[1], "class" => $this->inpclass, "title" => $this->title, "type" => ($this->TypeAff == "PASSWD" ? "password" : 'text')));
				if ($this->ValChpAvMaj!="" && $this->ValChpAvMaj!=$this->ValChp) $retVal.= " (".$this->trad("avtmaj").": ".DispCustHT($this->ValChpAvMaj).") ";
			break;
				
		} // fin switch sur type d'affichage
		return ($this->retOrEch($retVal));
	} // fin methode EchoEdit

/**	methode qui affiche le controle d'edition
	pour le type d'aff AUTOMATIQUE uniquement */
	function EchoEditAuto() {
		$this->checkNmChp4EditHtml(); /* regenere les noms de champs html et 4 edit. Rappelé a chaque appel de Edit pour compatibilité */
		$retVal .= $this->genEditJS();
		$retVal .= $this->testOblig();
		if ($this->TTC != 'date') $this->ValChp=stripslashes($this->ValChp);
		if ($this->TypEdit == "C") { // en consult, on appelle la manuelle
			$this->TypeAff = "STA";
			return($this->EchoEdit());
		} // fin edition consult (ro)
		// edition (pas consult)
		if ($this->title == "") $this->title = $this->Libelle;
		switch ($this->TTC) { // switch sur type de champ
			case "boolean": // specifique postgres TODO faudrait peut-être rajouter un hidden quand rien n'est checké
				$retVal = echCheckBox($this->NmChpHtml4Edit, true, false, $this->ValChp=="true").tradLib($val0[1])."vrai (true)\n";
			break;
	
			case "enum" : // liste de valeurs stockées dans les caract du champ
			case "set" :
				$othparams['multiple'] = ($this->TTC == "set");
				$othparams['force_type'] = $this->Fccr;
				$othparams['tbclesel'] = explode(",", $this->ValChp);
				$othparams['title'] = $this->title;
				$othparams['class'] = $this->inpclass;
				
				foreach ($this->tbvalsetenum as $k=>$v) $tabval[$v] = $v;
				$retVal = dispLD2($tabval, $this->NmChpHtml4Edit, $othparams);
			break;
	
			case "date" : // calendrier
				$classi = $this->dateInpAttribs;
				$retVal .= dispInpTxt($this->NmChpHtml4Edit, DateF($this->ValChp), array("oth_att" => $classi." ".$this->evJS, "class" => $this->inpclass, "title" => $this->title));
				if ($this->ValChpAvMaj!="" && $this->ValChpAvMaj!=$this->ValChp) $retVal.= "(Avt MAJ : ".DateF($this->ValChpAvMaj).") ";
			break;
	
			case "datetime" : 
				$classi = $this->datetimeInpAttribs;
				$retVal .= dispInpTxt($this->NmChpHtml4Edit, DateTimeF($this->ValChp), array("oth_att" => $classi." ".$this->evJS, "class" => $this->inpclass, "title" => $this->title));
				if ($this->ValChpAvMaj!="" && $this->ValChpAvMaj!=$this->ValChp) $retVal.= "(Avt MAJ : ".DateTimeF($this->ValChpAvMaj).") ";
			break;
	
			case "txtblob" : // textarea
				$retVal .= dispTxtArea($this->NmChpHtml4Edit, $this->ValChp, $othparams = array( 
													 "cols" => $GLOBALS['nbctxa'], // SIZE= html
													 "rows" => $GLOBALS['nbrtxa'], // SIZE= html
													 "title" => $this->Comment, // titre html 
													 "class" => $this->inpclass
													 ));
			break;
	
			case "char" : // char ou varchar
				$lg = strrchr($this->FieldType, "(");
				$lg = substr($lg,1,-1); // ne recup que l'entier
				$size = min(intval($lg * 1.5),100);
			default:
				$retVal .= dispInpTxt($this->NmChpHtml4Edit, htmlspecialchars($this->ValChp), array("oth_att" => $classi." ".$this->evJS , "size" => $size,"maxlength" => $lg, "class" => $this->inpclass, "title" => $this->title));
				if ($this->ValChpAvMaj!="" && $this->ValChpAvMaj!=$this->ValChp) $retVal.= "(Avt MAJ : ".$this->ValChpAvMaj.") ";
			break;
		} // fin switch type de champ
		return ($this->retOrEch($retVal));	
	} // fin Methode EchoEditAuto

/** fonction qui effectue eventuellement des mise à jour avant l'affichage
@param $couser code du user qui doit etre mis dans les champs USMAJ USCREA, typiqueement le user du gars connecté (en session) */
	function InitAvMaj($couser = "") {
		if ($this->TypEdit == "C") return; // fait rien en consult evidemment
		unset($this->ValChpAvMaj);
		switch ($this->Tt_AvMaj) {
			case "DJ": // date du jour
				$this->ValChpAvMaj = $this->ValChp;
				$this->ValChp = (!stristr($this->TypeAff, "DT_TSTAMP") ? date("Y-m-d") : time());
			break;
			case "DJSN": // date du jour si nul
				if ($this->ValChp=="" || $this->ValChp==0) $this->ValChp = (!stristr($this->TypeAff, "DT_TSTAMP") ? date("Y-m-d") : time());
			break;
			case "DJP2MSN": // date du jour plus 2 mois si nul curieux, je sais plus à quoi ça sert
				if ($this->ValChp=="" || $this->ValChp==0) $this->ValChp = (!stristr($this->TypeAff, "DT_TSTAMP") ? date("Y-m-d", time() + 5270400) : time() + 5270400);
			break;
			case "US": // code User
				$this->ValChpAvMaj = $this->ValChp;
				$this->ValChp = $couser != "" ? $couser : ($this->TTC == "numeric" ? 0 : $GLOBALS['couserinc']);
			break;
			case "USSN": // code User si nul avant (pour les nouvelles uniquement par e.)
				if ($this->ValChp=="" || $this->ValChp==NULL || $this->ValChp=="0") {
					$this->ValChp = $couser != "" ? $couser : ($this->TTC == "numeric" ? 0 : $GLOBALS['couserinc']);
				}
			break;
			case "EDOOFT" : // edition seulement la premiere fois cad quand $this->TypEdit = N
				if ($this->TypEdit == "M") {
					if ($this->TypeAff == "LDL" || $this->TypeAff == "LDLM") {
						$this->TypeAff = "STAL";
					} else $this->TypeAff="STA";
				}
			default :
			break;
		} // fin switch sur type de MAJ avant
	} // fin methode d'init avant MAJ

	/** methode RetSet retourne un set ou un tableau prêt a être converti en sql pour les mises a jour
	* @param $keycopy une clé mise devant le nom du fichier joint éventuel (sinon risque d'ecrasement)
	* @param $rttb boolen qui indique si renvoie un tableau (true), ou un morceau de "set" sql (false)
	* cette fonction gère aussi les fichiers joints (!) (ce qui est assez sioux...)
	 * 
	* !!!! voir function PYATableMAJ dans fonctions_pya pour son utilisation !!
	 */
	function RetSet($keycopy, $rttb = false) {
		
		// gestion des traitements après mAJ, typiquement les encryption de mdp
		// fait avant le db_escape_string plus bas !!!!
		$tbTtAprMaj = explode(" ", $this->Tt_AprMaj);
		foreach ($tbTtAprMaj as $TtAprMaj) {
			if (stristr($TtAprMaj, 'Encrypt_')) {
				$enc_type = str_ireplace('Encrypt_', '', $TtAprMaj);
				//die($enc_type);
				$this->ValChp = encrypt_passwd($this->ValChp, $enc_type);
			}
		}
		// Filter values depending on treatment
		$tbTtPdtMaj = explode(" ", $this->Tt_PdtMaj);
		foreach ($tbTtPdtMaj as $TtPdtMaj) {
			switch (trim($TtPdtMaj)) {
				case 'email':
				case 'emailNN':	
					$this->ValChp = filter_var($this->ValChp, FILTER_SANITIZE_EMAIL);
					break;
				
				case 'tel':
				case 'telNN':	
					$this->ValChp = strtr($this->ValChp, ".-_/", '    ');
					$this->ValChp = preg_replace("#[^(\d )+]#", "", $this->ValChp);
					//$this->ValChp = filter_var($this->ValChp, FILTER_SANITIZE_NUMBER_FLOAT);
					break;
				
				case 'number':
				case 'numberNN':	
					$options = array('flags'=>FILTER_FLAG_ALLOW_FRACTION);
					$this->ValChp = filter_var($this->ValChp, FILTER_SANITIZE_NUMBER_FLOAT, $options);
					break;
						
				case 'puretext':
				case 'puretextNN':
					if (!is_array($this->ValChp)) {
						$this->ValChp = filter_var($this->ValChp, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
					} else {
						foreach ($this->ValChp as $ki=>$vi)	$this->ValChp[$ki] = filter_var($vi, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
					}
					//$this->ValChp = filter_var($this->ValChp, FILTER_SANITIZE_ENCODED);
					break;
			}
		}
		
		$keycopy = str_replace("'","",$keycopy);
		if ($this->MaxFSize==0 || $this->MaxFSize=="") $this->MaxFSize = $GLOBALS['MaxFSizeDef'] ;
		if ($this->TTC == "date") { // date
			$this->ValChp = DateA($this->ValChp);
		} elseif ($this->TTC == "datetime") { // datetime
			$this->ValChp = DateTimeA($this->ValChp);
		} elseif (strstr($this->TypeAff,"DT_TSTAMP")) {
			$this->ValChp = DateF2tstamp($this->ValChp);
		} elseif (is_array($this->ValChp)) { // le champ est un tableau => on vient d'un set ou d'une liste deroulante multiple (liee ou pas)
			if ($this->TTC != "set") $valset = ","; // Nouveau: avant rien
			foreach($this->ValChp as $val) {
				if ($val!="NULL" && $val!="") $valset .= db_escape_string($val).","; // on se retrouve avec ,vals1,vals2,vals3,
			}
			if ($this->TTC == "set") $valset = substr($valset, 0, -1); // enleve la derniere virgule a la fin que pour les set
			$this->ValChp = $valset; 
		} // fin si valeur est un tableau
		elseif ($this->TTC=="numeric" && count($this->tblocfkeys) <=1) {
			//$this->ValChp = (float)str_replace("'","",$this->ValChp); // astuce pour convertir en nombre si pas bon; vire les ' qui peuvent trainer sinon le +0 ne marche pas avec les nombres
			// en fait il faut virer le +0 à cause d'OSS : dans Oracle le Sep décimal est "," et si on fait +0 ça arrondit le nombre à l'entier
			$this->ValChp = str_replace("'","",$this->ValChp);
		} else { // pas tableau ni date
			$this->ValChp=db_escape_string($this->ValChp);
		}
		
		// gestion des champs type fichier joint
		if ($this->TypeAff == "FICFOT") {
			$value = $this->OFN; // OFN=OldFileName valeur du champ par defaut
			// en php5, la variable $this->ValChp=$_REQUEST[toto] contient en fait $_FILES[toto] qui est un tableau...
			if (is_array($this->ValChp)) $this->ValChp = $this->ValChp['name'];
			if ($this->ValChp == "none") $this->ValChp = ""; // en pratique si pas de fichier yil ya none
			// dans rep courant, chemindestockage = Le champ valeur s'il est correct (commence pas par # et non vide) /Sinon NomBase_NomTable_NomChamp
			if (strpos ($this->Valeurs, "#") !=1  && $this->Valeurs != "") {
				$ChemImg = $this->Valeurs;
			} else 	$ChemImg = "./".$this->NmBase."_".$this->NmTable."_".$this->NmChamp."/";
			// gestion effacement anciens fichiers attaches
			if ($this->OFN != "" && ($this->TypEdit == -1 || // ancien fi
					(($this->TypEdit == 'M' || $this->TypEdit == 2) && ($this->Fok == "" || $this->ValChp != "")))) { 
						//  condition: effacement ou en Maj ou copie
						// ancien fichier attache existe (OFN!="") ET la case a ete decochée (Fok=="")
						// OU  un nouveau fichier a été entré ($Valchp!="") (on en enlevé $keycopy.$this->Fname != $keycopy.$this->OFN, cad le non effacement si même nom de fichier)
				if (file_exists($ChemImg.$this->OFN)) delfich ($ChemImg.$this->OFN);
				$value = "";
			} // fin gestion effacement anciens fichiers attache
			if ($this->TypEdit == 2 && $this->ValChp != "") { // copie de fiche => copie de fichier s'il y en a un
				if (file_exists($ChemImg.$this->ValChp)) {
					$cok = copy($ChemImg.$this->ValChp, $ChemImg."2".$this->ValChp);
					$value = "2".$this->ValChp;
					if (!$cok) throw new Exception ("RetSet : Copie du fichier $ChemImg$value vers $ChemImg"."2"."$value impossible <br/>Cliquez sur le bouton retour arriere de votre navigateur");
				}
			} elseif ($this->TypEdit != -1 && $this->ValChp != "") { // si nouveau, copie ou modif avec fichier attache
				// remplacement des caracteres Zarbi ds les noms de fichier cause bugs
				$this->Fname = cv2purascii($this->Fname);
				$this->Fname = db_escape_string(strtr($this->Fname," '*?$\"","______"));
				// variable determinant fichier avec son chemin complet
				$ChemFich=$ChemImg.(strstr($this->Fname,$keycopy) ? "" : $keycopy).$this->Fname; // ne remet la clé que si elle y est pas déjà.				
				/*$retVal.= "debug:";
				$retVal.= "valeur (ftemp): $this->ValChp<br/>";
				$retVal.= "Chemin : $ChemImg<br/>";
				$retVal.= "nom fichier: $this->Fname<br/>";
				$retVal.= "Fok: $this->Fok<br/>";
				$retVal.= "Ancien nom: $this->OFN<br/>";
				$retVal.= "taille: $this->Fsize<br/>"; 
				die($retVal);*/		
				// test taille du fichier attache    
				if (($this->Fsize > $this->MaxFSize) && ($this->ValChp != "")) {
					$retVal.= str_replace("###",  $this->MaxFSize, $this->trad("err1")); 
					unlink ($this->ValChp);
					$this->error = $retVal.str_replace("###",  $this->Fsize, $this->trad("err2"));
					throw new Exception ($this->error);
					$this->ValChp = $value = ""; // annule le nom de fichier pour qu'il ne soit mis en base...
				} else { // fin si fichier trop gros
					$retcopy=copy($this->ValChp,$ChemFich);
					//die("copy(".$this->ValChp.",".$ChemFich.",res=$retcopy");
					if (!$retcopy) { 
						$this->error = "Impossible de copier le fichier '". $this->Fname."' depuis le fichier temporaire '$this->ValChp' ! (chemin complet : $ChemFich)<br/>";
						delfich ($this->ValChp);
					}
				}
				// affecte le nouveau nom de la variable a stocker : à priori le nom du fichier precede de la clé
				if (!$this->error) $value= (strstr($this->Fname,$keycopy) ? "" : $keycopy).$this->Fname;// ne remet la clé que si elle y est pas déjà.
			} // fin si nouveau ou modif avec fichier attach�
			$this->ValChp = $value;
		} // fin si type de champ=fichier attache

		// fonction qui retourne eventuellement ' fonction du type de champ; NB qu'avec PostGres, le ' autour des champs num fout la merde 
		// avec Mysql et Oracle, ça change rien donc on met les ' systématiquement
		$cpstr = rt_carpstr($this->TTC);
		if ($this->ValChp !== "") { /// le !=== est indispensable, sinon la valeur "" est identique à la valeur 0 et bonjour le bordel !!
			if (count($this->tblocfkeys) <= 1) $this->ValChp = $cpstr.$this->ValChp.$cpstr;
		} else { // valeur vide
			$this->ValChp = $GLOBALS['db_type'] != "mysql" ? "DEFAULT" : ($this->FieldNullOk != "NO" ? "NULL" : $cpstr.$this->FieldValDef.$cpstr);
		}
		
		// gestion des clés multiples
		if (count($this->tblocfkeys) > 1) {
			$tbvalchp = explode(":", $this->ValChp);
			$tbret = array();
			foreach ($this->tblocfkeys as $i => $fk) {
				$this->NmChp4SqlRetSet = ($this->sepNmTableNmChpInRetSet ? $this->NmTable."." : "").$fk; // nom complet matable.monchamp ou pas ?
				if ($rttb) {
					$tbret[$this->NmChp4SqlRetSet] = $cpstr.$tbvalchp[$i].$cpstr;
				} else {
					$ret.=  $this->NmChp4SqlRetSet."=".$cpstr.$tbvalchp[$i].$cpstr.", ";
				}
			}
			return ($rttb ? $tbret: $ret); // retourne le set ou un tableau associatif...;
		} else { // clé simple "normale"
			$this->NmChp4SqlRetSet = $this->sepNmTableNmChpInRetSet ? $this->NmTable.".".$this->NmChamp : $this->NmChamp; // nom complet matable.monchamp ou pas ?
			return ($rttb ? Array($this->NmChp4SqlRetSet => $this->ValChp) : $this->NmChp4SqlRetSet."=".$this->ValChp.", "); // retourne le set ou un tableau associatif...;
		}
	} // fin methode RetSet

	// méthode qui parse le champ valeurs avce la nvelle syntaxe
	function ParseValeurs() {
		// on a déplacé la fonction de parsing dans fonctions_pya afin qu'elle puisse servir ailleurs
		$tbrv = parseVal($this->Valeurs); // retourne un tableau asso
		foreach ($tbrv as $k=>$v) $this->$k = $v; // transforme les clés du tableau en propriété de l'objet
		if ($this->locFKeys != '') $this->tblocfkeys =  explode(",",$this->locFKeys) ; // tableau des clés locales
	}
	/** fonction qui sert dans le cas des FK multiples ; elle parse et affecte la valeur courante du champ
	@param $tbValChp est le tableau asso des valeurs de TOUT l'enregistrement (y compris des autres champs ...)
	@return valeur du champ sous la forme val(k1):val(k2):val(k3) de valeurs
	en effet si clés multiples, on a besoin d'aller chercher les valeurs dans les autres champs */
	function AffVal($tbValChp) {
		/// assez sioux : il se peut que le tableau asso des valeurs soit sous la forme $tbValChp[NomChp] = val ou $tbValChp[NmTable#NomChp] = val, dans le cas d'oss et des requetes Oracle faisant intervenir des alias...
		if (is_array($this->tblocfkeys) && count($this->tblocfkeys)>1) { // FK multiple
			$tbv = array();
			foreach ($this->tblocfkeys as $fk) {
				$fk = trim($fk); // enlève espaces
				if (isset($tbValChp[$this->NmTable.'#'.$fk])) {
					$tbv[] = $tbValChp[$this->NmTable.'#'.$fk];
				} else $tbv[] = $tbValChp[$fk];
			}
			$this->ValChp = implode(":",$tbv);
		} else { // normal
			if (isset($tbValChp[$this->NmTable.'#'.$this->NmChamp])) {
				$this->ValChp = $tbValChp[$this->NmTable.'#'.$this->NmChamp];
			} else $this->ValChp = $tbValChp[$this->NmChamp];
		}
	}
	// renvoie la valeur ou l'echoise
	function retOrEch(&$retVal) {
		if ($this->DirEcho) {
			echo $retVal;
			return;
		} else return($retVal);
	}
	// TRaduction des labels
	function trad($lib) {
		global $labels;
		$nolang = (isset($_SESSION["NoLang"]) ? $_SESSION["NoLang"] : 0);
		return ($labels[$nolang][$lib] != "" ? $labels[$nolang][$lib] : "[! $lib !]");
	}
} // fin objet PYAobj


$GLOBALS['labels'] = array (
	0 => array (
		"all" => "(tous)",
		"ante" => "ant&eacute;rieure",
		"poste" => "post&eacute;rieure",
		"inf" => "inf&eacute;rieur ou égal",
		"sup" => "sup&eacute;rieur ou égal",
		"Enrpq" => "Enregistrements pour lequels ",
		"is" => " est ",
		"agrave" => " &agrave; ",
		"and" => " ET ",
		"neg" => "Negation",
		"fat" => "Fichier attaché",
		"null" => "vide (null)",
		"err6" => "<H6>Erreur : la valeur ### contenue dans le champ n'est pas dans la liste ! </H6>",
		"none" => "Aucun(e)",
		"err7" => "<H6>Erreur : valeur(s) ### contenue(s) dans le champ non presente(s) dans la liste ! </h6>",
		"oris" => " ou elle-meme",
		"linktof" => "Lien vers le fichier associe",
		"mes1" => "Pour supprimer l'image ou le fichier, decocher la case, ou choisissez-en un(e) autre.<br/>Sinon ne touchez a rien ...",
		"avtmaj" => "Avant MAJ",
		"err1" => "<B>Impossible de joindre le fichier !</B><br/>La taille des fichiers attaché est limitée à  ###, ",
		"err2" => " et le fichier attache a une taille de ### octets. <br/>Cliquez sur le bouton retour arriere de votre navigateur",
		"2disp" => "A afficher",
		"notnul" => "Significatif (Non nul, >0, !='')",
		"img2big" => "cette image a une taille trop importante pour être affichée",
		"uncktodel" => "Décochez la case pour effacer le fichier"
	),
	1 => array (
		"all" => "(all)",
		"ante" => "anterior with",
		"poste" => "posterior with",
		"inf" => "lower or equal than",
		"sup" => "greater or equal than",
		"Enrpq" => "Records of ",
		"is" => " wich are ",
		"agrave" => " ",
		"and" => " AND ",
		"neg" => "Negation",
		"fat" => "Attached vile",
		"null" => "empty (null)",
		"err6" => "<H6>Error : value ### contained in field is not in the list ! </H6>",
		"none" => "None",
		"err7" => "<H6>Erreur : value(s) ### contained in field are not in the list ! ! </h6>",
		"oris" => " or itself",
		"linktof" => "Link to attached file",
		"mes1" => "To delete image or file, uncheck the box, or select another file.<br/>If not, don't change anything...",
		"avtmaj" => "Before update",
		"err1" => "<B>Impossible to join this file !</B><br/>File size is limted to ###, ",
		"err2" => " and attached file's size is ### bytes. <br/>Click on return button of your browser",
		"2disp" => "To display",
		"notnul" => "Significant (Not null, >0, !='')",
		"img2big" => "this image is to big to be displayed",
		"uncktodel" => "Uncheck to delete the file"
	)
); // fin tableau langues