<?php

/* 
 * Classe permettant d'afficher un tableau html ou exporter en csv une liste d'enregistrements "PYA"
 * possibilité de spécifier une colonne pour des actions. 
 * Le champ (a définir typiquement par un la_cle as rowid4A) 
 */
define("rowid4A", "rowid4A"); // le nom de champ spécial qui sert pour les actions
class PYATable {
	var $theReq = "";
	var $DBname = "";
	var $NmTable = "";///  nom de la table par défaut (peut être pratique avec Oracle qui ne sait pas les ramener)
	var $tableClass = "";
	var $sqlLimit = "no";
	var $noTableIfOnly1Val = false; // si 1 seule valeur ramenée, n'affiche pas le tableau; si =1 libellé : valeur; si =2 que la valeur
	var $nbRows = 0;
	var $nbFields = 0;
	var $lastLib = "";
	var $lastVal = "";
	var $tbLibFields = null;
	var $tbResultsBrut = null; // fait pour tracer...
	var $tbResultsVCL = null;
	var $typReq = "select";
	var $autoClasRow = true;
	var $Actions = ''; // contient l'action a afficher, avec la val de la col à remplacer par ##
	// exemple '<a href="module.php?nom=ticketscaisses&Edit=1&Id=##" title="modifier la caisse"><img src="../client/plugins/ticketspromos/media/edit.png"></a>';
	var $nomColAction = 'Actions'; // en tête de la colonne
	var $footTable = '';
	var $ForceNewPOOD = false;

	/**
	 * 
	 * @param type $theReq
	 * @param type $DBname
	 * @param type $typreq 'select' ou 1 ou autre
	 * @param type $getTbPO recup le tableau t/f (f => qd exec query seulement)
	 * @throws Exception
	 */
	public function __construct($theReq, $DBname, $typreq = "select", $getTbPO = true) {
		if (trim($theReq) == '' || trim($DBname) == '')	throw new Exception ("Class PYATable : La requête et la base de données doivent être spécifiés à l'init");
		//echo "typreq = $typreq, getTbPO = $getTbPO";
		$this->theReq = trim($theReq);
		$this->DBName = $DBname;
		$this->typReq = $typreq;
		if ($this->typReq == 1) $this->typReq = 'select'; // compatibilité OSS
		if ($this->typReq == 'select' && $getTbPO) { // $this->typReq === 1 = 'select' dans OSS
			$this->tbPO = getTbPOReq ($this->theReq, $this->DBName, array("limit" => $this->sqlLimit, 'NmTable'=>$this->NmTable, 'ForceNewPOOD'=>$this->ForceNewPOOD));
		} else $this->tbPO['db_resreq'] = db_query($this->theReq);
	}
	
	public function getCompTable() {
		if ($this->typReq == 'select') {
			/**if ($this->tbPO['db_num_rows'] == 0) { cette merde d'oracle ne sait pas compter le nbre de lignes
				//return ('');
			}**/
			$r .= "<table".($this->tableClass ? ' class="'.$this->tableClass.'"' : '').">\n";
			$r .= $this->getHeadTable();
			$r .= $this->getBodyTable();
			$r .= $this->footTable;
			$r .= "</table>\n";
			// Si pas d'enregistrement, n'affiche pas les entêtes
			if ($this->nbRows == 0) {
				$r = "<p>Aucun enregistrement ramené par cette requête </p>\n";
			// gestion si une seule et unique valeur, pas de tableau
			} elseif ($this->nbRows == 1 && $this->nbFields == 1 && $this->noTableIfOnly1Val > 0) {
				$r = '<p class="oneVal">';
				if ($this->noTableIfOnly1Val == 1) { // affichage libellé du champ$recBrut
					$r .= "<span class=\"libOneVal\">$this->lastLib : </b>\n";
				}
				$r .= $this->lastVal;
				$r .= "</p>\n";
			}
		} else {
			$res = db_query($this->theReq);
			$nbrecmod = (int)db_affected_rows($res);
			$this->tbResultsBrut[0] = $nbrecmod;
			switch ($this->typReq) {
				case "update":
				case 2: // compatibilité OSS"de"delete"lete"
					$lb2 = "modifiés";
					$lbr = "update";
				break;
				case "insert":
				case 3: // compatibilité OSS
					$lb2 = "insérés";
					$lbr = "insert";
				break;
				case "delete":
				case 4: // compatibilité OSS
					$lb2 = "effacés";
					$lbr = "delete";
				break;
			}
			$this->tbResultsBrut[1] = "Req type $lbr";
			$r = "$nbrecmod enregistrement(s) $lb2";
		}
		return $r;
	}
	
	public function getHeadTable() {
		//print_r($this->tbPO);
		$r .= "<thead><tr>\n";
		foreach ($this->tbPO as $c=>$PO) {
			if (is_object($PO)  && $c != 'db_resreq' && $c != 'db_num_rows') {
				$this->nbFields ++;
				$r .= "<th>".($c == rowid4A ? $this->nomColAction : $PO->Libelle)."</th>\n";
				$this->lastLib = $this->tbLibFields[$c] = $PO->Libelle;
			} elseif ($c != 'db_resreq' && $c != 'db_num_rows') {
				$r .= "<th>".$c."</th>\n";
			}
		}
		$r .= "</tr></thead>\n";
		return $r;
	}
	
	public function getBodyTable() {
		$r .= "<tbody>\n";
		$i = 0;
//		print_r(db_fetch_assoc($this->tbPO['db_resreq']));
		while ($rw = db_fetch_assoc($this->tbPO['db_resreq'])) {
			$i++;
			$this->nbRows ++;
			$par = ($i % 2 ==0  ? "roweven" : "rowodd");
			$r .= $this->autoClasRow ? '<tr class="row '.$par.'">' : '<tr>';
			$this->tbResultsBrut[$i] = $rw;
			foreach ($rw as $c=>$v) {
				$r .= "<td class='tdp_$c'>";
				if ($c == rowid4A) {
					$vcl = str_replace("##", $v, $this->Actions);
				} elseif (is_object($this->tbPO[$c])) {
					$this->tbPO[$c]->AffVal($rw);
					$vcl = $this->tbPO[$c]->EchoVCL();
				} else $vcl = $v;
				$r .= $vcl;
				$this->lastVal = $this->tbResultsVCL[$i][$c] = $vcl;
				$r .= "</td>";
			}
			$r .= "</tr>\n";
		}
		if ($i == 0) {
			$this->tbResultsBrut[1] = "Aucun enregistrement correspondant à la requête";
		}
		$r .= "</tbody>\n";
		return $r;
	}
	/**
	 * 
	 * @param string $path chemin ou exporter le fichier (avec ou sans / à la fin)
	 * @param type $format CSV.;  ou TSV.tab, ou ... 
	 * @param type $nomf nom du fichier (traité)
	 * @param type $entete T/F si entêtes = noms des champs à renvoyer
	 * // @param type $gchp T/F si champs à entourer par des guillements
	 * @return type
	 */
	public function exportTable($path, $format = "CSV.;", $nomf ,$entete=false) { // , $gchp=false
		$i = 0;
		list ($ext, $sep) = explode(".", $format);
		if ($sep == "") $sep = ";";
		if ($sep == "tab") $sep = chr(10);
		if (substr($path, -1, 1) != "/") $path .= '/';
		$nomf = str_replace(array(" ", ".","/","\\"), array("_", "-","_","_"), $nomf);
		$ext = strtolower($ext);
		$nomcompf = $path.$nomf.".".$ext;
		try {
			$h = fopen($nomcompf, "w");
			while ($rw = $entete ? db_fetch_assoc($this->tbPO['db_resreq']) : db_fetch_row($this->tbPO['db_resreq'])) {
				if ($i == 0 && $entete && $entete != "no" && $entete != "false") {
					foreach ($rw as $k=>$v) $tbet[] = $k;
					fwrite($h, implode($sep, $tbet)."\n");
				}
				fwrite($h, implode($sep, $rw)."\n");
				$i++;
			}
			fclose($h);
			return ("$i lignes écrites dans $nomcompf\n");
			
		} catch (Exception $ex) {
            die ("Impossible de créer le fichier $path.$nomf");
        }
	}
}

/*
 *  [zzorder] => PYAobj Object
        (
            [NbCarMxCust] => 0
            [NmBase] => DRH2
            [NmTable] => zzia_ligne_mandat_orv
            [NmChamp] => zzorder
            [Libelle] => <i>zzorder</i>
            [title] => 
            [Typaff_l] => NOR
            [FieldType] => 
            [TTC] => 
            [FieldValDef] => 
            [ValChp] => 1
            [ValChpAvMaj] => 
            [FieldNullOk] => 
            [FieldKey] => 
            [FieldExtra] => 
            [TypeAff] => AUT
            [Fccr] => 
            [Valeurs] => 
            [Val2] => 
            [nmfpopl] => popl.php
            [chempopl] => 
            [TypFilt] => 
            [TypCSA] => 
            [Tt_AvMaj] => 
            [Tt_PdtMaj] => 
            [Tt_AprMaj] => 
            [TypEdit] => C
            [DirEcho] => 
            [Comment] => 
            [MaxFSize] => 
            [MaxImgWidth] => 0
            [MaxImgSize2display] => 100000
            [dateInpAttribs] =>  class="dateinput" size="8" maxlength="10" 
            [datetimeInpAttribs] =>  class="datetimeinput" size="12" maxlength="19" 
            [error] => 
            [errInit] => 
            [classDivECF] => DivECF
            [classFieldECF] => FieldECF
            [classLabECF] => LabECF
            [classCSAECF] => CSAECF
            [modTable] => 
            [br] => 
            [dispComment] => 
            [altLibel] => 
            [bDN] => 1
            [bDL] => 1
            [choixMult] => yes
            [dispCSA] => yes
            [dhidden] => 1
            [dialFormId] => dialogform
            [ErrInit] => Champ zzorder (table :zzia_ligne_mandat_orv, base: DRH2) non trouve dans la table de description; champ non trouve ou parametres non passes en arguments
        )
 */