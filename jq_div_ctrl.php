<?php

/*
 Contrôle liste liée multiple ou pas PYAObj
 * appelé directement par inclusion dans PYAObj
 */
$retVal = $this->Libelle." => ".$this->TypeAff;
$nc = $this->NmChamp;

$retVal = '
<div>
	<input type="text" id="txt2s'.$nc.'" style="color:grey" value="Rechercher ...">
	<div id="ress'.$nc.'"></div>
	<div>';
if (!$Mult) {
	$retVal .= '<input type="hidden" name="'.$this->NmChpHtml.'" id="'.$nc.'"  value="'.$this->ValChp.'"/><span id="valtxt'.$nc.'"> '.$this->EchoVCL().'</span>';
} else {
	$tabVS = $this->RVLS("",true); // renvoie les valeurs liées sous forme de tableau  
	if (count($tabVS) == 0) $tabVS = array("-");
		$retVal .= dispLD2($tabVS, $this->NmChpHtml,  array("multiple" => true, //s'il est multiple ou non (non par défaut)
													 "force_type" => "LDF", // force  les cases à cocher ou boutons radio (=RAD) ou liste deroulante (=LDF), ou checkBoxes (=CKBX) qqsoit le nbre de valeur; on peut mettre RADBR ou CKBXBR pour forcer les retour à la ligne
													 "tbclesel" => array(), // tableau des clés sélectionnées
													 "allselected" => true, // force toutes les options a etre sélectionnées (ben oui ça sert)
													 "id" => "", // id html
													 "size" =>  max(2,count($tabVS)), // SIZE= html
													 "title" => "", // titre html 
													 "class" => "",
													 "style" => "",
													 "oth_att" => "",
													 "dir_echo" => false, // retourne ou echoise direct
													 "novalues_msg" => "<h6>Aucune liste de valeurs disponible <br/></h6>£<h6>No values list available <br/></h6>"
													 ));
		$retVal .= ' <button id="delitem" type="button">-</button>';
		$retVal .= outJS('$(function() {
			$("#delitem").click( function () {
				$("#'.$this->NmChpHtml.' option:selected").each( function() {
					$(this).remove()
				});
			});
			}); // fin doc ready
		');
}	
$retVal .= '	</div>
</div>';
$retVal .= outJS('$(function() {
	// efface la val courante des qu on clique, mais que le premier coup
	first'.$nc.' = true;
	$("#txt2s'.$nc.'").click( function() {
		if (first'.$nc.') {
			$("#txt2s'.$nc.'").val("");
			first'.$nc.' = false;
		}
	});

	// met à jour la LD en ajax des qu on tappe pkus de 3 caract ds la boite de rech
	var ajaxurl="ld_ajax_dyn.php?chp_lnk='.urlencode($this->Valeurs).($_REQUEST['SESSION_NAME'] ? "&SESSION_NAME=".$_REQUEST['SESSION_NAME'] : "").'&nmchp='.$nc.($Mult ? '&Mult=1' : '').'&txt2srch=";	
	$("#txt2s'.$nc.'").keyup( function() {
		var valcs = $("#txt2s'.$nc.'").val();
		if (valcs.length >= 2) {
			$("#ress'.$nc.'").load(ajaxurl + valcs );
		}
	});
}); // fin doc ready
');
