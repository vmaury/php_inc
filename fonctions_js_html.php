<?php 
/**
   * Fichier fonctions html et js
   * 
   * @author: Vincent MAURY <artec.vm@arnac.net>
   */
/** VOIR LES FONCTIONS JS PLUS BAS.... */

$GLOBALS['nValRadLd'] = 4; // nbre de valeurs passage liste deroulante=>boutons radio case a cocher
$GLOBALS['SzLDM'] = 6; // parametre size pour les listes deroulantes multiples
$GLOBALS['DispMsg'] = true; // affichage par defaut des messages type "appuyez sur control pour selectionner plusieurs valeurs", ou "Aucune valeur selectionnee"
define ("VSLD", "#SEL#"); // caracteres inseres en debut de valeur de listes indiquant la selection
// tableau des évenements verification de formulaire auto
$GLOBALS['CARSEP_LANG'] = "£";
$GLOBALS['tbEvenmtVFAutoJS'] = array ( 
			"notNull" => "Non vide",
			"email" => "Adresse email",
			"emailNN" => "Adresse email non vide",
			"tel" =>"N° téléphone",
			"telNN" => "N° téléphone non vide",
			"number" => "Nombre",
			"numberNN" => "Nombre >0",
			"puretext"	=> "Texte pur",
			"puretextNN"	=> "Texte pur non vide",
    	 	);
/** 
DispLD2 fonction qui affiche une liste deroulante, ou des boutons radio ou cases a cocher
ceci fonction du nombre de valeurs specifie dans la variable globale $nValRadLd
les valeurs selectionnées sont precedées  de la chaine VSLD
@param  $tbval array tableau de la liste de valeurs:
@param $nmC string nom du champ
@param $othparams array parametres additionnels : multiple" => false, //s'il est multiple ou non (non par défaut)
													 "force_type" => "", // force  les cases à cocher ou boutons radio (=RAD) ou liste deroulante (=LDF), ou checkBoxes (=CKBX) qqsoit le nbre de valeur; on peut mettre RADBR ou CKBXBR pour forcer les retour à la ligne
													 "tbclesel" => array(), // tableau des clés sélectionnées
													 "allselected" => false, // force toutes les options a etre sélectionnées (ben oui ça sert)
													 "id" => "", // id html
													 "size" => "", // SIZE= html
													 "title" => "", // titre html 
													 "class" => "",
													 "style" => "",
													 "oth_att" => "",
													 "dir_echo" => false, // retourne ou echoise direct
													 "novalues_msg" => "<h6>Aucune liste de valeurs disponible <br/></h6>£<h6>No values list available <br/></h6>"
*/
function dispLD2($tbval, $nmC, $othparams = array("multiple" => false, //s'il est multiple ou non (non par défaut)
													 "force_type" => "", // force  les cases à cocher ou boutons radio (=RAD) ou liste deroulante (=LDF), ou checkBoxes (=CKBX) qqsoit le nbre de valeur; on peut mettre RADBR ou CKBXBR pour forcer les retour à la ligne
													 "tbclesel" => array(), // tableau des clés sélectionnées
													 "allselected" => false, // force toutes les options a etre sélectionnées (ben oui ça sert)
													 "id" => "", // id html
													 "size" => "", // SIZE= html
													 "title" => "", // titre html 
													 "class" => "",
													 "style" => "",
													 "oth_att" => "",
													 "dir_echo" => false, // retourne ou echoise direct
													 "novalues_msg" => "<h6>Aucune liste de valeurs disponible <br/></h6>£<h6>No values list available <br/></h6>"
													 )) {
	//print_r($othparams['tbclesel']);
	if (!is_array($tbval)) $tbval = array($tbval);
	if (!$othparams['tbclesel']) $othparams['tbclesel'] = array(); // pr pas que les tests merdent
	if (!is_array($othparams['tbclesel'])) $othparams['tbclesel'] = array($othparams['tbclesel']);
	$idC = $othparams['id'] != "" ? $othparams['id'] : $nmC;
	if ($othparams['multiple']) $nmC .= "[]" ; /// pas sur que ce soit conforme avec les radio et les checkbox, mais bon ca a l'air de focntionner jusqu'a present
	$class = $othparams['class'] != "" ? ' class="'.$othparams['class'].'" ' : '';
	$style = $othparams['style'] != "" ? ' style="'.$othparams['style'].'" ' : '';
	$oth_att = $othparams['oth_att'] != "" ? ' '.$othparams['oth_att'].' ' : '';
	if (count($tbval) == 0) { // pas de liste de valeurs
		if ($othparams['novalues_msg']) $retVal.= tradLib($othparams['novalues_msg']);
		$retVal.= echochphid($NmC.($othparams['multiple'] ? "[]" : ""), "", false, $idC); // necessaire au traitement par derriere
	} elseif ((count($tbval) > $GLOBALS['nValRadLd'] && $othparams['force_type'] == "") || $othparams['force_type'] == "LDF") { 
	// liste déroulante: nbre val suffisantes et pas de forcage 
		if ($othparams['multiple']) {
			$othparams['title'] .= " ".tradLib("Appuyez sur la touche Ctrl pour s&eacute;lectionner plusieurs valeurs£Press Ctrl to select several values");
			$mult = ' multiple="multiple" ';
			$SizeLDM = ' size="'.min($GLOBALS['SzLDM'],count($tbval)).'" ';
		} 
		$SizeLDM = $othparams['size'] > 0 ? ' size="'. $othparams['size'].'" ' : $SizeLDM;
		$retVal.= '<SELECT TITLE="'.$othparams['title'].'" ID="'.$idC.'" NAME="'.$nmC.'" '.$SizeLDM.$mult.$class.$style.$oth_att.'>'."\n";
		
		foreach ($tbval as $key =>$val) {
			$val = strip_tags($val);
			$retVal.= "	<OPTION VALUE=\"$key\" ";
			$niv = count(explode("|",$val));
			$retVal .= ' class="optld'.$niv.'" ';
			if (strstr($val, VSLD) || in_array($key,$othparams['tbclesel']) || $othparams['allselected']) {
				$sel = ' selected="selected" ';
				$val = str_replace (VSLD, "", $val); // retourne la chaine ss le car de sleection
			} else $sel="";
			$retVal .= $sel.">$val</OPTION>\n";
			} // fin boucle sur les valeurs
		$retVal.= "</SELECT>";
	} // fin liste deroulante
	elseif ($othparams['multiple'] && !stristr($othparams['force_type'],"RAD") || stristr($othparams['force_type'],"CKBX")) { 
	// cases a cocher si multiple OU pas de forcage en radio
		$retVal.= '<span id="'.$idC.'" TITLE="'.$othparams['title'].'">'; // comme ça on peut mettre un id sur le span
		foreach ($tbval as $key => $val) {
			if ($key != "") {
				$retVal.= '<INPUT TYPE="CHECKBOX" NAME="'.$nmC.'" VALUE="'.$key.'" TITLE="'.$othparams['title'].'"';
				if (strstr($val, VSLD) || in_array($key,$othparams['tbclesel']) || $othparams['allselected']) {
					//$sel="checked";
					$sel = ' checked="checked" '; // XHTML
					$val = str_replace (VSLD, "", $val); // retourne la chaine ss le car de selection
				}
				else $sel="";
				$retVal.= $sel.$class.$style.$oth_att.">&nbsp;".$val;
				$retVal.= (stristr($othparams['force_type'],"BR") ? "<br/>\n" : " &nbsp;&nbsp;\n");
			} // fin si valeur non nulle    
		} // fin boucle sur les valeurs
		$retVal.= '</span>'; 
	} // fin cases a cocher
	else { // boutons radio
		$retVal.= '<span id="'.$idC.'" TITLE="'.$othparams['title'].'">'; // comme ça on peut mettre un id sur le span
		foreach ($tbval as $key =>$val) {
			$retVal.= '<INPUT TYPE="RADIO" NAME="'.$nmC.'" VALUE="'.$key.'" TITLE="'.$othparams['title'].'"';
			if (strstr($val, VSLD) || in_array($key,$othparams['tbclesel'])) {
				$sel = ' checked="checked" '; // XHTML
				$val = str_replace (VSLD, "", $val); // retourne la chaine ss le car de selection
			} else $sel = "";
			$retVal .= $sel.">&nbsp;".$val;
			$retVal .= stristr($othparams['force_type'],"BR") ? "<br/>\n" : " &nbsp;&nbsp;\n";
		} // fin boucle sur les valeurs
		$retVal.= '</span>'; // comme ça on peut mettre un id sur le span
	} // fin boutons radio
	return retOrEcho ($retVal, $othparams['dir_echo']);
} // fin fonction dispLD2
// Ancienne fonction pour compatibilité
function DispLD($tbval, $nmC, $Mult="no", $Fccr="", $dir_echo=true, $idC="") {
	$bmult = ($Mult != "no");
	return( dispLD2($tbval, $nmC, array("multiple" => $bmult, //s'il est multiple ou non (non par défaut)
													 "force_type" => $Fccr, // force  les cases à cocher ou boutons radio (=RAD) ou liste deroulante (=LDF), ou checkBoxes (=CKBX) qqsoit le nbre de valeur; on peut mettre RADBR ou CKBXBR pour forcer les retour à la ligne
													 "id" => $idC, // id html
													 "oth_att" => "",
													 "dir_echo" => $dir_echo, // retourne ou echoise direct
													 ))); 
}
/**  retourne une liste déroulante + une boite texte
 * 
 * 
 * @param type $tbval tableau des valeurs
 * @param type $nmC nom du champ
 * @param type $valC valeur du champ
 * @param type $dir_echo 
 * @param type $idC
 * @return type
 */
function DispLDandTxt ($tbval, $nmC, $valC="", $dir_echo=true, $idC="") {
	if (!is_array($tbval)) $tbval = array($tbval);
	if ($idC == "") $idC = $nmC;
	$kex = false; // valeur courante existe dans les valeurs clé de la liste déroulante
	foreach ($tbval as $k=>$v) {
		if ($k == $valC) {
			$tbval[$k] = VSLD.$v;
			$kex = true;
		}
	}
	$tbval = $tbval + array('OTH' => (!$kex ? VSLD : "").tradLib("Autre".$GLOBALS['CARSEP_LANG']."Other"));
	$retVal .= dispLD2($tbval, "assLD4Txt".$idC, array("multiple" => false, //s'il est multiple ou non (non par défaut)
													 "force_type" => "LDF", // force  en LD
													 "oth_att" => ' onchange="CheckLDandTxt(\''.$idC.'\');"', /// CheckLDandTxt est dans php_inc/jQuery/shared_inc.js.php
													 "dir_echo" => false, // retourne
													 ));
	$type = $kex ? 'hidden' : 'text'; // ch'ais pas pquoi j'ai mis ça... ca fout la gouaille parfois
	$type = 'text';
 	$retVal .= '<input type="'.$type.'" value="'.$valC.'" name="'.$nmC.'" id="'.$idC.'">';
   	return retOrEcho ($retVal, $dir_echo);
}
/** crache une checkbox avec un label
 * 
 * @param type $name
 * @param type $value
 * @param type $label
 * @param type $dir_echo
 * @param type $checked
 * @param type $id
 * @param type $disabled
 * @return type
 */
function echCheckBoxWLabel($name, $value, $label, $dir_echo=true, $checked=false, $id="", $disabled=false, $class="") {
	if ($id=="") $id = $name;
	$ret = '<label for="'.$id.'">'.$label.'</label><input name="'.$name.'" type="checkbox" value="'.$value.'" '.($checked ? 'checked="checked" ' : '').' '.
			($disabled ? 'disabled="disabled" ' : '').' id="'.$id.'"'.($class ? ' class="'.$class.'" ' : '').'/>';
	// dans le cas du disabled, il faut mettre ça sinon le champ est ignoré dans ls formulaires au submit !!!
	// si on met un readonly, l'utilisateur peut manipuler la checkbox ; quelle misère !!
	if ($disabled) $ret .= echochphid($name, $checked ? $value : '', false);
   	return retOrEcho ($ret, $dir_echo);
	
}
/** crache une checkbox 
 * 
 * @param type $name nom
 * @param type $value val
 * @param type $dir_echo dir_echo
 * @param type $checked checked
 * @param type $id id 
 * @param type $disabled disabled
 * @param type $class class
 * @param type $title titre
 * @return type ckbx
 */
function echCheckBox($name, $value, $dir_echo=true, $checked=false, $id="", $disabled=false, $class="", $title="") {
	if ($id=="") $id = $name;
	$ret = '<input name="'.$name.'" type="checkbox" value="'.$value.'" '.($checked ? 'checked="checked" ' : '').' '.($disabled ? 'disabled="disabled" ' : '').' id="'.$id.'"'.($class ? ' class="'.$class.'" ' : '').($title ? ' title="'.$title.'" ' : '').'/>';
	// dans le cas du disabled, il faut mettre ça sinon le champ est ignoré dans ls formulaires au submit !!!
	// si on met un readonly, l'utilisateur peut manipuler la checkbox ; quelle misère !!
	if ($disabled) $ret .= echochphid($name, $checked ? $value : '', false);
   	return retOrEcho ($ret, $dir_echo);
}

/**  echoise un commentaire html
 * 
 * @param type $title
 * @param type $comment
 * @param type $dir_echo
 * @return type
 */
function echComment($title, $comment, $dir_echo=true) {
    $ret .= "\n<!-- $title : $comment -->\n";
   	return retOrEcho ($ret, $dir_echo);
}

// fonction qui echoise un texte dans un style
function echspan($style, $text, $dir_echo=true) {
    $ret = "<span class=\"$style\">$text</span>";
   	return retOrEcho ($ret, $dir_echo);
}
/// crache un lien html
function dispLink($url, $obj, $othparams = array("id" => "", // id html
													 "title" => "", // titre html 
													 "class" => "",
													 "style" => "",
													 "target" => "",
													 "oth_att" => "", // autres attributs à rentrer tel quel ex: ' onclick="monjs()" '
													 "dir_echo" => false, // retourne ou echoise direct)) {
													  )) {
	$tbop2o = array("id", "target", "title", "class", "style");
	foreach ($tbop2o as $k) {
		if ($othparams[$k] != "") $att .= " $k=\"".$othparams[$k].'" ';
	}
	$ret = '<a href="'.$url.'" '.$att.$othparams['oth_att'].'>'.$obj.'</a>';
	return retOrEcho ($ret, $othparams['dir_echo']);		
}

/** renvoie un lien html
 * 
 * @param type $url
 * @param type $obj
 * @param type $title
 * @param type $class
 * @param type $target
 * @return type
 */
function makelink($url, $obj, $title="", $class="", $target="") {
	return ('<a href="'.$url.'" '.( $target !="" ? 'target="'.$target.'" ' : ""). ( $title !="" ? 'title="'.$title.'" ' : ""). ( $class !="" ? 'class="'.$class.'" ' : "").'>'.$obj.'</a>');
}

/** renvoie une input type=text ou type=password
 * 
 * @param type $NmC
 * @param type $ValC
 * @param type $othparams array( "id" => "", // id html
													 "size" => "", // SIZE= html
													 "maxlength" => "", // SIZE= html
													 "title" => "", // titre html 
													 "class" => "",
													 "style" => "",
													 "oth_att" => "", // autres attributs à rentrer tel quel ex: ' onclick="monjs()" '
																	// type = password si besoin
													 "dir_echo" => false, // retourne ou echoise direct
 * @return type
 */
function dispInpTxt($NmC, $ValC, $othparams = array( "id" => "", // id html
													 "size" => "", // SIZE= html
													 "maxlength" => "", // SIZE= html
													 "title" => "", // titre html 
													 "class" => "",
													 "style" => "",
													 "oth_att" => "", // autres attributs à rentrer tel quel ex: ' onclick="monjs()" '
													 "dir_echo" => false, // retourne ou echoise direct
													 "type" => "text"
													 )) {
	if ($othparams['id'] == "") $othparams['id'] = $NmC;
	$tbop2o = array("id", "size", "maxlength", "title", "class", "style"); // tableau des attributs HTML à sortir direct
	foreach ($tbop2o as $k) {
		if ($othparams[$k] != "") $att .= " $k=\"".$othparams[$k].'" ';
	}
	$type = $othparams['type'] == 'password' ? 'password' : 'text';
	$ret = '<INPUT TYPE="'.$type.'" NAME="'.$NmC.'" VALUE="'.$ValC.'" '.$att.$othparams['oth_att'].">\n";
	return retOrEcho ($ret, $othparams['dir_echo']);
}
/** retourne un <textearea... */
function dispTxtArea($NmC, $ValC, $othparams = array( "id" => "", // id html
													 "cols" => "", // SIZE= html
													 "rows" => "", // SIZE= html
													 "title" => "", // titre html 
													 "class" => "",
													 "style" => "",
													 "oth_att" => "", // autres attributs à rentrer tel quel ex: ' onclick="monjs()" '
													 "dir_echo" => false, // retourne ou echoise direct
													 )) {
	if ($othparams['id'] == "") $othparams['id'] = $NmC;
	$tbop2o = array("id", "cols", "rows", "title", "class", "style"); // tableau des attributs HTML à sortir direct
	foreach ($tbop2o as $k) {
		if ($othparams[$k] != "") $att .= " $k=\"".$othparams[$k].'" ';
	}
	$ret = '<TEXTAREA NAME="'.$NmC.'" '.$att.$othparams['oth_att'].'>'.$ValC."</TEXTAREA>\n";
	return retOrEcho ($ret, $othparams['dir_echo']);
}

/** retourne un <input type=hidden ... */
function dispInpHid($NmC, $ValC, $othparams = array('dir_echo' => false, 'id' => '')) {
	$id = $othparams['id'] ? $othparams['id'] : $NmC;
	$ret = '<input type="hidden" name="'.$NmC.'" value="'.$ValC.'" id="'.$id.'" >'."\n";
	return retOrEcho ($ret, $othparams['dir_echo']);
}

// fonction qui echoise un champ hidden pour compat
function echochphid($NmC, $ValC, $dir_echo=true, $id = "") {
	return dispInpHid($NmC, $ValC, array('dir_echo' => $dir_echo, 'id' => $id));
}
// retourne la valeur ou l'échoise
function retOrEcho(&$r,$de) {
	if ($de) {
		echo $r;
		return;
	} else return($r);
}
/** FONCTIONS PLUS JAVASCRIPT
*/
// fonction qui encrypte les mails en JS
/// TODO Pomper celle de typo3
function encJSmail ($admail, $dir_echo=true) {
	$ret = '
<script language="javascript">document.write(\'<a href="mailto:'.$admail.'">'.$admail.'</a>\');</script><noscript>'.str_replace("@","[/at\]",$admail).'</noscript>';
   	return retOrEcho ($ret, $dir_echo);
}
// fonction qui affiche du HTML customise fonction d'une chaine de car
function DispCustHT($Val2Af, $htmlspecialchars=true) {
	// si dans la chaine il y a un @, pas d'espaces ni de retour chariot, alors c'est une adressemail 
	if (VerifAdMail($Val2Af)) {
		$Val2Af="<A HREF=\"mailto:".$Val2Af."\">".$Val2Af."</a>";
	} else if (strstr($Val2Af,"http://")  && !strstr($Val2Af,"\n")) {
		$Val2Af="<A HREF=\"".$Val2Af."\" target=\"_blank\">".$Val2Af."</a>";
	} else if (strstr($Val2Af,"www.")  && !strstr($Val2Af," ") && !strstr($Val2Af,"\n")) {
		$Val2Af="<A HREF=\"http://".$Val2Af."\" target=\"_blank\">".$Val2Af."</a>";
	} else {  // sinon traitement divers
		if ($htmlspecialchars) {
			$Val2Af = nl2br(htmlspecialchars($Val2Af));
		}
		$Val2Af=($Val2Af=="" ? "&nbsp;" : $Val2Af);
	}
	return ($Val2Af);
}

// conversion d'un tableau associatif en html
function table2htmlTable($tb, $dir_echo=true) {
	if (count($tb) == 0) return;
	$str.='<table border="1">'."\n";
	$first = true;
	foreach ($tb as $row) {
		if ($first) {
			$str .= "<thead>";
			foreach ($row as $f=>$v) {
				$str.= "<th>$f</th>";
			}
			$str .= "</thead>\n";
			$first= false;
		}
		$str .= "<tr>";
		foreach ($row as $f=>$v) {
			if ($v=="") $v = "&nbsp;";
			$str.= "<td>$v</td>";
		}
		$str .= "</tr>\n";
	}
	$str .= "</table>\n";
   	return retOrEcho ($str, $dir_echo);
}
/** Transforme tableaux asso entete / table en html
 * 
 * @param type $tbentetes
 * @param type $table
 * @param type $tbfoot opt
 * @param type $class opt
 * @return string
 */
function ttable2html($tbentetes, $table, $tbfoot= false, $class=""){
	//print_r($table);
	$ret .= "<table id='synth4payes' class='$class'>\n";
	$ret .= "<thead><tr>";
	if (!is_array($tbentetes)) {
		foreach ($table[0] as $k=>$v) $tbentetes[$k] = $k;
	}
	foreach ($tbentetes as $k=>$value) {
		$ret .= "<th>$value</th>\n";
	}
	$ret .= "</tr></thead>\n";
	foreach ($table as $row) {
		$ret .= "<tr>";

		$firstC = true;
		foreach ($tbentetes as $k=>$value) {
			if ($firstC) {
				$ret .= "<th>{$row[$k]}</th>\n";
				$firstC = false;
			} else 	$ret .= "<td>".(trim($row[$k]) != "" ? $row[$k] : "&nbsp;")."</td>\n";
		}
		$ret .= "</tr>\n";
	}
	if (is_array($tbfoot)) {
		$ret .= "<tfoot><tr>\n";
		foreach ($tbentetes as $k=>$value) {
			$ret .= "<th>".(trim($tbfoot[$k]) != "" ? $tbfoot[$k] : "&nbsp;")."</th>\n";
		}
		$ret .= "</tr></tfoot>\n";
	} else $ret .= $tbfoot;
	$ret .= "</table>\n";
	return $ret;
}

/** Transforme tableaux asso entete / table en TSV
 * 
 * @param type $tbentetes
 * @param type $table
 * @param type $tbfoot = false
 * @param type $sep=";"
 * @param type $utf8 = FALSE
 * @return string
 */
function ttable2tsv($tbentetes, $table, $tbfoot= false, $sep = ";", $utf8 = false) {
	//$sep = chr(9);	
	foreach ($tbentetes as $k=>$value) {
		//$ret .= (!$utf8 ?  utf8_decode ($value) : $value).$sep;
		$ret .= (!$utf8 ?  utf8_decode($value) : $value).$sep;
		//$ret .= $value.$sep;
	}
	$ret .= "\n";
	if (is_array($tbfoot))  $table = $table + $tbfoot;
	foreach ($table as $row) {
		foreach ($tbentetes as $k=>$value) {
			if (!strstr($row[$k],'http')) $row[$k] = str_replace(".",",", $row[$k]);
			$row[$k] = str_replace("\n","-", $row[$k]);
			if (!$utf8) $row[$k] = utf8_decode ($row[$k]);
			$ret .= $row[$k].$sep;
		}
		$ret .= "\n";
	}
	return $ret;
}
/** fonction qui crache du JS avec les bonnes balises Javascript
 * 
 * @param type $myjs : le JS a cracher
 * @param type $dir_echo : comme d'hab
 * @param type $outJQDR : false par defeaut, si true crache $(document).ready(function(){
 * @return type
 */
function outJS($myjs,$dir_echo = false, $outJQDR = false) {
	$ret = '
<script type="text/javascript">
	/*<![CDATA[*/
	'.($outJQDR ? "\n $(document).ready(function(){\n" : "")
	.$myjs
	.($outJQDR ? "\n});\n" : "")
	.'/*]]>*/
</script>
'
	;
   	return retOrEcho ($ret, $dir_echo);
}

// fonction qui retourne les FONCTIONS JAVASCRIPT a initialiser
// pour l'utilisation d'Ajax
function echAjaxJSFunctions($dbscr=true) {

$theJS = "
/* JS Functions for Ajax/XMLHttpRequest dynamic content (made by DLCube ;) */

function ahah(url,target) {
   // native XMLHttpRequest object
   document.getElementById(target).innerHTML = 'envoi de la requete au serveur...';
   if (window.XMLHttpRequest) {
       var req = new XMLHttpRequest();
       req.onreadystatechange = function() {ahahDone(target,req);};
       req.open(\"GET\", url, true);
       req.send(null);
   // IE/Windows ActiveX version
   } else if (window.ActiveXObject) {
       var req = new ActiveXObject(\"Microsoft.XMLHTTP\");
       if (req) {
           req.onreadystatechange = function() {ahahDone(target,req);};
           req.open(\"GET\", url, true);
           req.send();
       }
   } 
}
function ahahDone(target,req) {
   // only if req is loaded
   if (req.readyState == 4) {
       // only if OK
       if (req.status == 200 || req.status == 304) {
           results = req.responseText;
           document.getElementById(target).innerHTML = results;
       } else {
           document.getElementById(target).innerHTML=\"ahah error:\" + req.statusText;
       }
   } 
}
/* End of JS Functions for Ajax/XMLHttpRequest dynamic content (made by DLCube ;) */

";
return ($dbscr ? outJS($theJS) : $theJS);
}
/**
 *  ajoute un boton qui permet d'afficher/masquer du texte
 * 
 * @param type $theid
 * @param type $thecontent
 * @param type $theclass
 * @param type $thetitle
 * @param type $initDisp
 * @return type 
 */

function toggleAffDiv($theid, $thecontent="", $theclass="fxbutton", $thetitle="afficher/masquer", $initDisp=false) {
	$initDisp = $initDisp || $_SESSION['hidPosOf'.$theid];
	$ret = '<a name="AncOfTgAf'.$theid.'"></a>';
	$ret .= '<input type="hidden" name="hidPosOf'.$theid.'" id="hidPosOf'.$theid.'" value="'.$initDisp.'"/>';
	$ret .= '<input title="'.$thetitle.'" type="button" id="butOf'.$theid.'" value="'.(!$initDisp ? "+" : "-").'" class="'.$theclass.'" onclick="if (document.getElementById(\''.$theid.'\').style.display==\'none\') { document.getElementById(\'butOf'.$theid.'\').value=\'-\';document.getElementById(\''.$theid.'\').style.display=\'block\'; document.getElementById(\'hidPosOf'.$theid.'\').value=\'1\'; } else {document.getElementById(\'butOf'.$theid.'\').value=\'+\';document.getElementById(\''.$theid.'\').style.display=\'none\';document.getElementById(\'hidPosOf'.$theid.'\').value=\'0\';}"> <br/>';	
// 	$ret .= '<a href="#AncOfTgAf'.$theid.'" onclick="document.getElementById(\''."theb".'\').value=\'P\';document.getElementById(\''.$theid.'\').style.display=\'block\'" class="'.$theclass.'" title="'.$thetitle.'">+</a>&nbsp;';
// 	$ret .= '<a href="#AncOfTgAf'.$theid.'" onclick="document.getElementById(\''.$theid.'\').style.display=\'none\'" class="'.$theclass.'" title="'.$thetitle.'">&nbsp;-&nbsp;</a><br/>';
	
	if ($thecontent != "") $ret .= '<div id="'.$theid.'" style="display:'.($initDisp ? "block" : "none").'">'.$thecontent.'</div>';
	return($ret);
}
// crache le javascript de gestion d'onglets
// les onglets eux-même doivent avoir un id onglxxx, avec 1<=xxx<=$totNum
// le contenu des onglets id divonglxxx, avec 1<=xxx<=$totNum
/// TODO TODO : faire plutot ça en jQuery : cf http://www.sohtanaka.com/web-design/simple-tabs-w-css-jquery/
/// démo là : http://www.sohtanaka.com/web-design/examples/tabs/
function JStoggleAffOngl($totNum=10, $dbscr=true) {
	$theJS ="
function ToggleAffOngl(theid) {
	for(var icid=1;icid<=$totNum;icid++) {
		if (document.getElementById('ongl' + icid)) {
			if (icid != theid) {
				document.getElementById('divongl' + icid).style.display='none';
				document.getElementById('ongl' + icid).className='onglPassif';
			} else {
				document.getElementById('divongl' + icid).style.display='block';
				document.getElementById('ongl' + icid).className='onglActif';
				document.getElementById('idOnglAct').value = icid;
			}
		}
	}
}
	";
	return ($dbscr ? outJS($theJS) : $theJS);
}
// fonction JAVASCRIPT qui remplace un caractere a par b dans une chaine
function JSstr_replace($dir_echo = true, $dbscr = true) {
	$ret = 'function str_replace(a,b,expr) {
		var i=0
		while (i!=-1) {
			i=expr.indexOf(a,i);
			if (i>=0) {
				expr=expr.substring(0,i)+b+expr.substring(i+a.length);
				i+=b.length;
			}
		}
		return expr
	}
	';
	$ret = $dbscr ? outJS($ret) : $ret;
   	return retOrEcho ($ret, $dir_echo);
}

/** fonction qui permet de rentrer de proteger un lien par une boite JS ou il faut rentrer un mot de passe
// le js est a mettre dans le onclick plutot que dans le href, sinon on voit tout dans la barre d'etat
// ce n'est biensur pas super secure, mais bon on est pas encore chez Sarko donc ca va...
// s'utilise conjointement avec la fonction ci-après
// exemple : <a href="#" onclick="protectlnk('my_url','<?php echo calcJSEnc('my_passwd')?>','entrez votre mdp pour visiter cette url');">my_link</a>
*/
function JSprotectlnk($dir_echo = true, $dbscr = true,$offs = 69) {
$ret = "
function protectlnk(url,passwd,message) {
	//if (passwd == simpleCrypt(prompt(message,''))) {
	if (passwd == prompt(message,'')) {
	   location = url;}
	else alert ('Mot de passe incorrect');
}
// encryptage : anjoute ts les codes ascii avec un offset + la position du car
function simpleCrypt(passwd) {
	var len=passwd.length;
	var enc = 0;
	for(var i=0;i<len;i++){
		enc += ord(passwd.substr(i,1)) + i + ".$offs.";
	}
	return(enc);
}
function ord (string) {
    // Returns the codepoint value of a character  
    // 
    // version: 1008.1718
    // discuss at: http://phpjs.org/functions/ord    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   bugfixed by: Onno Marsman
    // +   improved by: Brett Zamir (http://brett-zamir.me)
    // *     example 1: ord('K');
    // *     returns 1: 75    // *     example 2: ord('\uD800\uDC00'); // surrogate pair to create a single Unicode character
    // *     returns 2: 65536
    var str = string + '';
    
    var code = str.charCodeAt(0);    
    if (0xD800 <= code && code <= 0xDBFF) { // High surrogate (could change last hex to 0xDB7F to treat high private surrogates as single characters)
        var hi = code;
        if (str.length === 1) {
            return code; // This is just a high surrogate with no following low surrogate, so we return its value;
                                    // we could also throw an error as it is not a complete character, but someone may want to know        
		}
        var low = str.charCodeAt(1);
        if (!low) {
        }        
        return ((hi - 0xD800) * 0x400) + (low - 0xDC00) + 0x10000;
    }	
    if (0xDC00 <= code && code <= 0xDFFF) { // Low surrogate
        return code; // This is just a low surrogate with no preceding high surrogate, so we return its value;
                                // we could also throw an error as it is not a complete character, but someone may want to know    
    }
    return code;
}
";
	$ret = $dbscr ? outJS($ret) : $ret;
   	return retOrEcho ($ret, $dir_echo);
}
// calcul de l'encryptage en php
function calcJSEnc($str,$offs = 69) {
	for ($i = 0; $i < strlen($str); $i++) {
		//$R2 .= (ord(substr($str,$i,1)) + $offs).",";
		$ret += (ord(substr($str,$i,1)) + $i + $offs);
	}
	return($ret);
}
// colle le code javascript d'ouverture d'une popup
function JSpopup($wdth=500, $hght=400, $nmtarget="Intlpopup", $dir_echo=true) {
	global $HTTP_HOST;
	$HostName=($HTTP_HOST=="" ? $_SERVER["HTTP_HOST"] : $HTTP_HOST); // because diff�entes versions
	// on change le nom de target des popups internet (externes) pour ne pas foutre la merde dans les popups ouvertes sur l'intranet
	$nmtarget=(strstr($HostName,"haras-nationaux.fr")!=false ? "Ext".$nmtarget : $nmtarget);
	$ret='
<script type="text/javascript">
	/*<![CDATA[*/
<!--
// ouverture d\'une Popup
var oPopupWin; // stockage du handle de la popup
function popup(page, width, height) {
    NavVer=navigator.appVersion;
	HostName=\''.$HostName.'\' // sert au debogage;
    NavVer=navigator.appVersion;
    if (NavVer.indexOf(\'MSIE 5.5\',0) >0  ) {
        var undefined;
        undefined=\'\';
        }
  closepop();
  if (width==undefined)
  width='.$wdth.';
  if (height==undefined)
  height='.$hght.';
    oPopupWin = window.open(page, \''.$nmtarget.'\', "alwaysRaised=1,dependent=1,height=" + height + ",location=0,menubar=0,personalbar=0,scrollbars=1,status=0,toolbar=0,width=" + width + ",resizable=1");
	oPopupWin.focus();
	// valeur de retour diff�ente suivant navigateur (merdique a souhait) !!!
	var bAgent = window.navigator.userAgent;
	var bAppName = window.navigator.appName;
	if ((bAppName.indexOf("Explorer") >= 0) && (bAgent.indexOf("Mozilla/3") >= 0) && (bAgent.indexOf("Mac") >= 0))
		return true; // dont follow link
	else return false; // dont follow link
	//return !oPopupWin;

}
function closepop() {
    if (oPopupWin) {
        var tmp;
        // Make sure oPopupWin is empty before
        // calling .close() or we could throw an
        // exception and never set it to null.
        tmp = oPopupWin;
        oPopupWin = null;
        // Only works in IE...  Netscape crashes
        // if you have previously closed it by hand
        tmp.close();
        //if (navigator.appName != "Netscape") tmp.close();
      }

}
// -->
	/*]]>*/
</script>
';
	$ret .= '<a name="LimKnifepopupDivAnchor"> </a><div id="LimKnifepopupDiv"></div>
<div id="LimKnifeFilter"></div>';
	
   	return retOrEcho ($ret, $dir_echo);
}
/** colle le code javascript d'ouverture d'une popup Loupe de photo qui se redimensionne automatiquement
Utilisation: appel de cette fonction en php au debut du fichier dans l'entete <HEAD> pas ex
<?php JSPopLoup();?>
ensuite: lien du type <a href="#" onclick="poploup(image_avec_chemin_relatif,titre,commentaire)">
A noter que le chemin relatif de l'image est donne par rapport au fichier appelant (comme pour une image normale)
*/
function JSPopLoup($nmtarget="Intlpopup", $dir_echo=true) {
// pour assurer compat. avec vieilles versions de php
$doc_root_vm = ($_SERVER["DOCUMENT_ROOT"]=="" ? "/home/httpd/html" : $_SERVER["DOCUMENT_ROOT"]);
// on calcule le chemin du fichier appeleant pour pouvoir utiliser des liens relatifs
// i.e. on enleve du chemin absolu (getcwd) la racine du serveur
$chemcour = str_replace ( $doc_root_vm,"" , getcwd());
//echo "test chemin:".getcwd()."<br/>";
?>
<script type="text/javascript">
	/*<![CDATA[*/
<!--
// ouverture d'une Popup Loupe auto redimensionnante
var oPopupWin; // stockage du handle de la popup
function poploup(image,titre,commentaire) {
    NavVer=navigator.appVersion;
    if (NavVer.indexOf("MSIE 5.5",0) == -1 && NavVer.indexOf("MSIE 6.",0) == -1) {
        var undefined;
        undefined='';
        }

    var tmp; // issu d'un copier/coller antediluvien
    if (oPopupWin) {
        // Make sure oPopupWin is empty before
        // calling .close() or we could throw an
        // exception and never set it to null.
        tmp = oPopupWin;
        oPopupWin = null;
        // Only works in IE...  Netscape crashes
        // if you have previously closed it by hand
        if (navigator.appName != "Netscape") tmp.close();
      }
  
    oPopupWin = window.open("", "<?php echo $nmtarget?>", "alwaysRaised=1,dependent=1,height=200,location=0,menubar=0,personalbar=0,scrollbars=no,status=0,toolbar=0,width=200,resizable=1");    
	oPopupWin.document.open();
	if (titre=="") {titre="Loupe";}
	oPopupWin.document.write("<HTML><HEAD><TITLE>"+titre+"</TITLE></HEAD>\n<BODY>\n");
	oPopupWin.document.write("<CENTER>\n");
	oPopupWin.document.write("<IMG SRC=\"<?php echo $chemcour?>/" + image+"\"><br/>\n");
	if (commentaire!="") {oPopupWin.document.write("<small><I>"+commentaire+"</I></small><br/>\n");}
	oPopupWin.document.write("<br/><a href=\"javascript:self.close()\" ><IMG SRC=\"/hn0700/partage/IMAGES/fermer.gif\" border=\"0\"></a>\n");
	oPopupWin.document.write("</CENTER>\n"); 
	oPopupWin.document.write("<script language=\"JavaScript\">\n");
	// la fonction d'ajustement n'est pas appel� directement, mais toutes les 5 sec pour laisser
	// le temps aux images de se charger ;-)
	oPopupWin.document.write("function ajuste() {\n");
	//oPopupWin.document.write("alert('coucou');"); DEBUG
   oPopupWin.document.write("var H = document.body.scrollHeight+50;\n");
	oPopupWin.document.write("var W = document.body.scrollWidth+30;\n");
	oPopupWin.document.write("var SH = screen.height;\n");
	oPopupWin.document.write("var SW = screen.width;\n");
	oPopupWin.document.write("window.moveTo((SW-W)/2,(SH-H)/2);\n");
	oPopupWin.document.write("window.resizeTo(W,H);\n");
	oPopupWin.document.write("} \najuste();"); // appel au premier coup
	oPopupWin.document.write(" \nsetTimeout(\"ajuste()\",2000);");
		oPopupWin.document.write("</sc"+"r"+"ipt>\n"); // astuce sinon � arrete le script courant 
	oPopupWin.document.write("</bo"+"d"+"y></HT"+"M"+"L>\n"); // idem
	oPopupWin.document.close();
	oPopupWin.focus();    
	return !oPopupWin;
}
// -->
	/*]]>*/
</script>
<?php 
} // fin fonction php jspoploup