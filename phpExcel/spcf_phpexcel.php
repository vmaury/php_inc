<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *  crache un fichier excel avec PHPExcel 
 * @param type $tbentete
 * @param type $table
 * @param type $tbfoot
 */
function ttable2PHPExcel($tbentete, $table, $tbfoot="false", $options = array(), $outfilename = "") {
//	print_r($options);
//	print_r($table);
//	die();
//	

	
	
//	error_reporting(E_ALL & ~E_NOTICE);
//	ini_set('display_errors', TRUE);
	require_once 'PHPExcel.php';
//	ini_set('display_startup_errors', TRUE);
	date_default_timezone_set('Europe/Paris');
			

	if (!$options['Creator']) $options['Creator'] = app_title;
	if (!$options['Title']) $options['Title'] = "Export de ".app_title;
	if (!$options['fileName']) $options['fileName'] = app_title."_export_excel.xls";
	// Create new PHPExcel object
	$objPHPExcel = new PHPExcel();

	/* Set document properties
	$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
								 ->setLastModifiedBy("Maarten Balliauw")
								 ->setTitle("Office 2007 XLSX Test Document")
								 ->setSubject("Office 2007 XLSX Test Document")
								 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
								 ->setKeywords("office 2007 openxml php")
								 ->setCategory("Test result file");*/
	$objPHPExcel->getProperties()->setCreator($options['Creator'])
								 ->setLastModifiedBy($options['Creator'])
								 ->setTitle($options['Title'])
								 ->setSubject($options['Subject'])
								 ->setDescription($options['Description']);

	// ENTETE
	$pCol =0;
	foreach ($tbentete as $k=>$val) {
		$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow($pCol, 1, $val);
		$objPHPExcel->setActiveSheetIndex(0)->getStyleByColumnAndRow($pCol, 1)->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->getCommentByColumnAndRow($pCol,  1)->getFillColor()->setRGB('DDDDDD'); // MARCHE PÔ
		$tbK[$k] = $pCol; // pr la suite
		$pCol ++; 
	}
	// TABLE BODY
	$pLine = 2;
	foreach ($table as $line) {
		foreach ($line as $k=>$val) {
			$val = strip_tags($val);
			if (substr($val,0,1) == "=") {
				$isFormula = true;
			} else {
				$isFormula = false;
				$val = str_replace("&nbsp;", " ", $val);
				if (stristr($val, "€") || $options['format'][$k] == 'fcurr') {
					$val = trim(str_replace("€", "", $val));
					$fcurr = true;
				} else $fcurr = false;
				if (stristr($val, "%")) {
					$val = trim(str_replace("%", "", $val))/100;
					$fpc = true;
				} else $fpc = false;
			}
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow($tbK[$k],  $pLine, $val);
			if ($tbK[$k] == 0) { // colonne d'entête
				$objPHPExcel->setActiveSheetIndex(0)->getStyleByColumnAndRow($tbK[$k],  $pLine)->getFont()->setBold(true);
			}
			if ($fcurr) $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($tbK[$k],  $pLine)->getNumberFormat()->setFormatCode("# ##0.00 €");
			if ($fpc) $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($tbK[$k],  $pLine)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE_00);
			if ($isFormula) $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($tbK[$k],  $pLine)->setDataType(PHPExcel_Cell_DataType::TYPE_FORMULA);
			
		}
		$pLine ++;
	}
	// FOOT
	if (is_array($tbfoot)) {
		foreach ($tbfoot as $k=>$val) {
			$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow($tbK[$k],  $pLine, $val);
			$objPHPExcel->setActiveSheetIndex(0)->getStyleByColumnAndRow($tbK[$k],  $pLine)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->getCommentByColumnAndRow($tbK[$k],  $pLine)->getFillColor()->setRGB('DDDDDD'); // MARCHE PÔ
		}
	}
	/** un test pour ces putasses de formules qui ne marchent pas 
	$objPHPExcel->getActiveSheet()
    ->setCellValue(
        'J10',
        '=SUM(J2:J7)'
    );
	$objPHPExcel->getActiveSheet()->getCell('J10')->setDataType(PHPExcel_Cell_DataType::TYPE_FORMULA); */
	
	// ttes les cols en autosize
	for ($k=0; $k<$pCol; $k++) $objPHPExcel->setActiveSheetIndex(0)->getColumnDimensionByColumn($k)->setAutoSize(true);
	//$objPHPExcel->setActiveSheetIndex(0)->getColumnDimensionByColumn(0)->setAutoSize(true);
	
//	// Add some data
//	$objPHPExcel->setActiveSheetIndex(0)
//				->setCellValue('A1', 'Hello')
//				->setCellValue('B2', 'world!')
//				->setCellValue('C1', 'Hello')
//				->setCellValue('D2', 'world!');
//
//	// Miscellaneous glyphs, UTF-8
//	$objPHPExcel->setActiveSheetIndex(0)
//				->setCellValue('A4', 'Miscellaneous glyphs')
//				->setCellValue('A5', 'éàèùâêîôûëïüÿäöüç');
//
	// Rename worksheet
	$objPHPExcel->getActiveSheet()->setTitle('Simple');


	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	if ($outfilename == "") {
		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$options['fileName'].'');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		$objWriter->save('php://output');
		exit;
	} else {
		$objWriter->save($outfilename);
	}
	//die("zop");
}