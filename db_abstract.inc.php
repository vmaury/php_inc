<?php 
/*   * Fichier abstraction de base de données
   * 
   * @author: Vincent MAURY <artec.vm@arnac.net>
   */

// connection à la bdd
/**
 *  connexion BDD; type dépend de switch($GLOBALS['db_type'])
 * @param type $Host
 * @param type $User
 * @param type $Pwd
 * @param string $DB
 * @param type $portIP
 * @param type $charset
 * @return type
 * @throws Exception
 */
function db_connect($Host,$User,$Pwd, $DB="", $portIP="", $charset="utf8") {
	$GLOBALS["NmChpComment"] = $GLOBALS['db_type'] == "oracle" ? "COMMENT2" : "COMMENT"; // nom de champ COMMENT interdit dans Oracle
	$GLOBALS["CisChpp"] = $GLOBALS['db_type'] == "mysql" ? "`" : ""; // caractère d'isolation des champs péraves
	$applicationsEnv = getenv('APPLICATIONS_ENV');
	if (isset($applicationsEnv) && $applicationsEnv != "") {
		$CryptPasswd = (strtolower(getenv('APPLICATIONS_ENV')) == "prod" ||  strtolower(getenv('APPLICATIONS_ENV')) == "prd") ? "*****" : $Pwd;
	} elseif(defined('typexec')) {
		$CryptPasswd = (strtolower(typexec) == "prod" ||  strtolower(typexec) == "prd") ? "*****" : $Pwd;
	} elseif (isset($GLOBALS['EnvType'])) {
		$CryptPasswd = (strtolower($GLOBALS['EnvType']) == "prod" ||  strtolower($GLOBALS['EnvType']) == "prd") ? "*****" : $Pwd;
	} else $CryptPasswd = "*****";
	switch($GLOBALS['db_type']) {
		case "pgsql":
			if ($DB=="") $DB="template1";
			if ($portIP=="") $portIP=5432;
			$link = pg_connect("host=$Host port=$portIP user=$User password=$Pwd dbname=$DB") ;
			if (!$link)  throw new Exception("Impossible de se connecter au serveur PostGreSql $Host avec le user $User, passwd: $CryptPasswd, db=$DB ");
			$GLOBALS["NmChpOid"] = "oid";
			$GLOBALS["id_vtb"] = "TBAS_";
			$GLOBALS['sqllenstr0'] = " !='' ";
			$GLOBALS['CSpIC'] = "`";
			break;
			
		case "oracle":
			if($DB == '' && $Host != '') $DB = $Host;
			if ($charset == "utf8") $charset = 'AL32UTF8';
			$link = oci_connect($User,$Pwd, $DB, $charset);
			if (!$link)  throw new Exception("Impossible de se connecter au serveur Oracle avec le user $User, passwd: $CryptPasswd, db=$DB ");
			// met le format date par défaut identique à celui de mysql et pgsql pour pas etre emmerdé... sauf quand on veut pas
			if (empty($GLOBALS['no_oci_dateA-format'])) db_query("ALTER SESSION SET NLS_DATE_FORMAT = 'YYYY-MM-DD'",$link);
			// ALTER SESSION SET NLS_NUMERIC_CHARACTERS ='. '; pour mettre le sep décimal à .
			$GLOBALS["NmChpOid"] = "ROWID";
			$GLOBALS['sqllenstr0'] = " IS NOT NULL "; // ces blaireaux d'oracle considèrent une chaine vide comme NULL
			$GLOBALS["id_vtb"] = "_vtb_";
			$GLOBALS['CSpIC'] = '"';
			break;
			
		case "mssql": // Attention, incompatible au dessus de php 5.6
			$link = mssql_connect($Host,$User, $Pwd);
			if (!$link)  throw new Exception("Impossible de se connecter au serveur MSSQL $Host avec le user $User, passwd: $CryptPasswd, db=$DB ");
			// met le format date par défaut identique à celui de mysql et pgsql pour pas etre emmerdé... sauf quand on veut pas
			//if (!$GLOBALS['no_oci_dateA-format']) db_query("ALTER SESSION SET NLS_DATE_FORMAT = 'YYYY-MM-DD'",$link);
			if ($DB != '') mssql_select_db($DB, $link);
			db_query("SET DATEFORMAT ymd", $link);
			$GLOBALS["NmChpOid"] = "";
			$GLOBALS["id_vtb"] = "_vtb_";
			$GLOBALS['CSpIC'] = '"';
			break;
		
		case "sqlsrv":
			if ($portIP != '') $Host .= ",$portIP";
			$connectionInfo = array( "Database"=>$DB, "UID"=>$User, "PWD"=>$Pwd);
			//print_r($connectionInfo);
			$link = sqlsrv_connect( $Host, $connectionInfo);
			if (!$link)  throw new Exception("Impossible de se connecter au serveur SQLSRV $Host avec le user $User, passwd: $CryptPasswd, db=$DB ");
//			db_query("SET DATEFORMAT ymd", $link);
//			$GLOBALS["NmChpOid"] = "";
//			$GLOBALS["id_vtb"] = "_vtb_";
//			$GLOBALS['CSpIC'] = '"';
			break;
			
		case "mysql":
		default:
			$GLOBALS['db_type'] = "mysql";
			if ($portIP=="") $portIP=3306;
			if (function_exists('mysqli_connect') && !isset($GLOBALS['forceOldMysqlDrv'])) {
				$link = mysqli_connect($Host,$User,$Pwd);// or die ("Impossible de se connecter au serveur MYSQL $Host avec le user $User, passwd: ***** ");
				$GLOBALS['mysqlDrv'] = "mysqli";
			} else {
				$GLOBALS['mysqlDrv'] = "mysql-deprec";
				$link = @mysql_connect($Host,$User,$Pwd);// or die ("Impossible de se connecter au serveur MYSQL $Host avec le user $User, passwd: ***** ");
			}
			if (!$link)  throw new Exception("Impossible de se connecter [{$GLOBALS['mysqlDrv']}] au serveur MYSQL $Host avec le user $User, passwd: $CryptPasswd ");
			$GLOBALS['CSpIC'] = "`"; // caractere pour "isoler" les noms de champs merdiques
			$GLOBALS['NmChpOid'] = "";
			$GLOBALS['sqllenstr0'] = "!=''";
			$GLOBALS["id_vtb"] = "_vtb_";
			if ($DB!="") {
				$oksdb = $GLOBALS['mysqlDrv'] == "mysqli" ? mysqli_select_db($link, $DB) : mysql_select_db($DB) ;
				if (!$oksdb) throw new Exception("Impossible d'ouvrir la base de donnees mysql $DB");
			}
			if ($charset != "") {
				if ($GLOBALS['mysqlDrv'] == "mysqli") {
					mysqli_query($link, "SET NAMES '$charset'");
					mysqli_query($link, "SET CHARACTER SET $charset");
					mysqli_set_charset($link, $charset);
				} else {
					mysql_query("SET NAMES '$charset'");
					mysql_query("SET CHARACTER SET $charset");
					mysql_set_charset($charset);
				}
			}
			break;
	}
	$GLOBALS ['db_lnkid'] = $link;
	return($link);
}

function db_close($lnk) {
	switch($GLOBALS['db_type']) {
	case "pgsql":
		pg_close($lnk);
		break;
		
	case "oracle":
		oci_close($lnk);
		break;
	
	case "mssql":
		mssql_close($lnk);
		break;
	
	case "sqlsrv":
		sqlsrv_close($lnk);
		break;
	
	case "mysql":
		default:
			if ($GLOBALS['mysqlDrv'] == "mysqli") {
				 mysqli_close($lnk);
			} else mysql_close($lnk);
		break;
	}
}

/**
 * execution requete
 * @param type $req
 * @param type $lnkid
 * @param type $mserridrq
 * @return type
 * @throws Exception
 */
function db_query($req,$lnkid="",$mserridrq=""){
	if ($GLOBALS['db_lnkid'] != "" && $lnkid == "") $lnkid = $GLOBALS['db_lnkid'];
	if ($lnkid == "" && ($GLOBALS['db_type'] == "oracle" || $GLOBALS['mysqlDrv'] == "mysqli")) throw new Exception("Le lnkid doit etre defini avec une connexion Oracle ou MysqlI; positionner la var _GLOBALS['db_lnkid']");
	$messret = "<br/><br/><b><a href=\"javascript:history.back()\">[RETOUR]</A></b> a la page precedente";
	if (defined('SQL_QUERY_ECHO_DEBUG') && SQL_QUERY_ECHO_DEBUG == 1) {
		echo "<pre>Req sql exécutée sur connexion $lnkid (".$GLOBALS['db_type'].") ".($mserridrq ? ", sid rq=$mserridrq" : "")." :\n$req\n</pre>";
	}
	switch($GLOBALS['db_type']) {
	case "pgsql":
		$ret = $lnkid ? @pg_query($lnkid,$req) : @pg_query($req);
		if (!$ret) throw new Exception("<PRE><U>Requete PgSql invalide</U> : <I>$req</I>\n<br/>$mserridrq\n<br/>( Id de connection =$lnkid \n)<U>Erreur PgSql</U>:"
				. "<I>".pg_last_error()."<br/>".pg_result_error()."</I></PRE>".$messret);
		break;
		
	case "oracle":
		$ret = @oci_parse($lnkid,$req);
		if (!$ret)  throw new Exception("parsing req [$req] oracle plante : ".var_export(oci_error($lnkid), true)) ;
		if (!@oci_execute($ret)) {
			throw new Exception("Execute req  [$req] oracle plante : ".var_export(oci_error($ret), true)); 
		}
		$c = @oci_commit($lnkid);
		if (!$c) throw new Exception("Commit req  [$req] oracle plante : ".var_export(oci_error($lnkid), true)) ;
		if (empty($GLOBALS['force_oci_num_rows'])) $_SESSION['lastOciRq'][$ret] = $req; // memorise la req pour le db_num_rows qui ne marchait pas avec oracle
		break;
		
	case "mssql":
		$ret = $lnkid ? @mssql_query($req,$lnkid) : @mssql_query($req) ;
		if (!$ret) throw new Exception("<PRE><U>Requete mssql invalide</U> : <I>$req</I>\n<br/>$mserridrq\n<br/>( Id de connection =$lnkid \n)<U>Erreur mssql</U>:"
				. "<I>".mssql_get_last_message()."<br/></I></PRE>".$messret);
		break;
	
	case "sqlsrv":
		$ret = sqlsrv_query($lnkid, $req);
		if (!$ret) throw new Exception("<PRE><U>Requete sqlsrv invalide</U> : <I>$req</I>\n<br/>$mserridrq\n<br/>( Id de connection =$lnkid \n)<U>Erreur sqlsrv</U>:"
				. "<I>". sqlsrv_errors()."<br/></I></PRE>".$messret);
		break;

	case "mysql":
	default:	
		if ($GLOBALS['mysqlDrv'] == "mysqli") {
			$ret = mysqli_query($lnkid,$req);
		} else {
			$ret = $lnkid ? @mysql_query($req,$lnkid) : @mysql_query($req) ;
		}
		if (!$ret) {
			$err = $GLOBALS['mysqlDrv'] == "mysqli" ? mysqli_error($lnkid) : mysql_error();
			throw new Exception("<U>Requete mysql invalide</U> : <I>$req</I>\n<br/> (Id de connection =".var_export($lnkid,true)."\n<br/>$mserridrq<br/><U>Erreur mysql</U>:<I>$err</I>".$messret);
		}
		break;
	}
	return $ret;
}

// fonction qui trafique une req sql pr lui rajouter une clause LIMIT
/** AVEC ORACLE c'EST LE BORDEL MONSTRE
Voir l'explication du comportement de ROWNUM là http://www.oracle.com/technology/oramag/oracle/06-sep/o56asktom.html
Comme oracle compte les résultats au fur et à mesure, une req "select * from t where ROWNUM > 1;" ne ramene JAMAIS rien

L'astuce (pourrie) trouvée : on appelle la requete sans condition basse et on affiche pas les $FirstEnr premiers résultats

$GLOBALS["NmChpOid"] est le champs masqué qui sert de clé/identifiant d'enregistrement, uniquement avec Orace et PGSql
**/
function addwherefORlimit($sql,$nblimit,$offset=0) {
	switch ($GLOBALS['db_type']) {
		case "mysql":
			if (!stristr($sql,"LIMIT")) { // si pas deja une clause de limite
				$limitc =  " LIMIT ".($offset>0 ? "$offset," : "").$nblimit;
				$sql .= $limitc; /// on simplifie, normalement il faudrait insérer la clause limit avant "PROCEDURE,FETCH,FOR"
			}
			break;
		
		case "pgsql":
			if (!stristr($sql,"LIMIT") && !stristr($sql,"OFFSET")) { // si pas deja une clause de limite
				$limitc =  " OFFSET $offset LIMIT $nblimit";
				$sql .= $limitc; /// on simplifie, normalement il faudrait insérer la clause limit avant "PROCEDURE,FETCH,FOR"
			}
			break;
	
		case "oracle" :
			if (!stristr($sql,"FETCH FIRST")) { // si pas deja une clause de limite
				$limitc =  " FETCH FIRST $nblimit ROWS ONLY ";
				$sql .= $limitc; /// on simplifie, normalement il faudrait insérer la clause limit avant "PROCEDURE,FETCH,FOR"
			} else {
				
			}/* avant oracle 12
			if (!stristr($sql,"ROWNUM")) { // si pas deja une clause de limite
				if ($offset == 0) {
					//$wherelimit = " (ROWNUM >= $FirstEnr AND ROWNUM <= ".($FirstEnr + $nbligpp).") ";
					$wherelimit = " (ROWNUM <= ".($nblimit)." + UID * 0) "; // voire "REMARQUE SUR LES CLAUSES LIMIT au début de ce fichier"
					if (stristr($sql,"where")) { // si deja "where toto" rajoute "where $wherelimit AND toto" 
						$sql = ustr_ireplace('where','where'.$wherelimit.' AND ',$sql); // mais seulement au premier where
					} else { // si pas where
						$tbsqlkw = array(" group "," having "," window "," union "," intercept "," expect "," order ");
						// recherche les mots clés ci-dessus et rajoute where $wheerlimit avant
						foreach ($tbsqlkw as $sqlkw) {
						if (stristr($sql,$sqlkw)) {
							return (str_ireplace($sqlkw,' WHERE '.$wherelimit.$sqlkw,$sql));
							}
						}
						// si pas trouvé
						$sql .= ' WHERE '.$wherelimit;
					}
				} else { // certainement approximatif, mais fonctionne
					// généré par adminer-master SELECT * FROM (SELECT t.*, rownum AS rnum FROM (SELECT * FROM "REF_PERSONNE" WHERE "PER_NUPERSO" <= '10000') t WHERE rownum <= 150) WHERE rnum > 100
					$sql = trim($sql);
					$posFrom = stripos($sql, " FROM ");
					$sel = substr($sql, 6, $posFrom - 6); // alors d'où sort le 17 c'est un mystère
					$sql = "SELECT $sel FROM (SELECT t.*, rownum AS rnum FROM ($sql) t WHERE rownum <= ".($nblimit + $offset)." + UID * 0 ) WHERE rnum > $offset";
				}
			} else {
				throw new Exception("La fonction addwherefORlimit est incompatible avec une clause ROWNUM existante dans le sql");
			}*/
		break;
		
		case "sqlsrv" : // A TESTER
			if (!stristr($sql,"FETCH NEXT")) { // si pas deja une clause de limite
				$limitc = $offset > 0 ? " OFFSET $offset ROWS " : '';
				$limitc .=  " FETCH NEXT $nblimit ROWS ONLY ";
				$sql .= $limitc; /// on simplifie, normalement il faudrait insérer la clause limit avant "PROCEDURE,FETCH,FOR"
			} 
		break;
	}
	return($sql);
}

/** compte lignes dans une table
 * 
 * @param type $table
 * @param type $where
 * @param type $lnkid
 * @return type
 */
function db_count($table,$where="",$lnkid="") {
	$where = ($where !="" && !stristr($where,"where")) ? " where $where ": $where;
	$res= db_query("select count(*) from $table $where",$lnkid);
	$ret = db_fetch_row($res);	
	return ($ret[0]);
}

/**
 * Retourne requête ss forme de tableau indéxé numériquement
 * @param type $req
 * @param type $lnkid
 * @return type 
 */
function db_qr_compres($req,$lnkid="") {
	$res=db_query($req,$lnkid);
	$i=0;
	$ret = false;
//if (db_num_rows($res)) { // car oci_num_rows ne fonctionne pas avec Oracle !!
	
	while ($rep = (array)db_fetch_row($res)) {
		$ret[$i] = cv2utf8sn($rep);
		$i++;
	}
	
	return($ret);
}

/**
 *  fonction qui effectue une requete et renvoie toutes les lignes dans un tableau asso
 * clé champ  0 :
 * valeur champ 1 et suivants
 * adapté pour utilisation directe dans les listes déroulantes
 * @param unknown_type $req
 * @param unknown_type $lnkid lien
 * @param unknown_type $carsep car sep valeurs
 */
function db_qr_compres4ld($req, $lnkid="", $carsep="-") {
	$res = db_query($req,$lnkid);
	$ret = [];
//if (db_num_rows($res)) { // car oci_num_rows ne fonctionne pas avec Oracle !!
	while ($rep = db_fetch_row($res)) {
		if (is_array($rep)) {
			$rep = cv2utf8sn($rep);
			$ret[$rep[0]] = isset($rep[1]) ? $rep[1] : $rep[0];
			if (isset($rep[2])) $ret[$rep[0]] .= $carsep.$rep[2];
			if (isset($rep[3])) $ret[$rep[0]] .= $carsep.$rep[3];
		}
	}
	return(count($ret) > 0 ? $ret : false);
}


/**
 *  fonction qui effectue une requete et renvoie une seule valeur, la première (utile pour les sommes ...)
 * @param unknown_type $req
 * @param unknown_type $lnkid lien
 */
function db_qr_1val($req, $lnkid="") {
	$res = db_query($req,$lnkid);
	$ret = cv2utf8sn(db_fetch_row($res));
	return (is_array($ret) && isset($ret[0]) ? $ret[0] : null);
}

/** fonction qui effectue une requete et renvoie toutes les lignes dans un tableau 
 * les lignes sont indexes numeriquement
 * les colonnes sont indexe par les noms des colonnes
 * @param type $req
 * @param type $lnkid
 * @return type
 */
function db_qr_comprass($req,$lnkid="") {
$res=db_query($req,$lnkid);
$i=0;
$ret = [];
//if (db_num_rows($res)) { // car oci_num_rows ne fonctionne pas avec Oracle !!
	while ($rep=db_fetch_assoc($res)) {
		$ret[$i] = cv2utf8sn($rep);
		$i++;
	}
	return(count($ret) > 0 ? $ret : false);
//	}
//else return (false);
}

// fonction qui effecture une requete et renvoie la premi�e ligne de r�onse sous forme d'un tableau indic�numeriquement
function db_qr_res($req,$lnkid="") {

$res=db_query($req,$lnkid);
	$ret=cv2utf8sn(db_fetch_row($res));
	if (!$ret) 	{
		$ret[0]="error or no record found";
	}
	return ($ret);
}
// fonction qui effecture une requete et renvoie la premi�e ligne de r�onse sous forme d'un tableau ASSOCIATIF
function db_qr_rass($req,$lnkid="") {
	$res=db_query($req,$lnkid);
	$ret=cv2utf8sn(db_fetch_assoc($res));
	if (!$ret) {
		$ret[0]="error or no record found";
	}
	return ($ret);
}

/** fonction qui effecture une requete et renvoie la premiere ligne de reponse sous forme d'un tableau ASSOCIATIF
 * 
 * @param type $req
 * @param type $lnkid
 * @return type
 */
function db_qr_rass2($req,$lnkid="") {
	$res=db_query($req,$lnkid);
	return  cv2utf8sn(db_fetch_assoc($res));
}

/**
 *  * transforme un tableau asso en morceau d'insert SQL
 * @param string $set tableau des champs=>valeurs
 * @param int $addquotes ajoute quotes : 0: ne fait rien; 1 : ajoute les quotes; 2 : echappe les chaines et ajoute les quotes)
										3 : auto : n'ajoute les quotes que si le nom du champ ne contient pas _NU, _BO, _FL
 */
function tbset2insert($set,$addquotes=false) {
	foreach ($set as $chp=>$val) {
		$lchp[] = $chp;
		$vchp[] = tVal4insSet($val, $chp, $addquotes);
	}
	return("(".implode(",",$lchp).") VALUES (".implode(",",$vchp).")");
}
/**
 * * transforme un tableau asso en morceau de SET SQL
 * @param string $set tableau des champs=>valeurs
 * @param int $addquotes ajoute quotes : 0: ne fait rien; 1 : ajoute les quotes; 2 : echappe les chaines (et ajoute les quotes)
 *										3 : auto : n'ajoute les quotes que si le nom du champ ne contient pas _NU, _BO, _FL
 */
function tbset2set($set,$addquotes=false) {
	foreach ($set as $chp=>$val) {
		$val = tVal4insSet($val, $chp, $addquotes);
		$lchp[] = "$chp = $val";
		}
	return(" ".implode(",",$lchp)." ");
}

/** echappe et ou ajoute quotes : 0: ne fait rien; 1 : ajoute les quotes; 2 : echappe les chaines (et ajoute les quotes)
 * 										3 : auto : n'ajoute les quotes que si le nom du champ ne contient pas _NU, _BO, _FL
 * 
 * @param type $val
 * @param type $chp
 * @param type $addquotes
 */
function  tVal4insSet($val, $chp, $addquotes = 0) {
	if (is_null($val)) return 'NULL';
	$r = $val;
	switch ($addquotes) {
		case 0:
			$r = $val;
		break;

		case 1:
			$r = "'$val'";
		break;

		case 2:
			$r = "'".db_escape_string($val)."'";
		break;

		case 3:
		case 4:
			if ($addquotes == 4 && ((stristr($val, "to_") && stristr($val, "(")) || stristr($val, "nextval")))  { // verrue oracle qui sert pour les (conversions de) dates
				$r = $val;
			} elseif (!(stristr($chp, '_nu') || stristr($chp, '_bo')  || stristr($chp, '_fl'))) {
				$r = "'".db_escape_string($val)."'";
			} else $r = (float)$val;
		break;
	}
	return($r);
}

/**
 *  genere une requete INSERT INTO xxxxx ON DUPLIACTE KEY UPDATE
 * @param string $table nom de la table
 * @param array $tbi tableau des champs=>valeur à insérer
 * @param array $tbset tableau des champs=>valeur à MAJ
 * @param int $addquotes 0: ne fait rien; 1 : ajoute les quotes; 2 : echappe les chaines (et ajoute les quotes)
 */
function insertondkupdate($table,$ins,$set="",$addquotes=false) {
        if ($set == "") $set = $ins;
        return("INSERT INTO $table ".tbset2insert($ins,$addquotes)." ON DUPLICATE KEY UPDATE ".tbset2set($set,$addquotes));
}


function db_escape_string($str, $link = '') {
	if (empty($link)) $link = $GLOBALS['db_lnkid'];
	if (is_array($str)) throw new Exception("Tableau passé en argument au lieu d'une string ".  var_export($str, true));
	switch($GLOBALS['db_type']) {
	case "pgsql":
		return(pg_escape_string($str));
		break;
		
	case "oracle":
		return(dblslashes($str));
		break;
	
	case "mssql":
		return(addslashes($str)); // pas idéal !
		break;
	
	case "sqlsrv":
		return str_replace( "'", "''", str_replace( '"', '""', $str)); 
		break;
	
	case "mysql":
		default:
			if ($GLOBALS['mysqlDrv'] == "mysqli") {
				return(mysqli_real_escape_string($link,$str));
			} else return(mysql_real_escape_string($str));
		break;
	}
}
/** sert à mettre les noms des champs clé en majuscule pour PostGres
 * 
 * @param type $tbl
 */
function upcaseKeysTable(&$tbl) {
	foreach ($tbl as $key => $value) { // pour pg qui ramène les noms de champ en minuscule
		$tbl[strtoupper($key)] = $value;
	}
}
// doubles les cotes pour insert ds Oracle
function dblslashes($str) {
	return str_replace(array("'", '\\'), array("''", '\\\\'), $str);
	return str_replace(array('"', "'", '\\'), array('\\"', "''", '\\\\'), $str);
	//return(str_replace("'","''",$str));
}

// fonction qui renvoie le tableau des champs de PYA
// en fonction de la sensibilité à la casse du sgbd
function rtb_ultchp() {
if (db_case_sens()) {
	return array(
	"NM_TABLE"=>"NM_TABLE",
	"NM_CHAMP"=>"NM_CHAMP",
	"LIBELLE"=>"LIBELLE",
	"ORDAFF_L"=>"ORDAFF_L",
	"TYPAFF_L"=>"TYPAFF_L",
	"ORDAFF"=>"ORDAFF",
	"TYPEAFF"=>"TYPEAFF",
	"VALEURS"=>"VALEURS",
	"VAL_DEFAUT"=>"VAL_DEFAUT",
	"TT_AVMAJ"=>"TT_AVMAJ",
	"TT_PDTMAJ"=>"TT_PDTMAJ",
	"TT_APRMAJ"=>"TT_APRMAJ",
	"TYP_CHP"=>"TYP_CHP",
	"COMMENT"=>"COMMENT");
	}
else return array(
	"NM_TABLE"=>"nm_table",
	"NM_CHAMP"=>"nm_champ",
	"LIBELLE"=>"libelle",
	"ORDAFF_L"=>"ordaff_l",
	"TYPAFF_L"=>"typaff_l",
	"ORDAFF"=>"ordaff",
	"TYPEAFF"=>"typeaff",
	"VALEURS"=>"valeurs",
	"VAL_DEFAUT"=>"val_defaut",
	"TT_AVMAJ"=>"tt_avmaj",
	"TT_PDTMAJ"=>"tt_pdtmaj",
	"TT_APRMAJ"=>"tt_aprmaj",
	"TYP_CHP"=>"typ_chp",
	"COMMENT"=>"comment");
}

// fonction qui renvoie ou non le caractere de protection
// des strings: avec pgsql si on le met avec des entiers ou des numeriques, ca merde
// avec Mysql et Oracle, ça change rien donc on met les ' systématiquement

function rt_carpstr($ttc) {
switch($GLOBALS['db_type']) {
	case "pgsql":
		switch($ttc) {
			case "integer":
			case "int4":
			//case "bool":
			//case "timestamp":
				return("");
			break;
		
			default:
				return("'");
			break;
		}
		
	default:
		return("'");
		break;
	}
}

// fonctions d'acces aux BDD
// fonctions qui renvoie true si la bdd est sensible à la case sur les noms de champs/tables
function db_case_sens() {
switch($GLOBALS['db_type']) {
	case "pgsql":
	case "mssql":
	case "sqlsrv":	
		return(false);
		break;
		
	case "mysql":
	case "oracle":
		default:
		return(true);
		break;
	}
}

// fonction qui retourne le type d'un champ
// Utiliser plutot la fonction ShowField qui retourne un tableau avec beaucoup plus d'infos
function mysqft ($NOMC,$NM_TABLE) {
	$CSpIC = $GLOBALS["CisChpp"];
	$resf=db_query("select $NOMC from $CSpIC$NM_TABLE$CSpIC LIMIT 0");
	return (db_field_type($resf,0));
}
// fonction qui retourne les flags d'un champ
// Utiliser plutot la fonction ShowField qui retourne un tableau avec beaucoup plus d'infos
// et est compatible avec autre chose que MySql....
function mysqff ($NOMC,$NM_TABLE) {
	$CSpIC = $GLOBALS["CisChpp"];
	$resf=db_query("select $NOMC from $CSpIC$NM_TABLE$CSpIC LIMIT 0");
	if ($GLOBALS['mysqlDrv'] == "mysqli") {
		die("Utiliser plutot la fonction ShowField qui retourne un tableau avec beaucoup plus d'infos");
	} else return (mysql_field_flags($resf,0)); 
}
// fonction qui retourne un tableau de hachage des caracteristiques d'un champ
function ShowField($NOMC,$NM_TABLE) {
	$CSpIC = $GLOBALS["CisChpp"];
	$table_def = db_query("SHOW FIELDS FROM $CSpIC$NM_TABLE$CSpIC LIKE '$NOMC'");
	return (db_fetch_array($table_def));
}
// fonction qui renvoie un tableau a 2 dim des caracteristiques du ou des champs d'une table
function db_table_defs($NM_TABLE, $NM_CHAMPD = "*", $owner = "") {
	switch($GLOBALS['db_type']) {
	case "pgsql":
		$table_def= db_query("select $NM_CHAMPD from $NM_TABLE LIMIT 1");
		for ($i=0;$i<db_num_fields($table_def);$i++) {
			$NM_CHAMP=db_field_name($table_def,$i);
			$ret[$NM_CHAMP]['FieldType']=db_field_type($table_def,$i)."(".db_field_size($table_def,$i).")";
			// non encore fait pour postgresql
			$ret[$NM_CHAMP]['FieldValDef']="";
			$ret[$NM_CHAMP]['FieldNullOk']="YES"; // YES ou rien
			$ret[$NM_CHAMP]['FieldKey']=""; // cl�PRI, index=MUL, unique=UNI
			$ret[$NM_CHAMP]['FieldExtra']=""; // auto_increment 
			
			}
		break;
		
	case "mysql":
		$table_def = db_query("SHOW FIELDS FROM $NM_TABLE");
		while ($row_table_def = db_fetch_array($table_def)) {
			if ($NM_CHAMPD=="*" || $NM_CHAMPD==$row_table_def['Field']) {
				$NM_CHAMP=$row_table_def['Field'];
				$ret[$NM_CHAMP]['FieldType']=$row_table_def['Type'];
				//$ret[$NM_CHAMP]['FieldValDef']=($row_table_def['Default']!="" ? $row_table_def['Default'] : "" );
				$ret[$NM_CHAMP]['FieldValDef']=$row_table_def['Default'];
				$ret[$NM_CHAMP]['FieldNullOk']=(strtoupper($row_table_def['Null'])=="YES" ? "yes" : "no"); // YES ou rien
				$ret[$NM_CHAMP]['FieldKey']=($row_table_def['Key']!="" ? $row_table_def['Key'] : ""); // cl�PRI, index=MUL, unique=UNI
				$ret[$NM_CHAMP]['FieldExtra']=$row_table_def['Extra']; // auto_increment 
			}
		} 
		break;

	case "oracle":
		if ($owner!="") $whown =  " AND TC.OWNER='$owner' ";
		$table_def = db_query("SELECT TC.COLUMN_NAME, TC.DATA_TYPE, TC.DATA_LENGTH, TC.DATA_PRECISION,
        TC.DATA_SCALE, TC.NULLABLE, TC.DATA_DEFAULT
      FROM ALL_TAB_COLUMNS TC
      WHERE TC.TABLE_NAME='$NM_TABLE'
      $whown
      ORDER BY COLUMN_ID");
		while ($row_table_def = db_fetch_array($table_def)) {
			if ($NM_CHAMPD=="*" || $NM_CHAMPD==$row_table_def['COLUMN_NAME']) {
				$NM_CHAMP=$row_table_def['COLUMN_NAME'];
				$ret[$NM_CHAMP]['FieldType']=$row_table_def['DATA_TYPE'];
				$ret[$NM_CHAMP]['FieldValDef']=$row_table_def['DATA_DEFAULT'];
				$ret[$NM_CHAMP]['FieldNullOk']=(strtoupper($row_table_def['NULLABLE'])=="Y" ? "yes" : "no"); // YES ou rien
				/*$ret[$NM_CHAMP]['FieldKey']=($row_table_def['Key']!="" ? $row_table_def['Key'] : ""); // cl�PRI, index=MUL, unique=UNI
				$ret[$NM_CHAMP]['FieldExtra']=$row_table_def['Extra']; // auto_increment

				Pour d infos sur les index faire un $tb = db_qr_comprass("select * from ALL_IND_COLUMNS WHERE TABLE_NAME='CHEVAL'");
					 [INDEX_OWNER] => HARASIRE
					[INDEX_NAME] => PK_CHEVAL
					[TABLE_OWNER] => HARASIRE
					[TABLE_NAME] => CHEVAL
					[COLUMN_NAME] => CHE_NUCHEVAL
					[COLUMN_POSITION] => 1
					[COLUMN_LENGTH] => 8
					[CHAR_LENGTH] => 8
					[DESCEND] => ASC
					*/
			}
		} 
		break;

	default:
		$ret=false;
		break;
		
	}
	return($ret);
}     
/** retourne le nbre de résultats d'un select
 * 
 * @param type $res
 * @return type
 */     
function db_num_rows($res) { 
	switch($GLOBALS['db_type']) {
	case "pgsql":
		return(pg_num_rows($res));
		break;
		
	case "oracle":
	/*int oci_num_rows ( resource stmt ) oci_num_rows retourne le nombre de lignes affectées durant la dernière commande Oracle stmt
.	!!Note!! Cette fonction ne retourne pas le nombre de lignes sélectionnées. Pour les commandes de type SELECT, cette fonction va retourner le nombre de ligne qui ont été lues dans le buffer avec oci_fetch* 
	On a fait cette verrue pourrie pompée sur le php.net ds les commentaires, mais qui est inefficace au possible !! */
		if (empty($GLOBALS['force_oci_num_rows']) && $_SESSION['lastOciRq'][$res] && stristr($_SESSION['lastOciRq'][$res],"select")) {
			//$rqs = $_SESSION['lastOciRq'][$res];
			//$ifrom = strripos($rqs,"from"); // DERNIERE occurrence de from, pour les select imbriqués
			//$rp = db_qr_compres("select count(*) ".substr($rqs,$ifrom));
			return db_qr_1val("select count(*) from ({$_SESSION['lastOciRq'][$res]})");
		} else
			return(oci_num_rows($res));
		break;
		
	case "mssql":
		return(mssql_num_rows($res));
		break;
	
	case "sqlsrv":
		return(sqlsrv_num_rows($res));
		break;
	
	case "mysql":
	default:
		if ($GLOBALS['mysqlDrv'] == "mysqli") {
			return(mysqli_num_rows($res));
		} else return(mysql_num_rows($res));
		break;
	}
}

/** retourne le nbre de résultats d'un select
 * 
 * @param type $res
 * @return type
 */     
function db_affected_rows($res = null) { 
	switch($GLOBALS['db_type']) {
	case "pgsql":
		return(pg_affected_rows($res));
		break;
		
	case "oracle":
	/*int oci_num_rows ( resource stmt ) oci_num_rows retourne le nombre de lignes affectées durant la dernière commande Oracle stmt
.	!!Note!! Cette fonction ne retourne pas le nombre de lignes sélectionnées. Pour les commandes de type SELECT, cette fonction va retourner le nombre de ligne qui ont été lues dans le buffer avec oci_fetch* 
	On a fait cette verrue pourrie pompée sur le php.net ds les commentaires, mais qui est inefficace au possible !! */
		if (empty($GLOBALS['force_oci_num_rows']) && $_SESSION['lastOciRq'][$res]) {
			//$rqs = $_SESSION['lastOciRq'][$res];
			//$ifrom = strripos($rqs,"from"); // DERNIERE occurrence de from, pour les select imbriqués
			//$rp = db_qr_compres("select count(*) ".substr($rqs,$ifrom));
			$rp = db_qr_compres("select count(*) from({$_SESSION['lastOciRq'][$res]})");
			return($rp[0][0]);
		} else
			return(oci_num_rows($res));
		break;
		
	case "mssql":
		return(mssql_rows_affected($res));
		break;
	
	case "sqlsrv":
		return(sqlsrv_rows_affected($res));
		break;
	
	case "mysql":
	default:
		if (isset($GLOBALS ['db_lnkid']) && $GLOBALS ['db_lnkid'] != '') {
			if ($GLOBALS['mysqlDrv'] == "mysqli") {
				return(mysqli_affected_rows($GLOBALS ['db_lnkid']));
			} else return(mysql_affected_rows($GLOBALS ['db_lnkid']));
		} else {
			return(mysql_affected_rows());
		}
		break;
	}
}


function db_last_id($res="") { 
	switch($GLOBALS['db_type']) {
	case "pgsql":
		return(pg_last_oid($res));
		break;
	
	case "mssql":
	case "sqlsrv":	
	case "oracle":
		throw new Exception("db_last_id n'existe pas pour le pilote  {$GLOBALS['db_type']}");
		break;
	
	case "mysql":
	default:
		if ($GLOBALS['mysqlDrv'] == "mysqli") {
			return(mysqli_insert_id($GLOBALS ['db_lnkid']));
		} else return(mysql_insert_id());
		break;
	}
}

// renvoie un tableau de la liste des bases / Owners sous Oracle
function db_show_bases() {
	switch($GLOBALS['db_type']) {
	case "pgsql":
		$sql = "SELECT pdb.datname AS Database, pu.usename AS datowner, pg_encoding_to_char(encoding) AS datencoding, 
					(SELECT description FROM pg_description pd WHERE pdb.oid=pd.objoid) AS datcomment
					FROM pg_database pdb, pg_user pu
					WHERE pdb.datdba = pu.usesysid 
					AND pdb.datname NOT LIKE 'template%'
					ORDER BY pdb.datname";
		break;
	
	case "mysql":
		$sql="SHOW DATABASES";
		break;
	
	case "oracle":
		$sql="SELECT DISTINCT OWNER FROM ALL_TABLES ORDER BY OWNER";
		break;
}
	$rep = db_query($sql);
	if (db_num_rows($rep) > 0 || $GLOBALS['db_type']=="oracle") {
		while ($res=db_fetch_row($rep)) $ret[]=$res[0];
		return($ret);
	} else return (false);
}

function db_show_tables($DB="",$showSysTables=false) {
	switch($GLOBALS['db_type']) {
	case "pgsql":
		$sql = "select relname from pg_stat_user_tables order by relname";
		break;
	
	case "mysql":
		$sql="SHOW TABLES"; // from $DB marche plus !!
		break;

	case "oracle":
		//$DB="";
		//$sql="SELECT table_name FROM all_tables WHERE 1=1 ".($DB!="" ? " AND OWNER=upper('$DB') ": "")." ORDER BY table_name";
		if (!$showSysTables) $whereHST = " where table_name NOT LIKE '%$%' ";
		$sql="SELECT distinct table_name FROM all_tables $whereHST ORDER BY table_name";
		break;
	}

	
	$rep=db_query($sql);
	
	while ($res=db_fetch_row($rep)) {
		 $ret[]=$res[0];
			//if (!strstr($res[0],"$")) $ret[]=$res[0]; // special oracle
			
	}
	if (is_array($ret)) {
		return($ret);
	} else return (false);
}
/**
 * 
 * @param type $table
 * @param type $owner (owner pr oracle)
 * @return type
 */
function db_show_fields($table,$owner="") {
	switch($GLOBALS['db_type']) {
	case "pgsql":
		$sql="
		select c.relname, a.attname, t.typname||'('||a.attlen||')', d.adsrc
		from
		pg_class c , pg_attribute a, pg_type t, pg_attrdef d
		where (c.relkind='r' or c.relkind='v') 
		AND a.attrelid = c.oid
		AND t.oid=a.atttypid
		AND d.adrelid=c.oid
		AND d.adnum=a.attnum";
		break;
	
	case "mysql":
		$sql="SHOW FIELDS FROM $table";
		break;
	
	case "oracle":
		if ($owner != "") $whown = " AND OWNER='$owner' ";
		$sql = "SELECT TC.*
			FROM ALL_TAB_COLUMNS TC
			WHERE TC.TABLE_NAME='$table'
			$whown
			ORDER BY COLUMN_ID";
      	return (db_qr_comprass($sql));

		break;
	}
	$rep=db_query($sql);
	if (db_num_rows($rep) > 0 || $GLOBALS['db_type']=="oracle") {
		while ($res=db_fetch_row($rep)) $ret[]=$res[0];
		return($ret);
	} else return (false);
}
		
		

function db_num_fields($res) { 
	switch($GLOBALS['db_type']) {
	case "pgsql":
		return(pg_num_fields($res));
		break;
	
	case "oracle":
		return (oci_num_fields($res));
		break;

	case "mssql":
		return(mssql_num_fields($res));
		break;
	
	case "sqlsrv":
		return(sqlsrv_num_fields($res));
		break;
	
	case "mysql":
	default:
		if ($GLOBALS['mysqlDrv'] == "mysqli") {
			return(mysqli_num_fields($res));
		} else return(mysql_num_fields($res));
		break;
	}
}

function db_field_name($res,$i) { 
	switch($GLOBALS['db_type']) {
	case "pgsql":
		return(pg_field_name($res,$i));
		break;
	
	case "oracle":
		return(oci_field_name($res,($i + 1))); // dans oracle les index de champ commencent à 1
		break;
	
	case "mssql":
		return(mssql_field_name($res,$i));
		break;
	
	case "sqlsrv":
		throw new Exception("db_field_name n'existe pas pour le pilote  {$GLOBALS['db_type']}");
		//@see sqlsrv_field_metadata($res) ou sqlsrv_get_field(resource $stmt, int $fieldIndex);
		break;
	
	case "mysql":
	default:
		if ($GLOBALS['mysqlDrv'] == "mysqli") {
			return(mysqli_field("name",$res,$i));
		} else return(mysql_field_name($res,$i));
		break;
	}
}
// renvoie un tableau contenant le nom de tous les champs
function db_list_fields($res) {
	for($i=0;$i<db_num_fields($res);$i++) {
		$listf[]=db_field_name($res,$i);
	}
	return($listf);
}

function db_field_size($res,$i) { 
	switch($GLOBALS['db_type']) {
	case "pgsql":
		return(pg_field_size($res,$i));
		break;
		
	case "oracle":
		return(oci_field_size($res,($i + 1)));// dans oracle les index de champ commencent à 1
		break;
	
	case "mssql":
		return(mssql_field_length($res,$i));
		break;
	
	case "sqlsrv":
		throw new Exception("db_field_size n'existe pas pour le pilote  {$GLOBALS['db_type']}");
		//@see sqlsrv_field_metadata($res) ou sqlsrv_get_field(resource $stmt, int $fieldIndex);
		break;
	
	case "mysql":
	default:
		if ($GLOBALS['mysqlDrv'] == "mysqli") {
			return(mysqli_field("length",$res,$i));
		} else return(mysql_field_len($res,$i));
		break;
	}
}

function db_field_type($res,$i) { 
	switch($GLOBALS['db_type']) {
	case "pgsql":
		return(pg_field_type($res,$i));
		break;
		
	case "oracle":
		return(oci_field_type($res,($i + 1)));// dans oracle les index de champ commencent à 1
		break;
	
	case "pgsql":
		return(mssql_field_type($res,$i));
		break;
	
	case "sqlsrv":
	case "mssql":
		throw new Exception("db_field_type n'existe pas pour le pilote  {$GLOBALS['db_type']}");
		//@see sqlsrv_field_metadata($res) ou sqlsrv_get_field(resource $stmt, int $fieldIndex);
		break;
	
	case "mysql":
	default:
		if ($GLOBALS['mysqlDrv'] == "mysqli") {
			return(mysqli_field("type",$res,$i));
		} else return(mysql_field_type($res,$i));
		break;
	}
}

/** renvoie les tables d'un champ lors d'une requete
/// !! sur Oracle et pgsql, cette fonction ne fonctionne que s'il n'existe PAS 2 champs portant le même nom dans la base !!
/// si ça renvoie la première réponse...
 * 
 * @param type $res
 * @param type $i
 * @return type
 */
function db_field_table($res,$i) { // !! attention pas d'equivalent en postgresql ni en Oracle !!
	switch($GLOBALS['db_type']) {
	case "pgsql": // dur dur �d�erminer
		$rep = db_qr_res("select pg_class.relname from pg_class,pg_attribute where pg_class.oid=pg_attribute.attrelid and pg_attribute.attname='".db_field_name($res,$i)."' and pg_class.relkind='r'");
		return($rep[0]);
		break;
	
	case "oracle":
		$rep = db_qr_res("select TABLE_NAME from ALL_TAB_COLUMNS where COLUMN_NAME='".db_field_name($res,$i)."'");
		return($rep[0]);
		break;
	
	case "sqlsrv":
	case "mssql":
		throw new Exception("db_field_table n'existe pas pour le pilote  {$GLOBALS['db_type']}");
		//@see sqlsrv_field_metadata($res) ou sqlsrv_get_field(resource $stmt, int $fieldIndex);
		break;
	
	
	case "mysql":
	default:
		if ($GLOBALS['mysqlDrv'] == "mysqli") {
			return(mysqli_field("table",$res,$i));
		} else return(mysql_field_table($res,$i));
		break;
	}
}

/** retourne record comme un tableau à indic numérique
 * 
 * @param type $res ressource
 * @return array
 */
function db_fetch_row($res) { 
	switch($GLOBALS['db_type']) {
	case "pgsql":
		return(pg_fetch_row($res));
		break;
	
	case "oracle":
		return(oci_fetch_row($res));
		break;
	
	case "mssql":
		return cv2utf8sn(mssql_fetch_row($res));
		break;
	
	case "sqlsrv":
		return sqlsrv_fetch_array($res, SQLSRV_FETCH_NUMERIC);
		break;
	
	case "mysql":
	default:
		if ($GLOBALS['mysqlDrv'] == "mysqli") {
			return(mysqli_fetch_row($res));
		} else return(mysql_fetch_row($res));
		break;
	}
}

/** retourne record comme un tableau à indic alpha ET numérique
 * 
 * @param type $res ressource
 * @return array
 */
function db_fetch_array($res) { 
	switch($GLOBALS['db_type']) {
	case "pgsql":
		return(pg_fetch_array($res));
		break;
	
	case "oracle":
		return(oci_fetch_array($res));
		break;
	
	case "mssql":
		return cv2utf8sn(mssql_fetch_array($res));
		break;
	
	case "sqlsrv":
		return sqlsrv_fetch_array($res);
		break;
	
	case "mysql":
	default:
		if ($GLOBALS['mysqlDrv'] == "mysqli") {
			return(mysqli_fetch_array($res));
		} else return(mysql_fetch_array($res));
		break;
	}
}
/** retourne record comme un tableau assoc 
 * 
 * @param type $res ressource
 * @return array
 */
function db_fetch_assoc($res) { 
	switch($GLOBALS['db_type']) {
	case "pgsql":
		return(pg_fetch_assoc($res));
		break;
	
	case "oracle":
		return(oci_fetch_assoc($res));
		break;
	
	case "mssql":
		return cv2utf8sn(mssql_fetch_assoc($res));
		break;
	
	case "sqlsrv":
		return sqlsrv_fetch_array($res, SQLSRV_FETCH_ASSOC);
		break;
	
	case "mysql":
	default:
		if ($GLOBALS['mysqlDrv'] == "mysqli") {
			return(mysqli_fetch_assoc($res));
		} else return(mysql_fetch_assoc($res));
		break;
	}
}


function db_fetch_object($res) { 
	switch($GLOBALS['db_type']) {
	case "pgsql":
		return(pg_fetch_object($res));
		break;
	
	case "oracle":
		return(oci_fetch_object($res));
		break;
	
	case "mssql":
		return(mssql_fetch_object($res));
		break;
	
	case "sqlsrv":
		return sqlsrv_fetch_object($res);
		break;
	
	case "mysql":
	default:
		if ($GLOBALS['mysqlDrv'] == "mysqli") {
			return(mysqli_fetch_object($res));
		} else return(mysql_fetch_object($res));
		break;
	}
}
/// deprecated mais il doit en rester pas mal...
// connection et selection eventuelle d'une base
function msq_conn_sel($Host,$User,$Pwd,$DB="") {
     return(db_connect($Host,$User,$Pwd,$DB)) ;
	}

// fonction qui effectue une requete sql, et affiche une erreur avec la requete si necessaire
function msq($req,$lnkid="",$mserridrq="") {
	return (db_query($req,$lnkid="",$mserridrq=""));
}


/**
 * Fonction qui sert à former une clause IN à partir de valeurs séparées par un $caractere
 * exemple : select multiple renvoie 'O,1,2,3' 
 * 			 cette fonction transforme en ('1','2','3')
 */
function selectMultiple2inClause($tableau,$caractere){
	$inClause = '(';
	foreach( explode($caractere,$tableau) as $element ){
		if( $inClause != '(' ){// Pour n'ajouter la virgule qu'apres le premier passage
			$inClause .= ',';
		}
		$inClause .= "'".$element."'";
	}
	return $inClause.')';
}

/** diverses propriétés d'un champ...
 * 
 * @param type $prop
 * @param type $result
 * @param type $field_offset
 * @return type
 */
function mysqli_field($prop, $result, $field_offset)
{
    $properties = mysqli_fetch_field_direct($result, $field_offset);
    return is_object($properties) ? $properties->$prop : null;
}
/** remplace mysql_field_flags
 * 
 * @param type $result
 * @param type $Idf
 * @return type
 */
function mysqlivm_field_flags($result,$Idf) {
	if ($GLOBALS['mysqlDrv'] == "mysqli") {
		return(h_flags2txt(mysqli_field("flags",$result,$Idf)));
	} else return mysql_field_flags($result,$Idf) ;
}
/** convertit l'entier pourri des flags en texte (comme avant)
 * 
 * @global type $mysqliflags
 * @param type $flags_num
 * @return type
 */
function h_flags2txt($flags_num)
{
    global $mysqliflags;

    if (!isset($mysqliflags))
    {
        $mysqliflags = array();
        $constants = get_defined_constants(true);
        foreach ($constants['mysqli'] as $c => $n) if (preg_match('/MYSQLI_(.*)_FLAG$/', $c, $m)) if (!array_key_exists($n, $mysqliflags)) $mysqliflags[$n] = $m[1];
    }

    $result = array();
    foreach ($mysqliflags as $n => $t) if ($flags_num & $n) $result[] = $t;
	if (in_array("PRI_KEY", $result)) $result[] = "PRIMARY"; // pour compat pya
    return implode(' ', $result);
}

/** encode en utf8 si nécéssaire (en mode mssql)
 * 
 * @param type $tb
 * @return type
 */
function cv2utf8sn($tb) {
	if ($GLOBALS['db_type'] == "mssql" && is_array($tb) && count($tb) >0) {
		$tbr = array();
		foreach ($tb as $k=>$v) $tbr[$k] = utf8_encode ($v);
		return $tbr;
	} else return $tb;
}