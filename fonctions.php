<?php 
/* FICHIER DE FONCTIONS */
// PARTAGE PAR TOUTES LES APPLIS
// abstraction BDD
require_once(__DIR__."/db_abstract.inc.php");
// fonctions liées à PYA
include_once(__DIR__."/fonctions_pya.php"); 
// fonctions JS,AJAX, DOM HTML diverses
require_once(__DIR__."/fonctions_js_html.php");
// et tt ce qui concerne l'objet PYA
require_once(__DIR__."/PYAobj.class.php");
// // fonctions servant au debogage
require_once(__DIR__."/debug_tools.inc"); 

// attrape toutes les exceptions survenant qui ne sont pas catchées
// il est possible d'écraser cela dans l'appli afin de mieux charter les exceptions...
function exception_handler($exception) {
  echo "<pre>\n\n
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  Exception levée : \"" , $exception->getMessage(), "\", code \"" . $exception->getCode()."\"\n";
      // these are our templates
    $traceline = "#%s %s(%s): %s(%s)";
    $msg = "Detail de l'erreur :  %s avec le message '%s' \n survenue dans %s, ligne %s\n\nEtat de la pile:\n%s\n  ==>> %s ligne %s";

    // alter your trace as you please, here
    $trace = $exception->getTrace();
//    if ($GLOBALS['EnvType'] != "prod") {
//		foreach ($trace as $key => $stackPoint) {
//			// I'm converting arguments to their type
//			// (prevents passwords from ever getting logged as anything other than 'string')
//			$trace[$key]['args'] = array_map('gettype', $trace[$key]['args']);
//		}
//	} 

    // build your tracelines
    $result = array();
    foreach ($trace as $key => $stackPoint) {
        $result[] = sprintf(
            $traceline,
            $key,
            $stackPoint['file'],
            $stackPoint['line'],
            $stackPoint['function'],
			var_export($stackPoint['args'], true)
            //is_object($stackPoint['args']) ? var_export($stackPoint['args'], true) : (is_array($stackPoint['args']) ? implode(', ', $stackPoint['args']) : $stackPoint['args'])
			//is_array($stackPoint['args']) ? implode(', ', $stackPoint['args']) : $stackPoint['args']
        );
    }
    // trace always ends with {main}
    $result[] = '#' . ++$key . ' {main}';

    // write tracelines into main template
    $msg = sprintf(
        $msg,
        get_class($exception),
        $exception->getMessage(),
        $exception->getFile(),
        $exception->getLine(),
        implode("\n", $result),
        $exception->getFile(),
        $exception->getLine()
    );

    // log or echo as you please
    echo '<pre>'.$msg."\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!";
}
// dit à php qu'il faut traiter les exceptions avec la fonction ci-dessus
set_exception_handler('exception_handler');

/** remise en forme d'une date en francais j/m => jj/mm/aaaa
	@param boolean $rettb retourne un tableau
*/
function rmfDateF($DateOr, $rettb=false){
	$tab = explode("/",$DateOr);
	$tab[0] = (int)$tab[0]+0; // conversions de type
	$tab[1] = (int)$tab[1]+0;
	$tab[2] = corrYear((int)$tab[2]+0);
	if ($tab[1] <10) $tab[1] = "0".$tab[1];
	if ($rettb) {
		return($tab);
	} else return(implode("/",$tab));
}
// corrige année si elle est nulle
function corrYear($y) {
	$y = (int)$y;
	if ($y == 0) $y = date("Y");
	if ($y < 70) $y += 2000;
	if ($y > 70 && $y < 100) $y += 1900;
	return($y);
}
/**  conversion d'une date en francais jj/mm/aa vers anglais aaaa-mm-jj
 * 
 * @param string $DateOr
 * @return type
 */
function DateA($DateOr){
	if (strstr($DateOr,"/")) { // si pas de /, fait rien elle est peut-etre deja au bon format
		$tab =  rmfDateF($DateOr,true);
		$DateOr = $tab[2]."-".$tab[1]."-".$tab[0];
	}
	return($DateOr);
}
/**  conversion d'une datetime en francais jj/mm/aa 00:00(:00) vers anglais aaaa-mm-jj 00:00(:00)
 * 
 * @param string $DateOr
 * @return type
 */
function DateTimeA($DateOr){
	if (strstr($DateOr," ")) { // si pas de -, fait rien elle est peut-etre deja au bon format)
		$tbd = explode(" ", $DateOr);
		return(DateA($tbd[0])." ".$tbd[1]);
	}
	return(DateA($DateOr));
}

/** conversion d'une date en anglais aa-mm-jj vers francais jj/mm/aaaa 
 * 
 * @param string $DateOr
 * @return type
 */
function DateF($DateOr){
	if (strstr($DateOr,"-")) { // si pas de -, fait rien elle est peut-etre deja au bon format
		$tab = explode("-",$DateOr);
		//print_r($tab);
		$tab[0] = (int)$tab[0]+0;
		$tab[1] = (int)$tab[1]+0;
		// si ya l'heure à la suite
		$time = false;
		if (strstr($tab[2]," ")) {
			list($tab[2], $time) = explode(" ", $tab[2]);
		}
		if ($tab[1] < 10) $tab[1] = "0".$tab[1];
		$DateOr = $tab[2]."/".$tab[1]."/".$tab[0].($time ? " $time" : "");
	}
	return($DateOr);
}
/**  conversion d'une datetime en anglais aaaa-mm-jj 00:00:00 vers francais jj/mm/aa 00:00:00
 * 
 * @param string $DateOr
 * @return type
 */
function DateTimeF($DateOr){
	if (strstr($DateOr," ")) { // si pas de -, fait rien elle est peut-etre deja au bon format)
		$tbd = explode(" ", $DateOr);
		// vire les secondes qui mettent la merde
		if (strstr($tbd[1],":")) {
			$tbh = explode(":", $tbd[1]);
			return(DateF($tbd[0])." ".$tbh[0].":".$tbh[1]);
		}
		return(DateF($tbd[0])." ".$tbd[1]);
	}
	return(DateF($DateOr));
}

/**  conversion d'une date en francais vers un timestamp
 * 
 * @param type $DateStr
 * @return type
 */
function DateF2tstamp($DateStr) {
	if (trim($DateStr) == "" || !strstr($DateStr,"/")) return (0);
	$tab = rmfDateF($DateStr,true);
	if ($tab[2]>=2038) $tab[2] = 2037; // bug an 2038
	return(mktime(0,0,0,$tab[1],$tab[0],$tab[2]));
}
/**  conversion d'une datetime en francais vers un timestamp
 * 
 * @param type $DateStr
 * @return type
 */
function DateTimeF2tstamp($DateStr) {
	if (trim($DateStr) == "" || !(strstr($DateStr,"/") || strstr($DateStr,":"))) return (0);
	$tbdt = explode(" ", $DateStr);
	$tbd = explode("/", $tbdt[0]);
	$tbt = explode(":", $tbdt[1]);
	return(mktime($tbt[0]+0,$tbt[1]+0,$tbt[2]+0,$tbd[1],$tbd[0],$tbd[2]));
}
/**  conversion date en anglais vers timestamp
 * 
 * @param type $DateStr
 * @return type
 */
function DateA2tstamp($DateStr) {
	if (trim($DateStr)=="" || !strstr($DateStr,"-")) return (0);
	$tab=explode("-",$DateStr);
	$tab[0] = corrYear((int)$tab[0]+0);
	$tab[1] = (int)$tab[1]+0;
	$tab[2] = (int)$tab[2]+0;
	if ($tab[0]>=2038) $tab[0]=2037; // bug an 2038
	return(mktime(0,0,0,$tab[1],$tab[2],$tab[0]));
}
/**  conversion d'une datetime en anglais vers un timestamp
 * 
 * @param type $DateStr
 * @return type
 */
function DateTimeA2tstamp($DateStr) {
	if (trim($DateStr) == "" || !(strstr($DateStr,"-") || strstr($DateStr,":"))) return (0);
	$tbdt = explode(" ", $DateStr);
	$tbd = explode("-", $tbdt[0]);
	$tbt = explode(":", $tbdt[1]);
	return(mktime($tbt[0]+0,$tbt[1]+0,$tbt[2]+0,$tbd[1],$tbd[2],$tbd[0]));
}

// affichage heure : min à partir d'un entier (format webcalendar)
function DispHRMN($hr) {
	return (c2c(floor($hr/100))."h".c2c($hr % 100)."mn");
}


/**
 * Transforme un nombre au format monétaire Français
 * 
 * @param type $number : le nombre à formater 
 * @param type $euro   : true si vous voulez afficher le caractère euro
 * @param type $dispZero : false pour afficher des - quand nbre = 0
 */
function monetaireFr($number,$euro = false, $dispZero=true){
	if ($number == 0 && !$dispZero) return '-';
	return number_format($number, 2,',', ' ').($euro ? '&euro;' : '');
}

/**
 * Fonction qui transforme un nombre au format excel vers un float "insertable" en mysql 
 * Supprime les espaces, les '€' et remplace les virgules par des points 
 * @param unknown_type $number
 */
function monetaire2float($number){
	return str_replace( array(' ','€','"') ,'',str_replace(',','.',$number));
}


/**
 * force affichage d'un nombre a 2 chiffres (pr affichage des minutes et heures par ex.) 
 * @param string $nb
 * @return type
 */
function c2c($nb) {
	if ($nb < 10) $nb = "0".$nb;
	return ($nb);
}
/**
 * equivalent de substr qui ne marche pas avec les chaines utf8!!
 * @param type $str
 * @param type $from
 * @param type $len
 * @return type
 */
function substrutf8($str,$from,$len){
    return preg_replace('#^(?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){0,'. $from .'}'.'((?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){0,'. $len .'}).*#s','$1', $str);
}

/**
 * lecture d'un fichier csv : première ligne contient les clés, suivantes les valeurs des clés
 * renvoie un tableau à deux dimensions $tb[col][ligne]=
 * utilisé dans GRH2 pour les profils, syncadeenov pr les filtres
 * 
 * @param type $file chemin fichie
 * @param type $cs car de séparation, par def ";"
 * @return type tableau à deux dimensions $tb[col][ligne]=
 */
function readfile2tb($file,$cs=";") {
	if (file_exists($file)) {
		$h=fopen($file,'r');
		while (!feof($h)) {
			$lig = fgets($h);
			if ( trim($lig) != '' && substr($lig,0,1) != "#") { // pas commentaire
				$i++;
				$lig = trim($lig);
				if ($cs != chr(9)) $lig = str_ireplace(chr(9), '', $lig); // vire les tab si c pas le char de separation 
				if ($i == 1) { // ligne entête
					$tbhead = explode($cs,$lig);
					$j=0;
					foreach($tbhead as $head) {
						if ($j>0) $tb[trim($head)] =  array();
						$j++;
					}
				} else { // ligne "normale"
					$tblig = explode($cs,$lig);
					$j =  0;
					foreach($tbhead as $head) {
						if ($j>0) $tb[$head][trim($tblig[0])] = trim($tblig[$j]);
						$j++;
					}
				}
			}
		}
		fclose($h);
		return ($tb);
	} else {
		echo "Erreur : impossible de lire $file";
		return (false);
	}
}

/** fonction qui trime ts les éléments d'un tableau (réentrante)
 * 
 * @param type $tb
 * @return type
 */
function trimtbl($tb) {
	if (is_array($tb)) {
		foreach ($tb as $c=>$v) $tb[$c] = trimtbl($v);
		return($tb);
	} else return (trim($tb));
}


/** fonction qui convertit un tableau associatif en XML; ré-entrante (à peaufiner..)
 * 
 * @param type $tbl
 * @param type $xmlbal boolean renvoie ou non <XML> </XML>
 * @param int $niv niveau profondeur (sert pr les indentations)
 * @return type
 */
function table2XML($tbl,$xmlbal = true,$niv=0) {
	$niv +=3;
	if (is_array($tbl)) {
		foreach ($tbl as $c=>$v) {
			if (is_array($v)) {
				$ret .= str_repeat(" ",$niv)."<$c>\n".table2XML($v,false,$niv).str_repeat(" ",$niv)."</$c>\n";
			} else $ret .= str_repeat(" ",$niv)."<$c>$v</$c>\n";
		}
	} else $ret .= $tbl;
	
	if ($xmlbal) { return ("<XML>\n$ret\n</XML>\n"); } else return ("$ret");
}

/** fonction qui vire les x derniers car d'une chaine
 * 
 * @param type $strap
 * @param type $nbcar
 * @return type
 */
function vdc($strap,$nbcar) {
	return (substr($strap,0,strlen($strap)-$nbcar));
}

/** fonction qui renvoie x espaces insécables
 * 
 * @param type $i
 * @return type
 */
function nbsp($i=1){
	return(str_repeat("&nbsp;",$i));
}


/** test si une chaine correspond à un fichier image
 ie si son nom contient l'extension .gif, .jpeg, etc ...
 * 
 * @param type $Nmf
 * @return type
 */
function TestNFImg($Nmf) {
	return preg_match('`(.gif|.jpg|.jpeg|.png|.tif)`i', $Nmf);
}

/** fonction qui coupe une chaine a la longueur desiree, sans couper les mots
 * 
 * @param type $strac chaine a couper
 * @param type $long longeur
 * @param type $strsuite skon met à la fin pr indiquer qu'on coupe
 * @param type $cut a priori force le cut si nom ss espace trop long (genre nom de fichier)

 * @return type
 */
function tronqstrww ($strac,$long=50,$strsuite=" [...]",$cut=false) {
	if (strlen($strac)<=$long) return $strac;
	$strac = wordwrap($strac,$long,"coucou",$cut);
	$tbstrac= explode("coucou",$strac);
	return($tbstrac[0].$strsuite);
}
/** fonction qui coupe une chaine a la longueur desiree
 * 
 * @param type $strac chaine a couper
 * @param type $long longeur
 * @param type $strsuite skon met à la fin pr indiquer qu'on coupe
 * @return type
 */
function tronqstrsimpl ($strac,$long=50,$strsuite=" [...]") {
	if (strlen($strac)<=$long) return $strac;
	return (substr($strac, 0, $long).$strsuite);
}
// magic_quotes_gpc est maintenant à On par défaut
function unescape($text) {
  if(get_magic_quotes_gpc()) {
    $text = stripslashes($text);
  }
  return($text);
} 
function escape($text) {
  if(!get_magic_quotes_gpc()) {
    $text = db_escape_string($text);
  }
  return($text);
}

/** verifie qu'une adresse mail est valide
 ie un @, pas d'espaces et pas de retour chariots
 * 
 * @param type $admail
 * @return type
 */
function VerifAdMail($admail) {
         if (strstr($admail,"@") && !strstr($admail," ")  && !strstr($admail,"\n"))
                  { return (true) ;}
         else return(false);
}


/** reformate n° de tel 
 * 
 * @param type $tel
 * @return type
 */
function tel_format($tel) {
  $tel = str_replace(array(" ",".",","),array(),$tel);
  $tel_format = chunk_split ($tel, 2 ," " );
  return $tel_format; 
}


/** convertit une chaine avec des accents en ascii pur
 * 
 * @param type $text
 * @param type $from_enc encodage (def ="UTF-8")
 * @return type
 */
function cv2purascii($text,$from_enc="UTF-8") {
	setlocale(LC_CTYPE, 'fr_FR');
	return(iconv($from_enc,'ASCII//TRANSLIT', (string)$text));
}
/** convertit une chaine avec des accents en ascii pur
 * car celle ci-dessus n'est pas fiable
 * 
 * @param type $str
 * @return type
 */
function cv2purasci2( $str )
{
    return strtr(utf8_decode($str), 
        utf8_decode(
        'ŠŒŽšœžŸ¥µÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýÿ'),
        'SOZsozYYuAAAAAAACEEEEIIIIDNOOOOOOUUUUYsaaaaaaaceeeeiiiionoooooouuuuyy');
}
/** efface un fichier s'il existe
 * 
 * @param type $ChemFich
 */
function delfich($ChemFich) {
  // echo "Chemin complet du fichier a effacer :$ChemFich<br/>";
  if (file_exists($ChemFich)) unlink ($ChemFich);
  }
  


 /** recupere un libelle dans une table fonction de la cle// sert aussi a tester si un enregistrement existe (renvoie faux sinon)
  * 
  * @param type $Table
  * @param type $ChpCle
  * @param type $ChpLib
  * @param type $ValCle
  * @param type $lnkid
  * @param type $wheresup
  * @param type $trace
  * @return type
  */
function RecupLib($Table, $ChpCle, $ChpLib, $ValCle, $lnkid="", $wheresup="", $trace=false) {
	$CSpIC = $GLOBALS["CisChpp"];
	$ValCle = db_escape_string($ValCle, $lnkid);
	$wheresup = ($wheresup!="" ? " AND ".$wheresup : "");
	$req = "SELECT $ChpCle, $ChpLib FROM $CSpIC$Table$CSpIC WHERE $ChpCle='$ValCle' $wheresup";
	if ($trace) echo $req;
	$reqRL = db_query($req, $lnkid);
	$resRL = db_fetch_row($reqRL);
	if ($resRL) {
		return($resRL[1]);
	} else return (false);
}


/** fonctions tableau arrangées qui déclenchent (PAS) une erreur si l'argument n'est pas un tableau..
 * 
 * @param type $glue
 * @param type $pieces
 * @return type
 */
function is_arr_implode($glue, $pieces) {
	if (is_array($pieces)) 	return(implode($glue,$pieces));
}


/** test si element dans tableau; si tableau pas tableau ne declenche PAS d'erreur
 * 
 * @param type $needle
 * @param type $haystack
 * @return type
 */
function is_arr_in_array($needle, $haystack) {
	if (is_array($haystack)) {
		return(in_array($needle,$haystack));
	} else return(false);
}

/**
 * equivalent de stristr mais cherche dans un tableau
 * @param type $tb haystack
 * @param type $needle
 */
function stritbstr($tb,$need) {
	foreach ($tb as $ks) {
		if (stristr($need, $ks)) return true;
	}
	return (false);
}

/**  la meme que array_key_exists meme qui peut prendre un arg de key qui soit un tableau
 * 
 * @param type $keys
 * @param type $array
 * @return boolean
 */
function array_keys_exists($keys,$array) {
	if (!is_array($keys)) $keys = array($keys);
    foreach($keys as $k) {
        if(!isset($array[$k])) {
        return false;
        }
    }
    return true;
}


/** concatenation de tableaux en préservant les index quelqu'ils soient
 * 
 * @param type $tb1
 * @param type $tb2
 * @return type
 */
function array_concat($tb1, $tb2) {
	if (!$tb2) return($tb1);
	foreach ($tb1 as $k=>$v) {
		$tb3[$k]=$v;
	}
	foreach ($tb2 as $k=>$v) {
		$tb3[$k]=$v;
	}
	return($tb3);
}


/** // convertit une chaine de type 0:Non demandé,1:Refusé,2:Accepté,3:A revoir en tableau de hachage
 * 
 * @param type $string
 * @param type $sep1
 * @param type $sep2
 * @return type
 */
function hash_explode($string, $sep1 = ":", $sep2 = ",") {
	$tbvrac = explode($sep2, $string);
	foreach($tbvrac as $tbelem) {
		$tbe = explode($sep1, $tbelem);
		if ($tbe[1] == "") {
			$tbe[1] = $tbe[0];
		} else { // à cause des config de l'active directory
			for ($i=2;$i<=10;$i++) {
				if (isset($tbe[$i])) {
					$tbe[1].= $sep1.$tbe[$i];
				} else break;
			}
		}
		$res[$tbe[0]] = $tbe[1];
	}
	return($res);
}
/** convertit tableau de hachage en chaine de type  0:Non demandé,1:Refusé,2:Accepté,3:A revoir 
 * 
 * @param type $array
 * @param type $sep1
 * @param type $sep2
 * @return type
 */
function hash_implode($array,$sep1=":",$sep2=",") {
	if (is_array($array) && count($array)>0) {
		foreach($array as $k=>$v) {
			if ($k!="") $res.=$k.$sep1.$v.$sep2;
		}
		$res = vdc($res,1);
		return($res);
	}
}
/**
 * La même que explode, mais ignore les chaines vides
 * @param unknown_type $sep
 * @param unknown_type $str
 * @param bool $trim si true ignore aussi les chaines composées d'espaces
 */
function explode_nn($sep, $str, $trim = false) {
	$tb = explode($sep, $str);
	if (is_array($tb)) {
		foreach ($tb as $k=>$v) {
			if ($trim) $v = trim($v);
			if ($v == "") unset($tb[$k]);
		}
		return($tb);
	} else return(array());
}
/** convertit tableau ["caca", "pipi"] en chaine a mettre ds un sql in
 * 'caca','pipi'
 * 
 * @param type $tb
 * @return type
 */
function tb2sqlin($tb) {
	if (is_array($tb)) {
		$tb2 = false;
		foreach ($tb as $val) {
			$tb2[] = "'".$val."'";
		}
		if (!$tb2) return ("''");
		return(implode(",",$tb2));
	} else return("'".$tb."'");
}
/** fonction utilisée pour le multilinguisme
* les lib contiennent liblang0£liblang1£liblang3 ...
* le n° de langue est stocké dans la var de session $_SESSION['NoLang']
 *
 * @param type $lib le lib a traduire
 * @return type 
 */
function tradLib($lib) {
	/// TODO : merde avec le séparateur de langue £ qui n'est pas un car ascii normal...
	$CARSEP_LANG = $GLOBALS['CARSEP_LANG'] ? $GLOBALS['CARSEP_LANG'] :  utf8_decode("£");
	if (strstr($lib,$CARSEP_LANG)) {
		$tblib = explode($CARSEP_LANG, $lib);
		$lib = $tblib[0];
		if (isset($_SESSION['NoLang'])) {
			if ($tblib[$_SESSION['NoLang']] !="") return($tblib[$_SESSION['NoLang']]);
		}
	}
	return($lib);
}


/**  fonction envoi de mail text+HTML, pompée sur nexen et bricol�...
 * 
 * @param type $destinataire
 * @param type $sujet
 * @param type $messhtml
 * @param type $from
 * @param type $encod
 * @param type $addheader
 * @param type $file
 * @return type
 */
function mail_html($destinataire, $sujet , $messhtml,  $from, $encod="iso-8859-1",$addheader="",$file="") {
//die("$destinataire, $sujet , $messhtml,  $from, $encod,$addheader");
/// Grosse simplification, ça ne marchait plus sur le nouveau www avce tout ce qu'il y a dessous
/// par contre il n'y a plus d'envoi en texte simple, pas sur que ça marche avec les clients texte simple...
$sepligne = "\r\n";
$entete .= "MIME-Version: 1.0".$sepligne;
//$entete .= "Content-Type: text/".(stristr($messhtml,"<html") ? "html" : "plain")."; charset=\"$encod\"$sepligne";
$boundary = "-----=".md5(uniqid(rand()));
$entete .= "Content-Type: multipart/mixed; boundary=\"$boundary\"\r\n";
$entete .= "To:".$destinataire.$sepligne;
$entete .= "From:".$from.$sepligne;
$entete .= $addheader; // on peut y mettre des gaziers en Cc ou Cci... par ex.
// $entete .= "CC: sombodyelse@noplace.com$sepligne";
//$entete .= "Date: ".date("l j F Y, G:i")."$sepligne"; // sert certainement a rien et fout la merde
//$texte_html .= "Content-Type: text/html; charset=\"$encod\"$sepligne";
$msg .= "--$boundary$sepligne";

// Et pour chaque partie on en indique le type
$msg .= "Content-Type: text/".(stristr($messhtml,"<html") ? "html" : "plain")."; charset=\"$encod\"$sepligne";
// Et comment il sera cod�
$msg .= "Content-Transfer-Encoding:8bit$sepligne";
// Il est indispensable d'introduire une ligne vide entre l'ent�e et le texte
$msg .= $sepligne;
$msg .= $messhtml;
//---------------------------------
// 2nde partie du message
// Le fichier
//---------------------------------
// Tout d'abord lire le contenu du fichier
// le chenmin du fichier est relatif au script appelant cette fonction

if ($file != "") {
	if (file_exists($file)) { // si fichier est sp�ifi�et existe ....

	// 	$fp = fopen($file, "rb");   // b c'est pour les windowsiens
	// 	$attachment = fread($fp, filesize($file));
	// 	fclose($fp);

		$attachment = file_get_contents($file);

		// puis convertir le contenu du fichier en une cha�e de caract�e
		// certe totalement illisible mais sans caract�es exotiques
		// et avec des retours �la ligne tout les 76 caract�es
		// pour �re conforme au format RFC 2045
		$attachment = chunk_split(base64_encode($attachment));
		$filetype = filetype($file);
		$file = basename($file);
		// Ne pas oublier que chaque partie du message est s�ar�par une fronti�e
		$msg .= "\r\n--$boundary\r\n";
		// Et pour chaque partie on en indique le type $filetype
		$msg .= "Content-Type: $filetype; name=\"$file\"\r\n";
		// Et comment il sera cod�	
		$msg .= "Content-Transfer-Encoding: base64\r\n";
		// Petit plus pour les fichiers joints
		// Il est possible de demander �ce que le fichier
		// soit si possible affich�dans le corps du mail
	//	$msg .= "Content-Disposition: inline; filename=\"$file\"\r\n";
		$msg .= "Content-Disposition: attachment; filename=\"$file\"\r\n";
		// Il est indispensable d'introduire une ligne vide entre l'ent�e et le texte
		$msg .= "\r\n";
		// C'est ici que l'on ins�e le code du fichier lu
		$msg .= $attachment . "\r\n";
		$msg .= "\r\n\r\n";

		// voil� on indique la fin par une nouvelle fronti�e
		$msg .= "--$boundary--\r\n";
	} else { // le fichier attach�n'a pas ��trouv�	$msg.="Le fichier $file qui devait etre attach��ce ce message n\'a pas  ��trouv�;
		throw new Exception("fichier $file introuvable; impossible d'envoyer le mail");
	}
}
return mail($destinataire, $sujet, $msg, $entete);

/*

$limite = "_parties_".md5 (uniqid (rand()));

$entete .= "MIME-Version: 1.0$sepligne";
$entete .= 'Content-Type: multipart/alternative'."$sepligne";
$entete .= "From:$from$sepligne";
$entete .= $addheader; // on peut y mettre des gaziers en Cc ou Cci... par ex.
//$entete .= "Date: ".date("l j F Y, G:i")."$sepligne"; // sert certainement a rien et fout la merde
$entete .= " boundary=\"----=$limite\"$sepligne$sepligne";

//Le message en texte simple pour les navigateurs qui
//n'acceptent pas le HTML
$texte_simple = "This is a multi-part message in MIME format.$sepligne";
$texte_simple .= "Ceci est un message est au format MIME.$sepligne";
$texte_simple .= "------=$limite$sepligne";
$texte_simple .= "Content-Type: text/plain; charset=\"$encod\"$sepligne";
$texte_simple .= "Content-Transfer-Encoding: 8bit$sepligne$sepligne";
//$texte_simple .=  "Procurez-vous un client de messagerie qui sait afficher le HTML !!";
$texte_simple .=  strip_tags(eregi_replace("<br/>", "$sepligne", $messhtml)) ;
$texte_simple .= "$sepligne$sepligne";

//le message en html original
$texte_html = "------=$limite$sepligne";
$texte_html .= "Content-Transfer-Encoding: 8bit$sepligne$sepligne";
$texte_html .= $messhtml;
$texte_html .= "$sepligne$sepligne$sepligne------=$limite--$sepligne";

return mail($destinataire, $sujet, $texte_simple.$texte_html, $entete);*/
}



/** // envoi de mail avec piece jointe
 * 
 * @param type $destinataire
 * @param type $sujet
 * @param type $message
 * @param type $from
 * @param type $file
 * @param type $contentType
 * @return type
 * @throws Exception
 */
function mail_fj($destinataire,$sujet,$message,$from,$file,$contentType) {
//----------------------------------
// Construction de l'ent�e
//----------------------------------
// On choisi g��alement de construire une fronti�e g��� aleatoirement
// comme suit. (REM: je n'en connais pas la raison profonde)
$boundary = "-----=".md5(uniqid(rand()));

// Ici, on construit un ent�e contenant les informations
// minimales requises.
// Version du format MIME utilis�
$header = "MIME-Version: 1.0\r\n";
// Type de contenu. Ici plusieurs parties de type different "multipart/mixed"
// Avec un fronti�e d�inie par $boundary
$header .= "Content-Type: multipart/mixed; boundary=\"$boundary\"\r\n";
$header .= "\r\n";

//--------------------------------------------------
// Construction du message proprement dit
//--------------------------------------------------

// Pour le cas, o le logiciel de mail du destinataire
// n'est pas capable de lire le format MIME de cette version
// Il est de bon ton de l'en informer
// REM: Ce message n'appara� pas pour les logiciels sachant lire ce format
$msg = "Je vous informe que ceci est un message au format MIME 1.0 multipart/mixed.\r\n";

//---------------------------------
// 1�e partie du message
// Le texte
//---------------------------------
// Chaque partie du message est s�ar�par une fronti�e
$msg .= "--$boundary\r\n";

// Et pour chaque partie on en indique le type
$msg .= "Content-Type: text/plain; charset=\"utf-8\"\r\n";
// Et comment il sera cod�
$msg .= "Content-Transfer-Encoding:8bit\r\n";
// Il est indispensable d'introduire une ligne vide entre l'ent�e et le texte
$msg .= "\r\n";
// Enfin, on peut �rire le texte de la 1�e partie

$msg .= $message."\r\n";
$msg .= "\r\n";

//---------------------------------
// 2nde partie du message
// Le fichier
//---------------------------------
// Tout d'abord lire le contenu du fichier
// le chenmin du fichier est relatif au script appelant cette fonction

if ($file!="" && file_exists($file)) { // si fichier est sp�ifi�et existe ....

// 	$fp = fopen($file, "rb");   // b c'est pour les windowsiens
// 	$attachment = fread($fp, filesize($file));
// 	fclose($fp);
	
	$attachment=file_get_contents($file);
	
	// puis convertir le contenu du fichier en une cha�e de caract�e
	// certe totalement illisible mais sans caract�es exotiques
	// et avec des retours �la ligne tout les 76 caract�es
	// pour �re conforme au format RFC 2045
	$attachment = chunk_split(base64_encode($attachment));
	$file = basename($file);
	// Ne pas oublier que chaque partie du message est s�ar�par une fronti�e
	$msg .= "--$boundary\r\n";
	// Et pour chaque partie on en indique le type
	$msg .= "Content-Type: $contentType; name=\"$file\"\r\n";
	//$msg .= "Content-Type: text/html; name=\"$file\"\r\n";
	// Et comment il sera cod�	
	$msg .= "Content-Transfer-Encoding: base64\r\n";
	// Petit plus pour les fichiers joints
	// Il est possible de demander �ce que le fichier
	// soit si possible affich�dans le corps du mail
//	$msg .= "Content-Disposition: inline; filename=\"$file\"\r\n";
	$msg .= "Content-Disposition: attachment; filename=\"$file\"\r\n";
	// Il est indispensable d'introduire une ligne vide entre l'ent�e et le texte
	$msg .= "\r\n";
	// C'est ici que l'on ins�e le code du fichier lu
	$msg .= $attachment . "\r\n";
	$msg .= "\r\n\r\n";
	
	// voil� on indique la fin par une nouvelle fronti�e
	$msg .= "--$boundary--\r\n";
} 
else { // le fichier attach�n'a pas ��trouv�	$msg.="Le fichier $file qui devait etre attach��ce ce message n\'a pas  ��trouv�;
	throw new Exception("fichier $file introuvable; impossible d'envoyer le mail");
}

return mail($destinataire, $sujet, $msg,"Reply-to: $from\r\nFrom: $from\r\n".$header);
}

/**
 * 
 * Effectue une ouverture de session avec un Id De Session spécifique, ce qui permet d'ouvrir plusieurs sessions dans le même navigateur

 * @param type $diffSess true par défut, false pas d'ouverture spécifique, car dans certains env. ça déconne
 */
function session_start_wthspid($diffSess = true) {
//echo "avant:".$_REQUEST['SESSION_NAME']."\n";
	//echo "diffSess $diffSess";
	if ($diffSess) {
		if(version_compare(phpversion(),'4.3.0')>=0) {
			if (!preg_match('`SESS[0-9a-z]`i', $_REQUEST['SESSION_NAME'])) {
			//if(!ereg('^SESS[0-9a-z]$',$_REQUEST['SESSION_NAME'])) {
				//echo " new session ";
				$_REQUEST['SESSION_NAME']='SESS'.uniqid('');
			}
			output_reset_rewrite_vars(); // l'annule, car des fois il s'en met plein les unes à la suite des autres...
			output_add_rewrite_var('SESSION_NAME',$_REQUEST['SESSION_NAME']); // rajoute &SESSION_NAME=$_REQUEST['SESSION_NAME'] à ts les liens, url de post etc...
			if (session_name() != $_REQUEST['SESSION_NAME']) session_name($_REQUEST['SESSION_NAME']);
		} 
		//echo "apres:".$_REQUEST['SESSION_NAME']."\n";
		session_id($_REQUEST['SESSION_NAME']);
	}
	session_start();
	//print_r($_SESSION);
}
/*
*  Function ustr_replace for "unique str_replace"
*  Replace a string only once in a string
*
*  Params :
*         @search  : string
*         @replace : string
*         @subject : string
*         @cur     : int
*
*  Note : if $cur is empty only the first occurence of $search will be replace else the function will take the next occurence after $cur (int position)
*
*  return string
*/
function ustr_replace($search, $replace, $subject, $cur=0) {
	return (strpos($subject, $search,$cur)) ? substr_replace($subject, $replace,(int)strpos($subject,$search,$cur), strlen($search)) : $subject;
}

//ustr_rieplace
/*
*  Function ustr_ireplace for "unique str_ireplace"
*  Replace a string only once in a string
*
*  Params :
*         @search  : string
*         @replace : string
*         @subject : string
*         @cur     : int
*
*  Note : if $cur is empty only the first occurence of $search will be replace else the function will take the next occurence after $cur (int position)
*
*  return string
*/
function ustr_ireplace($search, $replace, $subject, $cur=0) {
	return (stripos($subject, $search,$cur)) ? substr_replace($subject, $replace,(int)stripos($subject,$search,$cur), strlen($search)) : $subject;
}//ustr_replace

/** retourne le chemin absolu du script courant commençant par http(s):// terminé par '/'
 * 
 * @return chemin
 */
function getScriptAbsPath() {
	if (defined('ServerNameProtoc')) return ServerNameProtoc.dirname($_SERVER['REQUEST_URI'])."/";
	$protocol = (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1)
        ||
        //proxy forwarding
        isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https'
    ) ? 'https://' : 'http://';
	return ($protocol. $_SERVER['HTTP_HOST']. dirname($_SERVER['REQUEST_URI'])."/");
}

/**
 * Encrypt a password and returns the hash based on the specified enc_type.
 * si on lui passe un MDP déjà encrypté ({YYY}xxxx) ne le réencrypte pas le retourne telquel
 * Inspiré de phpldapadmin
 *
 * @param string The password to hash in clear text.
 * @param string Standard LDAP encryption type which must be one of
 *        crypt, ext_des, md5crypt, blowfish, md5, sha, smd5, ssha, sha512,phppwd or clear.
 * @return string The hashed password.
 */
function encrypt_passwd($password_clear,$enc_type) {
	
	$encTypeExistingPwd = getPasswordEncType($password_clear);
	if ($encTypeExistingPwd != 'clear') return $password_clear;
	
	$enc_type = strtolower($enc_type);

	switch($enc_type) {
		case 'blowfish':
			if (! defined('CRYPT_BLOWFISH') || CRYPT_BLOWFISH == 0)
				throw new Exception('Your system crypt library does not support blowfish encryption.');

			# Hardcoded to second blowfish version and set number of rounds
			$new_value = sprintf('{CRYPT}%s',crypt($password_clear,'$2a$12$'.random_salt(13)));

			break;

		case 'crypt':
			if ($GLOBALS['no_random_crypt_salt'])
				$new_value = sprintf('{CRYPT}%s',crypt($password_clear,substr($password_clear,0,2)));
			else
				$new_value = sprintf('{CRYPT}%s',crypt($password_clear,random_salt(2)));

			break;

		case 'ext_des':
			# Extended des crypt. see OpenBSD crypt man page.
			if (! defined('CRYPT_EXT_DES') || CRYPT_EXT_DES == 0)
				throw new Exception('Your system crypt library does not support extended DES encryption.');

			$new_value = sprintf('{CRYPT}%s',crypt($password_clear,'_'.random_salt(8)));

			break;

		case 'k5key':
			$new_value = sprintf('{K5KEY}%s',$password_clear);

			system_message(array(
				'title'=>_('Unable to Encrypt Password'),
				'body'=>'phpLDAPadmin cannot encrypt K5KEY passwords',
				'type'=>'warn'));

			break;

		case 'md5':
			$new_value = sprintf('{MD5}%s',base64_encode(pack('H*',md5($password_clear))));
			break;

		case 'md5crypt':
			if (! defined('CRYPT_MD5') || CRYPT_MD5 == 0)
				throw new Exception('Your system crypt library does not support md5crypt encryption.');

			$new_value = sprintf('{CRYPT}%s',crypt($password_clear,'$1$'.random_salt(9)));

			break;

		case 'sha':
			# Use php 4.3.0+ sha1 function, if it is available.
			if (function_exists('sha1'))
				$new_value = sprintf('{SHA}%s',base64_encode(pack('H*',sha1($password_clear))));
			elseif (function_exists('mhash'))
				$new_value = sprintf('{SHA}%s',base64_encode(mhash(MHASH_SHA1,$password_clear)));
			else
				throw new Exception('Your PHP install does not have the mhash() function. Cannot do SHA hashes.');

			break;

		case 'ssha':
			if (function_exists('mhash') && function_exists('mhash_keygen_s2k')) {
				mt_srand((double)microtime()*1000000);
				$salt = mhash_keygen_s2k(MHASH_SHA1,$password_clear,substr(pack('h*',md5(mt_rand())),0,8),4);
				$new_value = sprintf('{SSHA}%s',base64_encode(mhash(MHASH_SHA1,$password_clear.$salt).$salt));

			} else {
				throw new Exception('Your PHP install does not have the mhash() or mhash_keygen_s2k() function. Cannot do S2K hashes.');
			}

			break;

		case 'smd5':
			if (function_exists('mhash') && function_exists('mhash_keygen_s2k')) {
				mt_srand((double)microtime()*1000000);
				$salt = mhash_keygen_s2k(MHASH_MD5,$password_clear,substr(pack('h*',md5(mt_rand())),0,8),4);
				$new_value = sprintf('{SMD5}%s',base64_encode(mhash(MHASH_MD5,$password_clear.$salt).$salt));

			} else {
				throw new Exception('Your PHP install does not have the mhash() or mhash_keygen_s2k() function. Cannot do S2K hashes.');
			}

			break;

		case 'sha512':
			if (function_exists('openssl_digest') && function_exists('base64_encode')) {
				$new_value = sprintf('{SHA512}%s', base64_encode(openssl_digest($password_clear, 'sha512', true)));

			} else {
				throw new Exception('Your PHP install doest not have the openssl_digest() or base64_encode() function. Cannot do SHA512 hashes. ');
			}
			break;
			
		case 'phppwd':
			$phpPwdEncType = defined('phpPwdEncType') ? phpPwdEncType : PASSWORD_DEFAULT;
			$new_value = sprintf('{PHPPWD}%s', password_hash($password_clear, $phpPwdEncType));
			break;
		
		case 'clear':
		default:
			$new_value = $password_clear;
	}

	return $new_value;
}

/**
 * Given a clear-text password and a hash, this function determines if the clear-text password
 * is the password that was used to generate the hash. This is handy to verify a user's password
 * when all that is given is the hash and a "guess".
 * @param String The hash.
 * @param String The password in clear text to test.
 * @return Boolean True if the clear password matches the hash, and false otherwise.
 */
function password_check($cryptedpassword,$plainpassword) {

	//$cypher = getPasswordEncType($cryptedpassword);
	
	if (preg_match('/{([^}]+)}(.*)/',$cryptedpassword,$matches)) {
		$cryptedpassword = $matches[2];
		$cypher = strtolower($matches[1]);

	} else {
		$cypher = 'clear';
	}
	
	switch($cypher) {
		# SSHA crypted passwords
		case 'ssha':
			# Check php mhash support before using it
			if (function_exists('mhash')) {
				$hash = base64_decode($cryptedpassword);

				# OpenLDAP uses a 4 byte salt, SunDS uses an 8 byte salt - both from char 20.
				$salt = substr($hash,20);
				$new_hash = base64_encode(mhash(MHASH_SHA1,$plainpassword.$salt).$salt);

				if (strcmp($cryptedpassword,$new_hash) == 0)
					return true;
				else
					return false;

			} else {
				throw new Exception('Your PHP install does not have the mhash() function. Cannot do SHA hashes.');
			}

			break;

		# Salted MD5
		case 'smd5':
			# Check php mhash support before using it
			if (function_exists('mhash')) {
				$hash = base64_decode($cryptedpassword);
				$salt = substr($hash,16);
				$new_hash = base64_encode(mhash(MHASH_MD5,$plainpassword.$salt).$salt);

				if (strcmp($cryptedpassword,$new_hash) == 0)
					return true;
				else
					return false;

			} else {
				throw new Exception('Your PHP install does not have the mhash() function. Cannot do SHA hashes.');
			}

			break;

		# SHA crypted passwords
		case 'sha':
			if (function_exists('sha1'))
				$new_value = base64_encode(pack('H*',sha1($plainpassword)));
			elseif (function_exists('mhash'))
				$new_value = base64_encode(mhash(MHASH_SHA1,$plainpassword));
			else
				throw new Exception('Your PHP install does not have the mhash() function. Cannot do SHA hashes.');

			
			log2table("cryptdbg", $new_value.'|'. $cryptedpassword);
			//if (strcasecmp(password_hash($plainpassword,'sha'),'{SHA}'.$cryptedpassword) == 0)
			if (strcasecmp($new_value, $cryptedpassword) == 0)
				return true;
			else
				return false;

			break;

		# MD5 crypted passwords
		case 'md5':
			//log2table("cryptdbg", $plainpassword.'|'.password_hash($plainpassword,'md5').'|'.$cryptedpassword);
			//if( strcasecmp(password_hash($plainpassword,'md5'),'{MD5}'.$cryptedpassword) == 0)
			//log2table("cryptdbg", base64_encode(pack('H*',md5($plainpassword))).'|'. $cryptedpassword);
			//if( strcasecmp(base64_encode(pack('H*',md5($plainpassword))), $cryptedpassword))
			if(base64_encode(pack('H*',md5($plainpassword))) == $cryptedpassword)	
				return true;
			else
				return false;

			break;

		# Crypt passwords
		case 'crypt':
			# Check if it's blowfish crypt
			if (preg_match('/^\\$2+/',$cryptedpassword)) {

				# Make sure that web server supports blowfish crypt
				if (! defined('CRYPT_BLOWFISH') || CRYPT_BLOWFISH == 0)
					throw new Exception('Your system crypt library does not support blowfish encryption.');

				list($version,$rounds,$salt_hash) = explode('$',$cryptedpassword);

				if (crypt($plainpassword,'$'.$version.'$'.$rounds.'$'.$salt_hash) == $cryptedpassword)
					return true;
				else
					return false;
			}

			# Check if it's an crypted md5
			elseif (strstr($cryptedpassword,'$1$')) {

				# Make sure that web server supports md5 crypt
				if (! defined('CRYPT_MD5') || CRYPT_MD5 == 0)
					throw new Exception('Your system crypt library does not support md5crypt encryption.');

				list($dummy,$type,$salt,$hash) = explode('$',$cryptedpassword);

				if (crypt($plainpassword,'$1$'.$salt) == $cryptedpassword)
					return true;
				else
					return false;
			}

			# Check if it's extended des crypt
			elseif (strstr($cryptedpassword,'_')) {

				# Make sure that web server supports ext_des
				if (! defined('CRYPT_EXT_DES') || CRYPT_EXT_DES == 0)
					throw new Exception('Your system crypt library does not support extended DES encryption.');

				if (crypt($plainpassword,$cryptedpassword) == $cryptedpassword)
					return true;
				else
					return false;
			}

			# Password is plain crypt
			else {

				if (crypt($plainpassword,$cryptedpassword) == $cryptedpassword)
					return true;
				else
					return false;
			}

			break;

		# SHA512 crypted passwords
		case 'sha512':
			if (function_exists('openssl_digest') && function_exists('base64_encode')) {
				$new_value = base64_encode(openssl_digest($plainpassword, 'sha512', true));
//				echo "$new_value $cryptedpassword";
//				die();
				if (strcasecmp($new_value,$cryptedpassword) == 0) 
				//if (strcasecmp(password_hash($plainpassword,'sha512'),'{SHA512}'.$cryptedpassword) == 0)
						//die("true");
					return true;
				else
					return false;

				break;
			} else {
				throw new Exception('Your PHP install doest not have the openssl_digest() or base64_encode() function. Cannot do SHA512 hashes. ');
			}

		# php Password ie bcrypt 
		case 'phppwd':
			return password_verify($plainpassword, $cryptedpassword);
			break;

		# No crypt is given assume plaintext passwords are used
		case 'clear':
		default:
			if ($plainpassword == $cryptedpassword)
				return true;
			else
				return false;
	}
}

/**
 * Used to generate a random salt for crypt-style passwords. Salt strings are used
 * to make pre-built hash cracking dictionaries difficult to use as the hash algorithm uses
 * not only the user's password but also a randomly generated string. The string is
 * stored as the first N characters of the hash for reference of hashing algorithms later.
 *
 * @param int The length of the salt string to generate.
 * @return string The generated salt string.
 */
function random_salt($length) {
	if (DEBUG_ENABLED && (($fargs=func_get_args())||$fargs='NOARGS'))
		debug_log('Entered (%%)',1,0,__FILE__,__LINE__,__METHOD__,$fargs);

	$possible = '0123456789'.
		'abcdefghijklmnopqrstuvwxyz'.
		'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.
		'./';
	$str = '';
	mt_srand((double)microtime() * 1000000);

	while (strlen($str) < $length)
		$str .= substr($possible,(rand()%strlen($possible)),1);

	return $str;
}

/** retourne le type d'encryption d'un password en fonction de la chaine qu'il y a devant genre {MD5}xxxxx
 * 
 * @param type $cryptedpassword
 * @return type
 */
function getPasswordEncType($cryptedpassword) {
	if (preg_match('/{([^}]+)}(.*)/',$cryptedpassword,$matches)) {
		$cryptedpassword = $matches[2];
		$cypher = strtolower($matches[1]);
	} else {
		$cypher = 'clear';
	}
	return $cypher;
}
