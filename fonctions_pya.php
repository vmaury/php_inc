<?php 
/**
   * Fichier fonctions utiles à l'environnement PYA
   * 
   * @author: Vincent MAURY <artec.vm@arnac.net>
   */
   
$GLOBALS['nbcarmxlist'] = 50; // nbre de caracteres max affichés par cellules dans les tableaux liste (au dela ils sont (proprement) tronqués
$GLOBALS['nbligpp_def'] = 15; // nbre de lignes affichees par page par defaut
$GLOBALS['nbrtxa'] = 5; // nbre de lignes des textarea
$GLOBALS['nbctxa'] = 40; // nbre de colonnes des textarea
$GLOBALS['maxrepld'] = 2000; // maximum de réponses à afficher dans les listes dérouléntes
$GLOBALS['carsepldef'] = "-"; // caractere par defaut separant les valeur dans les listes deroulantes
$GLOBALS['maxprof'] = 10; // prof max des hierarchies
$GLOBALS['couserinc'] = "_INC"; // code user non donné pr les maj auto
$GLOBALS['valNC'] = 'N.C.'; // label affiché quand valeur clé de LD inconnue
$GLOBALS['MaxFSizeDef'] = 5000000; // taille max des fichiers joints par defaut!!
// Nom de la table de description des autres
$GLOBALS['TBDname'] = "DESC_TABLES";
// nom du champ contenant les caractéristiques globales de la table
$GLOBALS['NmChDT'] = "TABLE0COMM";
// id contenu ds les tables virtuelles ie celles qui n'existent pas en base
// $GLOBALS['id_vtb'] = "vtb_";  ==> Définit dans db_abstract, dépend du type de bdd utilisé

define("sepNmTableNmChp","#"); // met pas le . comme ça serait normal, car si un point dans les noms de var HTML il est transformé.

/**
 * Retounrne une condition en fonction de REQUEST pya
 * @param unknown_type $nmBase
 * @param unknown_type $nmTable
 * @param unknown_type $NomChp
 */
function retCondPya($nmBase, $nmTable, $NomChp) {	
	$NomChpSql = $nmTable.".".$NomChp; // on adpopte la vraie syntaxe sql
	$NomChpHtml = $nmTable.sepNmTableNmChp.$NomChp; // il faut bricoler, car le "." dans les noms de var html est transformé en _, donc on y met un # à la place
	
	if (isset($_REQUEST["tf_".$NomChpHtml])) { // mode champ avec noms complets, le type filtre est tjrs present
		$typFilt = $_REQUEST["tf_".$NomChpHtml];
		$valReq =  $_REQUEST["rq_".$NomChpHtml];
		$isNeg = $_REQUEST["neg_".$NomChpHtml];
		$nmvar = $NomChpHtml;
	} else {
		$typFilt = $_REQUEST["tf_".$NomChp];
		$valReq =  $_REQUEST["rq_".$NomChp];
		$isNeg = $_REQUEST["neg_".$NomChp];
		$nmvar = $NomChp;
	}

	if (isset($typFilt) && $valReq != "") { // si ces var non nulles, il y a forcement une condition
		$pyaObj = createOrLoadPyaObj($nmBase, $nmTable, $NomChp);
		// verrue pour avoir une requete égalité au lieu de recherche en like %val% comme avant
		if ($typFilt == "LDM" && ($pyaObj->TypeAff != "LDLM" && $pyaObj->TypeAff != "POPLM")) $typFilt = "LDMEG";

		$cond = SetCond ($typFilt, $valReq, $isNeg, $NomChpSql, $pyaObj->TTC == "numeric");
		if ($cond != "") {
			$_SESSION['memFilt']['rq_'.$nmvar] = $valReq;
			$_SESSION['memFilt']['neg_'.$nmvar] = $isNeg;
			$cond = str_replace("%%","%",$cond); // virer bizareries
		}
	}
	return($cond); 
}
/** 
fonction qui construit une condition sql
@param  $type_filtre string type de filtre: EGAL,INPLIKE,LDM,LDMEG
@param  $valeur_filtre string (ou tableau à 2 row pr les dates et valeurs inf/sup) valeur de la condition récupérée
@param  $neg_filtre string~boolen en fait si <> "" indique la négation
@param  $nom_champ string nom du champ
@param  $field_is_num boolean true si champ numérique 
*/

function SetCond ($type_filtre, $valeur_filtre, $neg_filtre, $nom_champ, $field_is_num=false) {
	if ($valeur_filtre != NULL && $valeur_filtre != "%") {
		if (!is_array($valeur_filtre)) $valeur_filtre = $field_is_num ? (float)$valeur_filtre + 0 : db_escape_string($valeur_filtre);
		switch ($type_filtre) { // switch sur type de filtrage
			case "EGAL" : // egalite, special
			case "EGALHID" : // egalite, special
				$valeur_filtre = trim($valeur_filtre);
				$cond = $field_is_num ? "$nom_champ = $valeur_filtre" : "$nom_champ = '".$valeur_filtre."'";
			break;
		
			case "INPLIKE" : // boite d'entree
				$valeur_filtre = trim($valeur_filtre);
				if (substr($valeur_filtre,-1,1) != "%" && !$field_is_num) $valeur_filtre .= "%";
				$cond = $field_is_num ? "$nom_champ = $valeur_filtre" : "$nom_champ LIKE '".$valeur_filtre."'";
			break;
				
	/// TODO ne devrait plus servir; one ne devrait utiliser que LDMEG et LDM_SPL
			case "LDM" : // liste a choix multiples de valeurs ds ce cas la valeur est un tableau
						// la condition resultante est NomChp LIKE '%Val1%' or NomChp LIKE '%Val2%' etc ...
				if (is_array($valeur_filtre)) {  // teste $valeur_filtre est bien un tabelau
					foreach ($valeur_filtre as $valf) {
						if ($valf == "%" || $valf=="000") {
							$cond="";
							break; // pas de condition s'il y a %
						}	else
							$cond .= $field_is_num ? "$nom_champ = $valf OR " : "$nom_champ LIKE '%".db_escape_string($valf)."%' OR "; // on av vire les % puis les a remis
					}
					if ($cond!="") $cond="(".vdc($cond,4).")"; // vire le dernier OR et rajoute () !!
				} else { // si ValF pas tableau
					if ($valeur_filtre == "%" || $valeur_filtre == "000") {
						$cond = "";
					} else {
						if (!$field_is_num) $gi = "'";
						$op = strstr($valeur_filtre,"%,") ? "LIKE" : "="; // dans le cas des LD valeurs fixes multiple, ça peut merder... une cond  = "%,toto,%" ne ramène rien
						$cond = "($nom_champ $op $gi$valeur_filtre$gi)";
					}
				} // fin si test sur val filtre tableau
			break;
				
			case "LDMEG" : // liste a choix multiples de valeurs ds ce cas la valeur est un tableau
			// la condition resultante est un NomChp ='Val1' or NomChp ='Val2' etc ...
				if (is_array($valeur_filtre)) {  // teste Valf est un tabelau
					foreach ($valeur_filtre as $valf) {
						if ($valf == "%" || $valf === "000") {
							// ya un bug avec les enum qui contiennent '0'; arrive pas à le résoudre
							$cond = "" ; 
							break; // pas de condition s'il y a %
						} else
							$cond .= $field_is_num ? "$nom_champ = $valf OR " : "$nom_champ='".db_escape_string($valf)."' OR ";
					} // fin boucle sur valeurs de filtre
					if ($cond!="") $cond="(".vdc($cond,4).")"; // vire le dernier OR  et rajoute () !!          
				} else { // si ValF pas tableau
					if ($valeur_filtre == "%" || $valeur_filtre == "000") {
						$cond="";
					} else {
						if (!$field_is_num) $gi="'";
						$cond="($nom_champ = $gi$valeur_filtre$gi)";
					}
				}
			break;
		
			case "LDM_SPL":   // special pour vals fixes muliple et liaison multiple: solutionne le pb qui fait que typo met pas les , au debut et a la fin
				if (is_array($valeur_filtre)) {  // teste Valf est un tabelau
					foreach ($valeur_filtre as $valf) {
						if ($valf == "%" || $valf == "000") {
							$cond = "";
							break; // pas de condition s'il y a %
						} else 
							$cond.= genLikeSpl($nom_champ, $valf)." OR "; 
					}
					if ($cond!="") $cond="(".vdc($cond,4).")"; // vire le dernier OR et rajoute () !!
				} else $cond=""; // si ValF pas tableau	
			break;
			
			case "DEG" : // date =
				if ($valeur_filtre == "%" || $valeur_filtre ==" ") break; // pas de condition
				$cond = "$nom_champ = '".DateA($valeur_filtre)."'";
			break;
				
			case "DANT" : // date antieure a...
			case "DPOST" : // date posterieure a... 
				if ($valeur_filtre == "%" || $valeur_filtre ==" ") break; // pas de condition
				$oprq = ($type_filtre == "DANT" ? "<=" : ">="); // calcul de l'operateur
				if ($field_is_num) { // alors c un tstamp
					$cond = "$nom_champ $oprq ".DateF2tstamp($valeur_filtre)."";
				} else
					$cond = "$nom_champ $oprq '".DateA($valeur_filtre)."'";
				break;
		
			case "DATAP" : // date inf et sup
				if ($valeur_filtre[0] != "%" && $valeur_filtre[0] != "") {
					if ($field_is_num) { // alors c un tstamp
						$cond = "$nom_champ >= ".DateF2tstamp($valeur_filtre[0]);
					} else // aaaa-mm-jj
						$cond = "$nom_champ >= '".DateA($valeur_filtre[0])."'";
				}
				// l'autre valeur
				if ($valeur_filtre[1] != "%" && $valeur_filtre[1] != "") {
				$cond = ($cond == "" ? "" : $cond." AND ");
				if ($field_is_num) { // alors c un tstamp
						$cond .= "$nom_champ <= ".DateF2tstamp($valeur_filtre[1]);
					} else 
						$cond .= "$nom_champ <= '".DateA($valeur_filtre[1])."'";
				}
			break;
				
			case "VINF" : // val inf a
			case "VSUP" : // val sup a
			if ($valeur_filtre == "%" || $valeur_filtre == "") break; // pas de condition
				$oprq = ($type_filtre == "VINF" ? "<=" : ">="); // calcul de l'operateur
				if ($field_is_num) { //
					$cond = "$nom_champ $oprq $valeur_filtre";
				} else // marche aussi avec alphanumeriques
					$cond = "$nom_champ $oprq '".$valeur_filtre."'";
			break;
		
			case "VIS" : // va entre bornes
				if ($valeur_filtre[0] != "%" && $valeur_filtre[0] != "") {
					if ($field_is_num) { // alors c un tstamp
						$cond = "$nom_champ >= ".$valeur_filtre[0];
					} else // marche aussi avec alphanum
						$cond = "$nom_champ >= '".$valeur_filtre[0]."'";
				}
				// L'autre valeur
				if ($valeur_filtre[1] != "%" && $valeur_filtre[1] != "") {
					$cond = $cond=="" ? "" : $cond." AND ";
					if ($field_is_num) { //
						$cond .= "$nom_champ <= ".$valeur_filtre[1];
					} else 
						$cond .= "$nom_champ <= '".$valeur_filtre[1]."'";
				}
			break;
		
			case "NOTNUL" : // inf et sup
				$cond = "(".$nom_champ ." IS NOT NULL AND ".($field_is_num ? "$nom_champ!=0" : "$nom_champ!=''"). ") ";
			break;
			
			default :
				$cond="";
			break;
		} // fin switch
	} // fin $valeur_filtre a une valeur coh�ente
	else $cond="";
	
	
	if ($cond!="" && $neg_filtre!="") $cond="NOT(".$cond.")"; // negationne �entuellement
	return($cond);
} // fin fonction SetCond

/**
 * Genere le sql pour chercher dune valeur dans des champs multivalués ,v1,v2,v3,
 * @param unknown_type $nom_champ
 * @param unknown_type $valf
 */
function genLikeSpl($nom_champ, $valf) {
	return (" ($nom_champ LIKE '%,".db_escape_string($valf).",%' OR $nom_champ LIKE '".db_escape_string($valf).",%' OR $nom_champ LIKE '%,".db_escape_string($valf)."' OR $nom_champ='".db_escape_string($valf)."') ");
}

/** 
// fonction qui renvoie un tableau de chaines contenant des couples Libellé.":|".valeurs, si les valeurs sont significatives
@param  $req string la requete sql
@param  $dbname string base de données
@param  $DirEcho boolen si echo direct
*/
function RTbVChPO($req, $dbname="", $DirEcho=false) {
	$tbPO = InitPOReq($req,$dbname);
	foreach ($tbPO as $PO) {
		$PO->TypEdit = "C";
		$PO->DirEcho = $DirEcho;
		if ($PO->ValChp !="" && $PO->ValChp !="NULL") $tbValRet[$PO->NmChamp] = $PO->Libelle.":|".$PO->EchoEditAll(false);
	}
	return($tbValRet);
}
/** 
// fonction renvoyant un tableau d'objets PYA (sauf 2 valeurs db_num_rows et db_resreq) initialises en fonction d'une simple requete SQL
// les objets sont initialises a partir des noms de champs, de tables et de base du resultat
@param  $req string la requete sql
@param  $db_name string la base de donnée
@param  $othparams array autres parametres
// $hashwnmtb : false, l'indice du tableau de hascyh est le NomChamp, true c'est NomTable|NomChamp */

function getTbPOReq ($req, $db_name="", $othparams = array( "DirEcho" => false, /// echo direct
															"TypEdit" => "C", /// type édition : C,N,1,2 / Consult, nouveau, modif, copie
															"limit" => 1, /// limit sql
															"co_user" => "", /// utilisateur
															"hashwnmtb" => false, /// renvoyer les clés du tableau d'objets (noms de champs) sous la forme NomTable#NomChamp
															"NmTable" => "", ///  nom de la table par défaut (peut être pratique avec Oracle qui ne sait pas les ramener)
															"ForceNewPOOD" => false, /// forceNew : si true va chercher l'objet en base systématiquement sans regarder le cache disque  
															 )) {
	if ($db_name == "") $db_name = $GLOBALS['DBName'];
	$limit = $othparams['limit'];
	$TypEdit = $othparams['TypEdit'] != "" ? $othparams['TypEdit'] : "C";
	
	if ($othparams['limit'] != "no") $req = addwherefORlimit($req,$othparams['limit']); // ajoute proprement la clause limit, meme avec Oracle (belle merde)
	$resreq = db_query($req);
	if ($othparams['limit'] == 1) {
		$tbValChp = db_fetch_array($resreq); // tableau des valeurs de l'enregistrement
	//	if (db_num_rows($resreq)== 0 && !($_SESSION['db_type'] == "oracle")) return (false); // le oci_num_rows ne fonctionne pas avec Oracle !!
	}  
	$CIL['db_num_rows'] = db_num_rows($resreq); // approximatif sous Oracle !!
	$CIL['db_resreq'] = $resreq;
	
	//  print_r($tbValChp);
	for ($i=0; $i<db_num_fields($resreq); $i++) {
		$NmChamp = db_field_name($resreq,$i);
		//echo "Chp traité : $NmChamp ";
		// la fonction db_field_table est tres aproximative avec Oracle et Pgsql
		// heureusement on peut préciser le nom de la table en faisant select toto as latable#toto dans la req cust
		if (strstr($NmChamp,sepNmTableNmChp)) { // la table est précisée dans le AS du champ de la req avec un #
			$tb = explode(sepNmTableNmChp,$NmChamp);
			$NmChp4hash = $NmChamp;
			$NmTable = $tb[0];
			$NmChamp = $tb[1];
		} else {
			$NmTable = $othparams['NmTable'] ? $othparams['NmTable'] : db_field_table($resreq,$i); // par défaut on prend le nom de la table si elle est donnée
			$NmChp4hash = $othparams['hashwnmtb'] ? $NmTable.sepNmTableNmChp.$NmChamp : $NmChamp;
		}
		// requetes custom : initialise pas le PO si mot clé ou nom de champ est un entier
		//echo  $CIL[$NmChp4hash]->NmChamp.":".preg_match("/^[0-9]+$/",$CIL[$NmChp4hash]->NmChamp)."<br/>";
		/// TODO TODO faut améliorer ce test
		if (!(preg_match("/sum\(|count\(|min\(|max\(|avg\(/i",$NmChamp) || preg_match("/^[0-9]+$/",$NmChamp))) {
			$CIL[$NmChp4hash] = createOrLoadPyaObj($db_name, $NmTable, $NmChamp, $othparams['ForceNewPOOD'], $othparams['DirEcho']);
			if ($CIL[$NmChp4hash]->ErrInit) $CIL[$NmChp4hash]->Libelle = '<i>'.$NmChp4hash.'</i>'; // si champ pas trouvé (table non définie dans DESC_TABLES) on garde son nom pr son libellé, en italique
		} else {
			$CIL[$NmChp4hash] = new PYAobj(); // nouvel objet
			$CIL[$NmChp4hash]->NmBase = $db_name;
			$CIL[$NmChp4hash]->NmTable = $NmTable;
			$CIL[$NmChp4hash]->NmChamp = $NmChamp;
			$CIL[$NmChp4hash]->Libelle = $NmChp4hash;
		}
		$CIL[$NmChp4hash]->TypEdit = $TypEdit;
		$CIL[$NmChp4hash]->DirEcho = $othparams['DirEcho'];
		
		if ($TypEdit != "N" && $othparams['limit']==1) { // si pas nouveau
			$CIL[$NmChp4hash]->ValChp = $tbValChp[$NmChamp];
			//echo $NmChamp."->".$tbValChp[$NmChamp];
		}
		if ($TypEdit != "C") $CIL[$NmChp4hash]->InitAvMaj($othparams['co_user']); // traitement avant mise à jour, mais pas en consultation
		$strdbgIPOR.=$NmChp4hash.", ";
		} // fin boucle sur les champs du resultat
	if ($GLOBALS['debug']) echo("Champs traites par la fct InitPOReq :".$strdbgIPOR."<br/>\n");
	return($CIL);														 
}

/**
 * set les valeurs de tous les objets d'un tableau
 * 
 * @param array $tbPo tableau d'objet 
 * @param array $rw  valeur (tableu asso rep. de la req.)
 * @param str $typedit C,M,N,1...
 * @param str $couser id du user à jour
 * 
 */
function setValTbPO(&$tbPo, $rw, $typedit = 'M', $couser = '') {
	foreach ($tbPo as $k=>$PO) { if (is_object($PO) && $k != 'db_resreq') {
		$tbPo[$k]->ValChp = $rw[$k];
		if ($typedit != 'C') $tbPo[$k]->InitAvMaj($couser);
	}}
}
/**
 *  conservée uniquement pour compatibilité
 */
function InitPOReq($req, $db_name="", $DirEcho=true, $TypEdit="", $limit=1, $co_user="", $othparams = array("hashwnmtb"=>false)) {
	return ( getTbPOReq ($req, $db_name, array( "DirEcho" => $DirEcho, /// echo direct
															"TypEdit" => $TypEdit, /// type édition : C,N,1,2 / Consult, nouveau, modif, copie
															"limit" => $limit, /// limit sql
															"co_user" => $co_user, /// utilisateur
															"hashwnmtb" => $othparams['hashwnmtb'], /// renvoyer les clés du tableau d'objets (noms de champs) sous la forme NomTable#NomChamp
															 )));
}

/**
 * Initialise un tableau d'objet PYA 
 * @param type $table nom de la table (qui peut être virtuelle...)
 * @param type $Base nom de la bdd
 * @param type $DirEcho false ou true (true par defaut)
 * @param type $TypEdit C,M, 2
 * @param type $co_user id du user courant
 * @return type tableau objets PYA
 */
 
function InitPOTable($table, $Base="", $DirEcho=true, $TypEdit="", $co_user="") {
  	if ($Base=="") $Base = $GLOBALS['DBName'];
	$reqt = db_qr_comprass("select NM_CHAMP FROM DESC_TABLES where NM_TABLE='$table' AND NM_CHAMP!='TABLE0COMM' ORDER BY ORDAFF");
	if (!$reqt) { 
		return(false);
	} else {
		foreach ($reqt as $chp) {
//			$CIL[$chp['NM_CHAMP']]=new PYAobj(); // nouvel objet
//			$CIL[$chp['NM_CHAMP']]->NmBase = $Base;
//			$CIL[$chp['NM_CHAMP']]->NmTable = $table;
//			$CIL[$chp['NM_CHAMP']]->NmChamp = $chp['NM_CHAMP'];
			$CIL[$chp['NM_CHAMP']] = createOrLoadPyaObj($Base, $table, $chp['NM_CHAMP']);
			$CIL[$chp['NM_CHAMP']]->TypEdit=$TypEdit;
			$CIL[$chp['NM_CHAMP']]->InitPO();
			if ($DirEcho!=true) $CIL[$chp['NM_CHAMP']]->DirEcho=false;
			if ($co_user!="" && $TypEdit!="C") $CIL[$chp['NM_CHAMP']]->InitAvMaj($co_user);
		}
		return($CIL);
	}
}

/** fonction qui met à jour l'enregistrement d'une table; inclut la MAJ des fichiers
 * @param string $DB nom bdd
 * @param string $table nom table
 * @param string $typedit typedit (N=new, M=modif, -1=delete, 2=copie)
 * @param array $tbKeys tableau des clés
 * @param string $req_vprec si défini : indique préfixe des champs ; typiquement si defini on teste on teste si $_REQUEST['vprec_NomChamp'] !=  $_REQUEST['NomChamp'] et on ne met à jour que dans ce cas là
 * @param string $lchp2maj si défini : liste des champs à mettre à jour; attention, il faut y mettre obligatoirement la clé 
 */
function PYATableMAJ($DB, $table, $typedit, $tbKeys=array(), $req_vprec="", $lchp2maj = "") {
	 if($typedit == 1) $typedit = "M";
	// construction du set, necessite uniquement le nom du champ ..
	if ($lchp2maj) $lchp2maj = " AND NM_CHAMP IN ('".str_replace(",", "','", $lchp2maj)."') ";
	$rq1 = db_query("SELECT * from DESC_TABLES where NM_TABLE='$table' AND NM_CHAMP!='TABLE0COMM' $lchp2maj AND (TYPEAFF!='HID' OR ( TT_PDTMAJ!='' AND TT_PDTMAJ!= NULL)) ORDER BY ORDAFF, LIBELLE");

	$key = substr(implode("-",$tbKeys)."-", 0, 50);
	$tbset = array();
	$tbWhK = array();
	while ($res1 = db_fetch_array($rq1)) {
		$NOMC = $res1['NM_CHAMP']; // nom variable=nom du champ
//		$PYAoMAJ = new PYAobj(); // recréée l'objet pour RAZ ses propriétés !!!
//		$PYAoMAJ->NmBase=$DB;
//		$PYAoMAJ->NmTable=$table;
//		$PYAoMAJ->NmChamp = $NOMC;
//		$PYAoMAJ->InitPO($_REQUEST[$NOMC],$res1); // init l'objet, sa valeur, lui passe le tableau d'infos du champ pr éviter une requete suppl.
		$PYAoMAJ = createOrLoadPyaObj($DB, $table, $NOMC);
		$PYAoMAJ->TypEdit = $typedit;
		if (defined('MaxFileSize') && MaxFileSize>0) $PYAoMAJ->MaxFSize=MaxFileSize;
		$PYAoMAJ->ValChp = $_REQUEST[$NOMC];
		if (array_key_exists($NOMC,$tbKeys)) {
			if ($tbKeys[$NOMC] != "") $PYAoMAJ->ValChp = $tbKeys[$NOMC]; // si val de clé définie elle est prioritaire
			$tbWhK = array_merge($tbWhK,$PYAoMAJ->RetSet($key."_",true));
		} 
		$keyf = $key.substr($NOMC, -1); // on fait ça car dans DeProj il y a plusieurs champs fichier par action
		$PYAoMAJ->ValChp = $_REQUEST[$NOMC]; // sinon en new ca bugue
		if ($PYAoMAJ->TypeAff=="HR_MN") {
			$PYAoMAJ->ValChp=  $_REQUEST[$NOMC."_mn"] +  $_REQUEST[$NOMC."_hr"]*100;
		} elseif ($PYAoMAJ->TypeAff == "FICFOT") {
			if ($_FILES[$NOMC]['name'] !="" && $_FILES[$NOMC]['error'] != "0") {
				$error = $_FILES[$NOMC]['error'];
				$err_lbl = "Erreur sur le champ $NOMC de type http : $error";
			}
			$PYAoMAJ->ValChp = $_FILES[$NOMC]['tmp_name']!="" ? $_FILES[$NOMC]['tmp_name'] : $PYAoMAJ->ValChp;
			$PYAoMAJ->Fok = $_REQUEST["Fok".$NOMC];
			$PYAoMAJ->Fname = $_FILES[$NOMC]['name'];
			$PYAoMAJ->Fsize = $_FILES[$NOMC]['size'];
			$PYAoMAJ->OFN = $_REQUEST["Old".$NOMC];
			// recup infos pour les pj dans le mail     
			$size = ($PYAoMAJ->Fsize >0 ? " (".round($PYAoMAJ->Fsize / 1000)."Ko) " : "");
			$chemfich ="http://".$_SERVER["SERVER_NAME"].str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']).$PYAoMAJ->Valeurs;
			$fich =($PYAoMAJ->Fname != "" ? $keyf."_".$PYAoMAJ->Fname : $PYAoMAJ->ValChp);
		}
		if (!(stristr($PYAoMAJ->FieldExtra, 'auto_increment') && ($typedit == "N" || $typedit == 2))) { // en cas de création, on ne spécifie pas les auto inc sinon ça plante ds les nouvelles versions de Mysql
			if (!$req_vprec || $_REQUEST[$req_vprec.$NOMC] != $PYAoMAJ->ValChp) { // maj que si req_vprec non définit, ou uniquement si valeur différente
					$tbset = array_merge($tbset,$PYAoMAJ->RetSet($keyf."_",true)); // key sert a la gestion des fichiers lies; la copie des fichiers est faite là-dedans
			}
		}
		if ($PYAoMAJ->error) {
			$error = true;
			$err_lbl .= "Erreur sur le champ $NOMC genree par pya :".$PYAoMAJ->error."\n";
			$PYAoMAJ->error = "";
		}
		//print_r($PYAoMAJ);
	} // fin boucle sur les champs
	if ($error) throw new Exception($err_lbl);

	if (count($tbWhK)>0) {
		foreach ($tbWhK as $chp=>$val) $lchp[]=$chp."=$val";
		$where= " where ".implode(" AND ",$lchp);
	}
//	print_r($_REQUEST);
//	print_r($tbset);
//	echo $where.$typedit.$lchp2maj;
	// GROS BUG  $where=" where ".$key.($where_sup=="" ? "" : " and $where_sup");
	//echovar("_REQUEST['typeditrcr']");
	if ($typedit == "M" || $typedit == "1") { // UPDATE
		$strqaj = "UPDATE $CSpIC$table$CSpIC SET ".tbset2set($tbset)." $where";
	} elseif ($typedit == -1) { // SUPPRESSION
		$strqaj = "DELETE FROM $CSpIC$table$CSpIC $where";
	} elseif ($typedit == "N" || $typedit == 2) { //INSERTION (idem duplication)
		$strqaj = "INSERT INTO $CSpIC$table$CSpIC ".tbset2insert($tbset)." ON DUPLICATE KEY UPDATE ".tbset2set($tbset);
	}
	//mail_html(devmail4debug, "PYA", "sql = $strqaj".var_export($_REQUEST, true), from, 'utf-8');

	db_query($strqaj);
//	echo "<br/>\nrequete sql: $strqaj\n";
	if ($typedit == "N" || $typedit == 2) {
		if ($GLOBALS['mysqlDrv'] == "mysqli") {
			return(mysqli_insert_id($GLOBALS['db_lnkid']));
		} else return(mysql_insert_id());
	}
}
/**
 * fonction qui convertit le contenu d'une valeur geree par LDLM dans pya ( ,toto,titi,
 * en valeur mettable dans un IN sql
 */
function cvldlm2in ($strap, $addsl=false) {
	if ($addsl) $adsl="'";
	if (strstr($strap,",")) {
		$tbuids=explode(",",$strap);
		foreach($tbuids as $uid) {
			if ($uid!="") $tbuids2[]=$adsl.$uid.$adsl;
		}
		$uidin=implode(",",$tbuids2);
	} else $uidin=$adsl.$strap.$adsl;
	return($uidin);
}

/** fonction de traitement des champs liés
@param $valbrut: chaine brute de liaison, 
@param $reqsup: req supplementaire (optionnelle)
@param $valc: valeur cherchee (optionnelle)
$valb0 la chaine de liaison a la forme suivante
	Nom_base,nom_serveur,nom_user,passwd;(0)table,(1) champ lié(clé)**, (2) et SUIVANTS champs affichés [[ condition where suppl mise dans PYA

** maintenant, gestion des clés multiples : dans le champ lié, peut mettre nomchamp1:nomchamp2:

retourne un tableau associatif si valc="", une valeur sinon
$reqsup est utilise par DRH2 et GDP1
*/
function ttChpLink2($valbrut, $valc = "", $othparams = array("reqsup" => "",
															"carsepl" => "",
															"maxrepld" => "",
															)) {
	$reqsup = $othparams['reqsup'];
	$othparams['carsepldef'] = $othparams['carsepldef'] ? $othparams['carsepldef'] : $GLOBALS['carsepldef'];
	$othparams['maxrepld'] = $othparams['maxrepld'] ? $othparams['maxrepld'] : $GLOBALS['maxrepld'];
	if (strstr($valbrut,"\n")) { // il est possible qu'on lui passe une chaine avec la nouvelle syntaxe. 
		$tbv = parseVal($valbrut);
		$valbrut = $tbv['Valeurs'];
	}
	if ($valbrut == "") throw new Exception ("Fct ttChpLink2, argument principal vide !!");
	$valbrut = explode(';',$valbrut);
	/// en cas de modif de la syntaxe, checker aussi PYAObj
	/// methode echoFilt, case LDLLV qui se sert de la chaine valb0 pour une requete imbriquée  
	if (count($valbrut) > 1) { // connection a une base differente TODO ca faudrait checker un de ces 4...
		$lntable = $valbrut[1];
		$defdb = explode(',',$valbrut[0]);
		$newbase = true;
		// si user et/ou hote d'acces a la Bdd est different, on etablit une nvlle connexion
		// on fait une nouvelle connection systematiquement pourt etre compatioble avec pg_sql
		//if (($defdb[1]!="" && $defdb[1]!=$DBHost)||($defdb[2]!="" && $defdb[2]!=$DBUser)) {
		$db_lnk_id_svg = $GLOBALS['db_lnkid']; // sauvegarde de lien 
		$lnc = db_connect($defdb[1],$defdb[2],$defdb[3],$defdb[0]);
		$newconnect = true;
		//}
	} else { // si pas de conexion spécifique
		$lntable = $valbrut[0];
	}
	// gestion condition AND depuis PYA
	//die($lntable);
	$valb2 = explode("[[", $lntable);
	if (count($valb2) > 1) {
		$lntable = $valb2[0];
		if ($valc == "" || (is_array($valc) && count($valc) == 0) || strstr($valc,'__str2f__')) { // on ne traite les req suppl que quand on a pas une valeur à chercher sf en popup
			if ($othparams['reqsup'] != "") {
				$reqsup= "(".$othparams['reqsup']." AND ".$valb2[1].")";
			} else $reqsup = $valb2[1];
		}
	}

	// 0: table, 1: champ lié(s)(clé(s)); 2: ET SUIVANTS champs affiches
	$defl = explode(',', $lntable);
	analChpOrder($defl[1],$orderby); // args passés par ref // on peut maintenant trier sur la clé aussi
	$nbca = 0; // on regarde les suivants pour construire la requete
	$rcaf = "";
	$tbmultk = explode(":",$defl[1]); // on gère les clé multiples: le nom du champ clé est dans ce cas clé1:clé2:clé3
	// on boucle sur les champs à afficher (commencent à l'indice 2, cf ci-dessus)
	while ($defl[$nbca+2] != "") {
		$nmchp = $defl[$nbca+2];
		$c2aff = true; // champ a afficher effectivement par defaut 
		$tbcs[$nbca+1] = $othparams['carsepldef']; // car de sep des champs par defaut
		if (strstr($nmchp, "!")) { // caractere separateur defini, qui va alors remplacer 
			$nmchp = explode("!", $nmchp);
			$tbcs[$nbca+1] = $nmchp[0]; // separateur avant le "!" 
			$nmchp = trim($nmchp[1]);
		} else $nmchp = trim($nmchp);
		/* si le chp a afficher champ comporte un & au debut, il faut aller cherche les valeurs dans une
		table; les parametres sont  indiques dans les caracteristiques d'édition de CE champ dans la table de définition*/
       	if (strstr($nmchp,"&")) { // si chainage
			$nmchp = substr ($nmchp,1); // enleve le &
			analChpOrder($nmchp,$orderby); // args passés par ref
     	 	$resvc = db_qr_rass2 ("select VALEURS from ".$GLOBALS['TBDname']." where NM_CHAMP='$nmchp' AND NM_TABLE='$defl[0]'");
     	 	if (!$resvc) throw new Exception("Chainage ds ttchplink2 : pas d'nregistrement de VALEURS pour NM_CHAMP='$nmchp' AND NM_TABLE='$defl[0]'");
     	 	$valbchain[$nbca+1] = $resvc['VALEURS']; // la nouvelle syntaxe est prise en compte au début de ttchplink
    	}
       	if (strstr($nmchp,"@@")) { // si ce champ indique un champ de structure hierarchique avec la clé de type pid= parent id
			$cppid = substr ($nmchp,2); // enleve le @@
			$c2aff = false; // le champ n'est pas à afficher
	 	} else 
			analChpOrder($nmchp,$orderby); // args passés par ref
		if ($c2aff) { // si champ à afficher
			$rcaf = $rcaf .",". $nmchp;
			$tbc2a[] = $nmchp; // tableau des champs où chercher
		}
		$nbca++;
	} // fin boucle
	if ($cppid) $nbca = $nbca-1; // un champ de moins à afficher dans le cas où il y a un arbre

	if (trim($reqsup) == "") $reqsup = "1=1"; // défaut comme ça 
	if  ($valc != "") { // une valeur est recherchée : soit on cherche 1 et 1 seule valeur, ou plusieurs si $valc est un tableau, ou une chaine de caractères
		if (is_array($valc)) { // on a un tableau, donc plusieurs enregistrements recherchés
			if (count($tbmultk) > 1) { // si FK clé multiple : dans ce cas la ou les valeurs sont de la forme val1:val2 pour chaque chmp FK
				foreach ($valc as $val1c) {
					$whsl .= "(".kv2sql($tbmultk,$val1c).") OR ";
				} 
			} else { // pas clé multiple
				foreach($valc as $uval) {
					$whsl.= " $defl[1]='$uval' OR ";
				}
			} // fin si pas clémltiple
			$whsl = vdc($whsl,3);
		} elseif (strstr($valc,'__str2f__')) { // on cherche une chaine parmi les champs à afficher (utilisé par la popup de sel générique)
			$val2s = str_replace('__str2f__','',$valc);
			foreach($tbc2a as $chp) {
				$whsl.=" $chp LIKE '%$val2s%' OR ";
			}
			$whsl = vdc($whsl,3);
		} else { // on cherche une et une seule valeur
			if (count($tbmultk) > 1) { // clé multiple
				$whsl.= ' ('.kv2sql($tbmultk,$valc).')';
			} else $whsl=" $defl[1]='$valc'"; // normal
		}
	}
	if (trim($whsl) == "") $whsl = "1=1";  
	$whsl = " where ($whsl) and ($reqsup)";
	//die($whsl);
	
	if ($cppid && $valc == "") { // on a une structure hierarchique et pas de valeur à chercher
		// on cherche les parents initiaux, ie ceux dont le pid est null ou egal a la cle du meme enregistrement
		if ($reqsup != "") $whreqsup = " AND $reqsup ";
		$rql = db_query("SELECT $defl[1], $cppid $rcaf from $defl[0] WHERE ($cppid IS NULL OR $cppid=$defl[1] OR $cppid=0) $whreqsup $orderby");
		while ($rw = db_fetch_row($rql)) {
			if($rw[0] != "") { // si cle valide 
				$resaf = tradLib($rw[2]);
				for ($k = 2; $k <= $nbca; $k++) {
					if ($valbchain[$k] != "") { // Chainage sur ce champ
						$resaf .= $tbcs[$k].ttChpLink2($valbchain[$k], $rw[$k + 1]);
					} else $resaf .= $tbcs[$k].tradLib($rw[$k +1]);
					$resaf .= strstr($tbcs[$k], '(') ? ')' : ''; // si paranthèse ouvrante on la ferme
				} // boucle sur chps eventuels en plus
				$tabCorlb[$rw[0]] = $resaf;
				rettarbo($tabCorlb, $rw[0], $defl, $cppid, $rcaf, $orderby, $nbca, $tbcs, 0, $whreqsup); 
				//print_r($tabCorlb);				
			} // fin si cle valide
		} // fin boucle reponses
		if (!is_array($tabCorlb)) { // pas de reponses
			$tabCorlb['err'] = "Error ! impossible construire l'arbre ";
		}
	} else { // pas hierarchique cad normal
		$defl[1] = str_replace(":", ",", $defl[1]); // au cas ou clé multiple
		if (strstr($valc,'__str2f__') && trim(str_replace(",", "", $defl[1])) == trim(str_replace(",", "", $rcaf))) {
			// verrue pifométrique quand recherche dans les valeurs d'un champ lui-même, non lié, pour les grouper
			$sql = addwherefORlimit("SELECT distinct($defl[1]) $rcaf from $defl[0] $whsl $orderby", $othparams['maxrepld']);
		} else $sql = addwherefORlimit("SELECT $defl[1] $rcaf from $defl[0] $whsl $orderby", $othparams['maxrepld']);
		//echo "<!--debug2srql=$sql, |{$defl[1]}|-|$rcaf|<br/>-->";
		$rql = db_query($sql);
		// constitution du tableau associatif a2 dim de corresp code ->lib
		//echo "<!--debug2 rql=SELECT $defl[1] $rcaf from $defl[0] $whsl $orderby <br/>-->";
		$tabCorlb = array();
		$nbkm = count($tbmultk); // nbre clés multiples
		while ($resl = db_fetch_row($rql)) {
			$cle = $resaf = "";
			if ($nbkm > 1) { // clé multiple
				for ($k=0; $k < $nbkm; $k++) $cle .= $resl[$k].":";
				$cle = vdc($cle,1);
			} else { // clé simple
				$nbkm = 1;
				$cle = $resl[0];
			}
			for ($k = $nbkm; $k < ($nbca + $nbkm); $k++) { // récupère les valeurs des champs
				$cs = $k != $nbkm ? $tbcs[$k] : ""; // pas de car sur le premier champ
				if ($valbchain[$k] != "") {
					$resaf .= $cs.ttChpLink2($valbchain[$k], $resl[$k]);
				} else $resaf .= $cs.tradLib($resl[$k]);
				$resaf .= (strstr($tbcs[$k], '(') && !strstr($tbcs[$k], '()')) ? ')' : ''; // si paranthèse ouvrante et pas double on la ferme
			}
			traiteParent($resaf); //pour traitement des fins de libellés entre () qui ont tjrs fait chier
			$tabCorlb[$cle] = stripslashes($resaf); // tableau associatif de correspondance code -> libelle
			//echo "<!--debug2 cle: $cle; val: $resaf ; valverif:   ".$tabCorlb[$cle]."-->\n";
		} // fin boucle sur les resultats
	} // fin si pas hierarchique cad normal

	// retablit la connexion precedente a la bdd res normaux si necessaire
	if ($newconnect || $newbase) {
		db_close($lnc);
		// db_connect($DBHost,$DBUser,$DBPass,$DBName);/// reouvre la session normale ! normalement elle est deja ouverte, on ne fait que la reactiver
		$GLOBALS['db_lnkid'] = $db_lnk_id_svg ; // sauvegarde de lien 
	}
	traiteParent($resaf); //pour traitement des fins de libellés entre () qui ont tjrs fait chier
	if ($valc != "" && !strstr($valc, '__str2f__')) {
		if ($resaf == "") $resaf = $GLOBALS['valNC'];
		return ($resaf);
	} else {
		return($tabCorlb); // retourne le tableau associatif
	}
}

/** 
 * Pour compatibilité
 */
function ttChpLink($valb0, $reqsup="", $valc="") {
	return ttChpLink2($valb0, $valc, array("reqsup" => $reqsup ));
}

/** traitement des fins de libellés entre () qui ont tjrs fait chier
 *  cherche " () " dans la chaine, si trouvé renvoie la fin entre ()
 * 
 */
function traiteParent(&$resaf) { //pour traitement des fins de libellés entre () qui ont tjrs fait chier
	if (strstr($resaf, " () ")) { // petite nouveauté ajoutée de 5/7/17 pour traitement des fins de libellés entre () qui ont tjrs fait chier
		$tbr = explode(" () ", $resaf);
		if (trim($tbr[1]) != "") {
			$resaf = $tbr[0].' <span class="finlibpya">('.trim($tbr[1]).')</span>';
		} else $resaf = $tbr[0];
	}
}
/**
 *  fonction complémentaire analysant les ordres
 */
function analChpOrder(&$nmchp,&$orderby) {
	if (strstr($nmchp,"~@")) { // si classement inverse en plus sur ce champ
		$nmchp = substr ($nmchp,2); // enleve le ~@
		$orderby = " order by $nmchp DESC "; 
	} elseif (strstr($nmchp,"@")) { // si classement en plus sur ce champ
		$nmchp = substr ($nmchp,1); // enleve le @
		$orderby = " order by $nmchp "; 
	}
}

/**
 *  fonction complementaire reentrante pour la gestion hierarchique
 *  !! le tableau pricipal est passe par argument !
 */
function rettarbo(&$tabCorlb, $valcppid, $defl, $cppid, $rcaf, $orderby, $nbca, $tbcs, $niv=0, $whreqsup="",$othparams = array()) {
	$othparams['carsepldef'] = $othparams['carsepldef'] ? $othparams['carsepldef'] : $GLOBALS['carsepldef'];
	$maxprof = $othparams['maxprof'] ? $othparams['maxprof'] : $GLOBALS['maxprof'];

	$niv++;
	if ($niv > $maxprof) {
		$tabCorlb['err'] = "ERREUR Profond max de l'arbre ($maxprof) depassee !";
		return;
	}
	$rqra = db_query("SELECT $defl[1],$cppid $rcaf from $defl[0] where ($cppid='$valcppid' AND $defl[1]!='$valcppid') $whreqsup $orderby");
	// constitution du tableau associatif a2 dim de corresp code ->lib
	while ($resra = db_fetch_row($rqra)) {
		//$cle=strtoupper($rera[0]);
		$cle = $resra[0];
		$resaf = $resra[2];
		for ($k=2; $k <= $nbca; $k++) {
			$cs = ($tbcs[$k]!="" ? $tbcs[$k] : $othparams['carsepldef']);
			$resaf = $resaf.$cs.$resra[$k + 1];
		}
		traiteParent($resaf); 
		$tabCorlb[$cle] = str_repeat("&nbsp;|&nbsp;&nbsp;",$niv-1)."&nbsp;|--".$resaf; // tableau associatif de correspondance code -> libelle
		rettarbo($tabCorlb,$cle,$defl,$cppid,$rcaf,$orderby,$nbca,$tbcs,$niv,$whreqsup);
	} // fin boucle sur les reponses
	return;
}

/**
 *  fonctions de parsing du champ VALEURS avec la nouvelle syntaxe
 * 
 */
function parseVal($val) {
	$tblval = explode("\n",$val);
	$ret = array();
	foreach ($tblval as $lval) {
		$lval = trim($lval);
		$firstcar = substr($lval,0,1);
		if ($firstcar == "$") {
			$lval = substr($lval,1); // vire premier car = $
			$tbv = explode ("=",$lval);
			foreach ($tbv as $k=>$v) $tbv[$k] = trim($v);
			if ($tbv[0] != '') $ret[$tbv[0]] = $tbv[1];
		} elseif ($firstcar == "#" || $firstcar == ";" || $lval== "") { // commentaire ou ligne vide, on s'en tape donc
		} else $ret['Valeurs'] = $lval; // a l'ancienne
	}
	return($ret);
}
/** fonction qui prépare un tableau asso a afficher dans une LD à partir de valeurs statiques, et de la valeur courante du champ
les balise de sélection sont rajoutées
@param $valeurs chaine de valeur type aa:toto,bb:titi, OU a,b,c
@param $valchp array/string valeur courante du champ 
*/
function valValc2tb($valeurs, $valchp = "") {
	if (!is_array($valchp)) $valchp = array($valchp);
	if (!is_array($valeurs)) $valeurs = explode(',',$valeurs);
	$tabval = array();
	foreach ($valeurs as $val) {
		if (strstr($val, ":")) {
			$valtb = explode(":", $val);
			$key = $valtb[0];
			$val = $valtb[1];
		} else $key = $val;
		$val = tradLib($val);
		if (in_array($key,$valchp)) {
			$val = VSLD.$val;
		}
		$tabval[$key] = stripslashes($val); // au cas ou ' dans les libelles
	}
	return $tabval;
}

/**
 *  fonction qui renvoie un morceau de reqsql ss la forme cle1=valeur1 AND 
 */ 
function kv2sql($tbmultk, $valc, $sep = ":") {
	$uval = explode($sep,$valc);
	$ik = 0;
	foreach($tbmultk as $ck) {
		$whsmk .= " $ck='".$uval[$ik]."' AND ";
		$ik++;
	}
	return(vdc($whsmk,5));
}

/**
 *  fonction qui recuperee les champ libelle (0) ou commentaire(1) d'une table dans DESC_TABLES
 */
function RecLibTable($NM_TABLE, $offs) {
	if ($GLOBALS["NmChpComment"]=="") $GLOBALS["NmChpComment"] = "COMMENT"; // pour oracle qui ne supporte pas le nom de champ comment
	$req = "SELECT LIBELLE,".$GLOBALS["NmChpComment"]." FROM ".$GLOBALS["CisChpp"].$GLOBALS['TBDname'].$GLOBALS["CisChpp"]." WHERE NM_TABLE='$NM_TABLE' AND NM_CHAMP='".$GLOBALS['NmChDT']."'";
	$reqRL = db_query($req);
	$resRL = db_fetch_row($reqRL);
	return($resRL[$offs]);
}

/** fonction servant pr les req sql custom avec paramètres
dans une chaine de type xxxx [arg1] yyyy [arg2] zzzz [arg3]
retourne array(arg1,arg2,arg3)
la chaine est transformée en xxxx ###1### yyyy ###2### zzzz ###3###
 * TODO a améliorer
@param $req la requete */

function parseArgsReq(&$req, $forceEdit = false) {
	$tbarg = array();
	$loop = true;
	do {
		$i++;
		if ($ret = ret1Arg($req, $i, "###", "[", "]", $forceEdit)) {
			$tbarg[] = $ret;
		} else $loop = false;
	} while ($loop);
	return ($tbarg);
}

/** dans une chaine de type xxxx [arg1=val1] yyyy [arg2] zzzz [arg3] 
	si arg1 contient "=" le sépare dans le retour
	retourne tb(arg1,typoss[,val1])et dans str met   xxxx ###i### yyyy [arg2] zzzz [arg3]
	@param &$str la req en cours de parsing
	@param $i le n° d'argument parsé en cours
*/
function ret1Arg(&$str, $i, $chm = "###", $ch1 = "[",$ch2="]", $forceEdit = false) {
	$boorqinsert = stristr($str, "insert") || $forceEdit;
	if ($np =  strpos($str, $ch1)) { // indice position de "["
		$ret = substr($str, $np+1); // chaine après [
		$str1 = substr($str, 0, $np);
		if ($np2 =  strpos($ret, $ch2)) { // indice position de "]"
			$str = $str1.$chm.$i.$chm.substr($ret, $np2 + 1); // nouveauté version 20/11/2020, entourage de l'id par ###$i### et non seulement ###$i
			$typoss = ($boorqinsert || stristr($ret, "where")) ? "Edit" : "Filt"; // si reste where à droite de l'arg c'est qu'on est à gauche donc que c'est un édit, sinon, c'est un filtre
			if ($typoss == "Filt") {
				$strrab = substr($ret, $np2 + 1, 10);
				///die($strrab);
				if (stristr($strrab, 'OR')) {
					$logic = "OU";
				}elseif (stristr($strrab, 'AND')) {
					$logic = "ET";
				} else $logic = "";
			}
			$strret = substr($ret, 0, $np2);
			if (strstr($strret, "=")) {
				$tb1 = explode("=", $strret);
				$ret = array($tb1[0], $typoss, $tb1[1],$logic);
			} else $ret = array($strret, $typoss,null,$logic);
			return $ret;
		} else $ret = false;
	} else $ret = false;
	return($ret);
}

/** Grosse bizarrerie : si le nom de champ (dans le sens input name ) comporte des ".", les "." sont transformés en "_" par apache/php?, donc il faut bricoler salement !!!
 * a cause de ça, on met plutot une syntaxe NomTable#NomChp
 * le NmVar est le NmVar théorique cad NomTable.NomChamp
 * cette fonction les remplace et retourne la var de REQUEST sous la forme NomTable.NomChamp, utilisable directement en sql
 */
function trafikPtsInRequestVar($nmvar) {
	$nmvar2 = str_replace(".", "_", $nmvar);
	if ($nmvar2 != $nmvar && isset($_REQUEST[$nmvar2])) {
		$_REQUEST[$nmvar] = $_REQUEST[$nmvar2];
		unset($_REQUEST[$nmvar2]);
	}
	$nmvar2 = str_replace(".", "#", $nmvar);
	if ($nmvar2 != $nmvar && isset($_REQUEST[$nmvar2])) {
		$_REQUEST[$nmvar] = $_REQUEST[$nmvar2];
		unset($_REQUEST[$nmvar2]);
	}
}
/**
 * 
 * Crée ou récupère sur la cache disque un objet PYAObj
 * @param unknown_type $base
 * @param unknown_type $nmtable
 * @param unknown_type $nmchamp
 * @param unknown_type $forceNew
 * @throws Exception
 */
function createOrLoadPyaObj($nmbase, $nmtable, $nmchamp, $forceNew = false, $dirEcho = 'toto') {
	//die($forceNew);
	$path2tmp = sys_get_temp_dir() ? sys_get_temp_dir() : (get_cfg_var('upload_tmp_dir') != "" ? get_cfg_var('upload_tmp_dir') : "/tmp");
	$nmbase = hash('md2', $nmbase); // à cause des connexions Oracle ou la bdd est genre (DESCRIPTION=(ENABLE=BROKEN)(CONNECT_TIMEOUT=10)(RETRY_COUNT=3)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=oda2-scan-stg.ifce.lan)(PORT=3030))(FAILOVER=ON))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=SIREDEV)(FAILOVER_MODE=(TYPE=SELECT)(METHOD=BASIC))))
	$filename = $path2tmp."/".$nmbase."-".$nmtable."-".$nmchamp.".pyaobj";
	if (!file_exists($filename) || $forceNew) {
		$pyaobj = new PYAobj(); // instancie un nouvel objet
		$pyaobj->NmBase = $nmbase;
		$pyaobj->NmTable = $nmtable;
		$pyaobj->NmChamp = $nmchamp;
		if ($dirEcho != 'toto') {
			$pyaobj->DirEcho = $dirEcho;
		}
		$pyaobj->InitPO();
		$pyaobjser = serialize($pyaobj);
		$h = fopen ($filename, "w");
		fwrite($h, $pyaobjser);
		fclose($h);
	} else {
		if ($nmtable == "") { // il peut arriver qu'on ai pas le nom de table, on essaye de le trouver sans
			$path = $path2tmp."/".$nmbase."-*-".$nmchamp.".pyaobj";
			$tbfm = glob($path);
			if (count($tbfm) != 1) {
				throw new Exception("Impossible de déterminer automatiquement la table dans createOrLoadPyaObj , base $nmbase, table $nmtable, champ $nmchamp");
			} else $filename = $tbfm[0];
		}
		$h = fopen ($filename, "r");
		$pyaobjser = fread($h, filesize($filename));
		fclose($h);
		$pyaobj = unserialize($pyaobjser);
	}
	if ($dirEcho != 'toto') {
			$pyaobj->DirEcho = $dirEcho;
	}
	return($pyaobj);
}
/**
 * Nettoie le cache disque sur le serveur
 * @param unknown_type $nmbase
 * @param unknown_type $nmtable
 * @param unknown_type $nmchamp
 */
function cleanPyaObjCache($nmbase = "*", $nmtable = "*", $nmchamp = "*") {
	$path2tmp = sys_get_temp_dir() ? sys_get_temp_dir() : (get_cfg_var('upload_tmp_dir') != "" ? get_cfg_var('upload_tmp_dir') : "/tmp");
	$filenamemask = $path2tmp."/".$nmbase."-".$nmtable."-".$nmchamp.".pyaobj";
	//unlink appelé en "boucle" automatiquement
	array_map("unlink", glob($filenamemask)); 
	//print_r(glob($filenamemask));
//	foreach (glob($filenamemask) as $f) {
//		unlink ($f) or die ("impossible de supprimer $f");
//	}
//	print_r(glob($filenamemask));
//	die($filenamemask);
	
}